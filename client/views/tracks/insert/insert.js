var pageSession = new ReactiveDict();

Template.TracksInsert.rendered = function() {
	
};

Template.TracksInsert.events({
	
});

Template.TracksInsert.helpers({
	
});

Template.TracksInsertInsertForm.rendered = function() {
	

	pageSession.set("tracksInsertInsertFormInfoMessage", "");
	pageSession.set("tracksInsertInsertFormErrorMessage", "");

	$('.datetimepicker').datetimepicker();


	//$("[name='my-checkbox']").bootstrapSwitch();

	var elemActive = document.querySelector('.js-switchActive');
	var initActive = new Switchery(elemActive);

	var elemFB = document.querySelector('.js-switchFB');
	var initFB = new Switchery(elemFB);

	var elemKik = document.querySelector('.js-switchKik');
	var initKik = new Switchery(elemKik);


	//$(".input-group.date").each(function() {
	//	var format = $(this).find("input[type='text']").attr("data-format");
    //
	//	if(format) {
	//		format = format.toLowerCase();
	//	}
	//	else {
	//		format = "mm/dd/yyyy";
	//	}
    //
	//	$(this).datepicker({
	//		autoclose: true,
	//		todayHighlight: true,
	//		todayBtn: true,
	//		forceParse: false,
	//		keyboardNavigation: false,
	//		format: format
	//	});
	//});

	$("input[type='file']").fileinput();
	$("select[data-role='tagsinput']").tagsinput();
	$(".bootstrap-tagsinput").addClass("form-control");
	$("input[autofocus]").focus();
};

Template.TracksInsertInsertForm.events({
	"submit": function(e, t) {
		e.preventDefault();
		pageSession.set("tracksInsertInsertFormInfoMessage", "");
		pageSession.set("tracksInsertInsertFormErrorMessage", "");

		var self = this;

		function submitAction(msg) {
			var tracksInsertInsertFormMode = "insert";
			if(!t.find("#form-cancel-button")) {
				switch(tracksInsertInsertFormMode) {
					case "insert": {
						$(e.target)[0].reset();
					}; break;

					case "update": {
						var message = msg || "Saved.";
						pageSession.set("tracksInsertInsertFormInfoMessage", message);
					}; break;
				}
			}

			Router.go("tracks", {});

			/*Stay in the same page... Router.go("tracks", {});*/
		}

		function errorAction(msg) {
			msg = msg || "";
			var message = msg.message || msg || "Error.";
			pageSession.set("tracksInsertInsertFormErrorMessage", message);
		}

		validateForm(
			$(e.target),
			function(fieldName, fieldValue) {

			},
			function(msg) {

			},
			function(values) {


				newId = Tracks.insert(values, function(e) { if(e) errorAction(e); else submitAction(); });
				//newId = Tracks.save(values, function(e) { if(e) errorAction(e); else submitAction(); });


				//if (typeof newId == 'undefined'){
                //
				//	newId = Tracks.insert(values, function(e) { if(e) errorAction(e); else submitAction(); });
				//	console.log("Insert new...");
                //
                //
				//}else{
                //
				//	newId = Tracks.update(values, function(e) { if(e) errorAction(e); else submitAction(); },{upsert:true});
				//	console.log("Update...");
                //
				//}
                //
				//console.log("newId="+newId);



			}
		);

		return false;
	},
	"click #form-cancel-button": function(e, t) {
		e.preventDefault();

		

		Router.go("tracks", {});
	},
	"click #form-close-button": function(e, t) {
		e.preventDefault();

		/*CLOSE_REDIRECT*/
	},
	"click #form-back-button": function(e, t) {
		e.preventDefault();

		/*BACK_REDIRECT*/
	}

	
});

Template.TracksInsertInsertForm.helpers({
	"infoMessage": function() {
		return pageSession.get("tracksInsertInsertFormInfoMessage");
	},
	"errorMessage": function() {
		return pageSession.get("tracksInsertInsertFormErrorMessage");
	}
	
});
