this.UsersListController = RouteController.extend({
	template: "UsersList",


	yieldTemplates: {
	},

	onBeforeAction: function() {
		this.next();
	},

	action: function() {
		if(this.isReady()) { this.render(); } else { this.render("loading"); }
	},

	isReady: function() {


		var subs = [
			Meteor.subscribe("tracks"),
			Meteor.subscribe("campaigns"),
			Meteor.subscribe("proxytracks"),
                        Meteor.subscribe("imuser")
		];
		var ready = true;
		_.each(subs, function(sub) {
			if(!sub.ready())
				ready = false;
		});
		return ready;
	},

	data: function() {


		return {
			params: this.params || {},
            tracks: Tracks.find({}, {}),
            campaigns: Campaigns.find({}, {}),
            prxoytracks: ProxyTracks.find({}, {}),
            imuser: Imuser.find({}, {})
		};
	},

	onAfterAction: function() {

	}
});
