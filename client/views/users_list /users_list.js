var pageSession = new ReactiveDict();
var raw = {};


Meteor.subscribe("tracks", {
    onReady: function() {
        readStatistics();
    }
})

function readStatistics() {
    raw = Tracks.find({}, {sort: {createdAt: -1}}).fetch();
}

var findSubscription = function(uid) {
    var groupArray = Imuser.find({imuser: uid}, {limit: 1}).fetch().map(function (x) {
        return x.group;
    });
    if (groupArray.length == 0)
        return "";

    var group = "<br>" + groupArray.toString();
    group = group.replace(/\(/g, "");
    group = group.replace(/\)/g, "<br>");
    group = group.substring(0, group.length - 4);

    return group;
}

var getName = function(firstName, lastName) {
    if (firstName == undefined)
        firstName = "";
    if (lastName == undefined)
        lastName = "";
    return firstName + " " + lastName;
}

var getConversionsUrls = function(uid) {
    var urlsArray = ProxyTracks.find({uid: uid}).fetch().map(function (x) {
        return x.url;
    });
    if (urlsArray.length == 0)
        return "";

    var urls = urlsArray.toString();
    urls = "<br>" + urls.replace(/,/g, "<br>");

    return urls;
}

var convertTracking = function(message) {
    if (message == null)
        return "";

    var tracking = "<br>" + message.replace(/,/g,'<br>');

    return tracking;
}

var findGender = function(gender, repeatView) {
    if (gender == undefined) {
        if (repeatView == 0)
            return "male";

        return "female";
    }

    return gender;
}

var findPlatform = function(uid) {
    if (uid.indexOf("==") != -1)
        return "Viber";

    return "Facebook";
}

var findInteractions = function(message) {
    if (message == null)
        return 0;

    return (message.match(/,/g) || []).length + 1;
}

var localeToCountry = function(locale) {
    if (locale == undefined)
        return "United States";

    var updatedLocale = locale;
    if (updatedLocale == 'en_US' || updatedLocale == 'es_US')
        return "United States";
    else if (updatedLocale == 'en_GB')
        return "United Kingdom";
    else if(updatedLocale == 'en_AU')
        return "Australia";
    else if (updatedLocale == 'en_NZ')
        return "New Zealand";
    else if (updatedLocale == 'en_IE' || updatedLocale == 'ga_IE')
        return "Ireland";
    else if (updatedLocale == 'en_IN' || updatedLocale == 'hi_IN')
        return "India";
    else if (updatedLocale == 'en_JM')
        return "Jamaica";
    else if (updatedLocale == 'en_PH')
        return "Phillippines";
    else if (updatedLocale == 'en_ZA')
        return "Southern Africa";
    else if(updatedLocale == 'en_CA' || updatedLocale == 'fr_CA')
        return "Canada";
    else if (updatedLocale == 'en_SG')
        return "Singapore";
    else if (updatedLocale == 'en_MT' || updatedLocale == 'mt_MT')
        return "Malta";
    else if (updatedLocale == 'fr_FR')
        return "France";
    else if (updatedLocale == 'fr_BE' || updatedLocale == 'nl_BE')
        return "Belgium";
    else if (updatedLocale == 'fr_LU' || updatedLocale == 'de_LU')
        return "Luxembourg";
    else if (updatedLocale == 'fr_CH' || updatedLocale == 'de_CH' || updatedLocale == 'it_CH')
        return "Switzerland";
    else if (updatedLocale == 'de_AT')
        return "Austria";
    else if (updatedLocale == 'de_DE')
        return "Germany";
    else if (updatedLocale == 'it_IT')
        return "Italy";
    else if (updatedLocale == 'be_BY')
        return "Belarus";
    else if (updatedLocale == 'pt_BR')
        return "Brazil";
    else if (updatedLocale == 'pt_PT')
        return "Portugal";
    else if (updatedLocale =='	fi_FI')
        return "Finland";
    else if (updatedLocale == 'hr_HR')
        return "Croatia";
    else if (updatedLocale == 'hu_HU')
        return "Hungary";
    else if (updatedLocale == 'in_ID')
        return "Indonesia";
    else if (updatedLocale == 'et_EE')
        return "Estonia";
    else if (updatedLocale == '	el_GR')
        return "Greece";
    else if (updatedLocale == '	el_CY')
        return "Cyprus";
    else if (updatedLocale == 'is_IS')
        return "Iceland";
    else if (updatedLocale == 'iw_IL')
        return "Israel";
    else if (updatedLocale == 'ja_JP')
        return "Japan";
    else if (updatedLocale == 'ko_KR')
        return "South Korea";
    else if (updatedLocale == 'lt_LT')
        return "Lithuania";
    else if (updatedLocale == 'lv_LV')
        return "Latvia";
    else if (updatedLocale == 'mk_MK')
        return "Macedonia";
    else if (updatedLocale == 'ms_MY')
        return "Malaysia";
    else if (updatedLocale == 'no_NO')
        return "Norway";
    else if (updatedLocale == 'pl_PL')
        return "Poland";
    else if (updatedLocale == 'ro_RO')
        return "Romania";
    else if (updatedLocale == 'ru_RU')
        return "Russia";
    else if (updatedLocale == 'sk_SK')
        return "Slovakia";
    else if (updatedLocale == 'sl_SI')
        return "Slovenia";
    else if (updatedLocale == 'sq_AL')
        return "Albania";
    else if (updatedLocale == 'sv_SE')
        return "Sweden";
    else if (updatedLocale == 'tr_TR')
        return "Turkey";
    else if (updatedLocale == 'uk_UA')
        return "Ukraine";
    else if (updatedLocale == 'bg_BG')
        return "Bulgaria";
    else if (updatedLocale == 'vi_VN')
        return "Vietnam";
    else if (updatedLocale == 'cs_CZ')
        return "Czech Republic";
    else if (updatedLocale == 'da_DK')
        return "Denmark";
    else if (updatedLocale == 'nl_NL')
        return "Netherlands";
    else if (updatedLocale == 'sr_RS')
        return "Serbia";
    else if (updatedLocale == 'sr_BA')
        return "Bosnia and Herzegovina";
    else if (updatedLocale == 'sr_ME')
        return "Montenegro";
    else if (updatedLocale == 'zh_CN')
        return "China";
    else if (updatedLocale == 'zh_HK')
        return "Hong Kong";
    else if (updatedLocale == 'zh_SG')
        return "Singapore";
    else if (updatedLocale == 'zh_TW')
        return "Taiwan";
    else if (updatedLocale == 'th_TH')
        return "Thailand";
    else if (updatedLocale == 'ar_AE')
        return "United Arab Emirates";
    else if (updatedLocale == 'ar_BH')
        return "Bahrain";
    else if (updatedLocale == 'ar_DZ')
        return "Algeria";
    else if (updatedLocale == 'ar_EG')
        return "Egypt";
    else if (updatedLocale == 'ar_IQ')
        return "Iraq";
    else if (updatedLocale == 'ar_JO')
        return "Jordan";
    else if (updatedLocale == 'ar_KW')
        return "Kuwait";
    else if (updatedLocale == 'ar_LB')
        return "Lebanon";
    else if (updatedLocale == 'ar_LY')
        return "Libya";
    else if (updatedLocale == 'ar_MA')
        return "Morocco";
    else if (updatedLocale == 'ar_OM')
        return "Oman";
    else if (updatedLocale == 'ar_QA')
        return "Qatar";
    else if (updatedLocale == 'ar_SA')
        return "Saudi Arabia";
    else if (updatedLocale == 'ar_SD')
        return "Sudan";
    else if (updatedLocale == 'ar_SY')
        return "Syria";
    else if (updatedLocale == 'ar_TN')
        return "Tunisia";
    else if (updatedLocale == 'ar_YE')
        return "Yemen";
    else if (updatedLocale == 'es_ES' || updatedLocale == 'ca_ES')
        return "Spain";
    else if (updatedLocale == 'es_MX')
        return "Mexico";
    else if (updatedLocale == 'es_AR')
        return "Argentina";
    else if (updatedLocale == 'es_BO')
        return "Bolivia";
    else if (updatedLocale == 'es_CL')
        return "Chile";
    else if (updatedLocale == 'es_CO')
        return "Colombia";
    else if (updatedLocale == 'es_CR')
        return "Costa Rica";
    else if (updatedLocale == 'es_DO')
        return "Dominican Republic";
    else if (updatedLocale == 'es_EC')
        return "Ecuador";
    else if (updatedLocale == 'es_GT')
        return "Guatemala";
    else if (updatedLocale == 'es_HN')
        return "Honduras";
    else if (updatedLocale == 'es_NI')
        return "Nicaragua";
    else if (updatedLocale == 'es_PA')
        return "Panama";
    else if (updatedLocale == 'es_PE')
        return "Peru";
    else if (updatedLocale == 'es_PR')
        return "Puerto Rico";
    else if (updatedLocale == 'es_PY')
        return "Paraguay";
    else if (updatedLocale == 'es_SV')
        return "El Salvador";
    else if (updatedLocale == 'es_UY')
        return "Uruguay";
    else if (updatedLocale == 'es_VE')
        return "Venezuela";
    else
        return "United States";
}

Template.UsersList.onRendered(function(){

});


Template.UsersList.rendered = function() {

    var chartIncomeData = [
        {
            label: "line",
            data: [ [1, 10], [2, 26], [3, 16], [4, 36], [5, 32], [6, 51] ]
        }
    ];

    var chartIncomeOptions = {
        series: {
            lines: {
                show: true,
                lineWidth: 0,
                fill: true,
                fillColor: "#0BBBEF"

            }
        },
        colors: ["0066cb"],
        grid: {
            show: false
        },
        legend: {
            show: false
        }
    };

};

Template.UsersList.events({

    'click .showhide': function(event){
        event.preventDefault();
        var hpanel = $(event.target).closest('div.hpanel');
        var icon = $(event.target).closest('i');
        var body = hpanel.find('div.panel-body');
        var footer = hpanel.find('div.panel-footer');
        body.slideToggle(300);
        footer.slideToggle(200);

        // Toggle icon from up to down
        icon.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
        hpanel.toggleClass('').toggleClass('panel-collapse');
        setTimeout(function () {
            hpanel.resize();
            hpanel.find('[id^=map-]').resize();
        }, 50);
    },

    'click .closebox': function(event){
        event.preventDefault();
        var hpanel = $(event.target).closest('div.hpanel');
        hpanel.remove();
    },

    "submit #dataview-controls": function(e, t) {
		return false;
	}

});

Template.UsersList.helpers({
	"isEmpty": function() {
		return !this.tracks || this.tracks.count() == 0;
	},
	"isNotEmpty": function() {
		return this.tracks && this.tracks.count() > 0;
	},
	"isNotFound": function() {
		return this.tracks && pageSession.get("UsersListViewSearchString") && UsersListViewItems(this.tracks).length == 0;
	}
});

var NumberOfUsers = function() {
    var dateToShow = pageSession.get("dateToShow");

    var campaignNames = Campaigns.find().fetch().map(function (x) {
        return x.name;
    });

    var filteredData = [];
    for (var i = 0; i < raw.length; i++)
        if (campaignNames.indexOf(raw[i].campaignName) != -1)
            filteredData.push(raw[i]);

    return filteredData.length;

    /*if (platform == "all" && gender == "all" && country == "all")
        return Tracks.find({campaignName: {$in: campaignNames}}).count();
    if (platform == "all" && gender == "male" && country == "all")
        return Tracks.find({$and: [{campaignName: {$in: campaignNames}}, {$or: [{gender: 'male'}, {$and: [{repeatView: '0'}, {gender: {$exists: false}}]}]}]}).count();
    if (platform == "all" && gender == "female" && country == "all")
        return Tracks.find({$and: [{campaignName: {$in: campaignNames}}, {$or: [{gender: 'female'}, {$and: [{repeatView: {$gt: '0'}}, {gender: {$exists: false}}]}]}]}).count();
    else if(platform == "Facebook" && gender == "all" && country == "all")
        return Tracks.find({$and: [{campaignName: {$in: campaignNames}}, {uid:{$not: /==/}}]}).count();
    else if(platform == "Facebook" && gender == "male" && country == "all")
        return Tracks.find({$and: [{campaignName: {$in: campaignNames}}, {uid:{$not: /==/}}, {$or: [{gender: 'male'}, {$and: [{repeatView: '0'}, {gender: {$exists: false}}]}]}]}).count();
    else if(platform == "Facebook" && gender == "female" && country == "all")
        return Tracks.find({$and: [{campaignName: {$in: campaignNames}}, {uid:{$not: /==/}}, {$or: [{gender: 'female'}, {$and: [{repeatView: {$gt: '0'}}, {gender: {$exists: false}}]}]}]}).count();
    else if (platform == "Viber" && gender == "all" && country == "all")
        return Tracks.find({$and: [{campaignName: {$in: campaignNames}}, {uid: /==/}]}).count();
    else if (platform == "Viber" && gender == "male" && country == "all")
        return Tracks.find({$and: [{campaignName: {$in: campaignNames}}, {uid: /==/}, {$or: [{gender: 'male'}, {$and: [{repeatView: '0'}, {gender: {$exists: false}}]}]}]}).count();
    else if (platform == "Viber" && gender == "female" && country == "all")
        return Tracks.find({$and: [{campaignName: {$in: campaignNames}}, {uid: /==/}, {$or: [{gender: 'female'}, {$and: [{repeatView: {$gt: '0'}}, {gender: {$exists: false}}]}]}]}).count();
    else if (platform == "all" && gender == "all" && country == "united_states")
        return Tracks.find({$and: [{campaignName: {$in: campaignNames}}, {$or: [{locale: {$exists: false}}, {locale: 'en_US'}, {locale: 'es_US'}]}]}).count();
    else if (platform == "Facebook" && gender == "all" && country == "united_states")
        return Tracks.find({$and: [{campaignName: {$in: campaignNames}}, {uid:{$not: /==/}}, {$or: [{locale: {$exists: false}}, {locale: 'en_US'}, {locale: 'es_US'}]}]}).count();
    else if (platform == "Viber" && gender == "all" && country == "united_states")
        return Tracks.find({$and: [{campaignName: {$in: campaignNames}}, {uid: /==/}, {$or: [{locale: {$exists: false}}, {locale: 'en_US'}, {locale: 'es_US'}]}]}).count();
    else if (platform == "all" && gender == "male" && country == "united_states")
        return Tracks.find({$and: [{campaignName: {$in: campaignNames}}, {$or: [{gender: 'male'}, {$and: [{repeatView: '0'}, {gender: {$exists: false}}]}]}, {$or: [{locale: {$exists: false}}, {locale: 'en_US'}, {locale: 'es_US'}]}]}).count();
    else if (platform == "all" && gender == "female" && country == "united_states")
        return Tracks.find({$and: [{campaignName: {$in: campaignNames}}, {$or: [{gender: 'female'}, {$and: [{repeatView: {$gt: '0'}}, {gender: {$exists: false}}]}]}, {$or: [{locale: {$exists: false}}, {locale: 'en_US'}, {locale: 'es_US'}]}]}).count();
    else if (platform == "Facebook" && gender == "male" && country == "united_states")
        return Tracks.find({$and: [{campaignName: {$in: campaignNames}}, {uid:{$not: /==/}}, {$or: [{gender: 'male'}, {$and: [{repeatView: '0'}, {gender: {$exists: false}}]}]}, {$or: [{locale: {$exists: false}}, {locale: 'en_US'}, {locale: 'es_US'}]}]}).count();
    else if (platform == "Facebook" && gender == "female" && country == "united_states")
        return Tracks.find({$and: [{campaignName: {$in: campaignNames}}, {uid:{$not: /==/}}, {$or: [{gender: 'female'}, {$and: [{repeatView: {$gt: '0'}}, {gender: {$exists: false}}]}]}, {$or: [{locale: {$exists: false}}, {locale: 'en_US'}, {locale: 'es_US'}]}]}).count();
    else if (platform == "Viber" && gender == "male" && country == "united_states")
        return Tracks.find({$and: [{campaignName: {$in: campaignNames}}, {uid: /==/}, {$or: [{gender: 'male'}, {$and: [{repeatView: '0'}, {gender: {$exists: false}}]}]}, {$or: [{locale: {$exists: false}}, {locale: 'en_US'}, {locale: 'es_US'}]}]}).count();
    else if (platform == "Viber" && gender == "female" && country == "united_states")
        return Tracks.find({$and: [{campaignName: {$in: campaignNames}}, {uid: /==/}, {$or: [{gender: 'female'}, {$and: [{repeatView: {$gt: '0'}}, {gender: {$exists: false}}]}]}, {$or: [{locale: {$exists: false}}, {locale: 'en_US'}, {locale: 'es_US'}]}]}).count();
    else if (platform == "all" && gender == "all" && country == "united_kingdom")
        return Tracks.find({$and: [{campaignName: {$in: campaignNames}}, {locale: 'en_GB'}]}).count();
    else if (platform == "Facebook" && gender == "all" && country == "united_kingdom")
        return Tracks.find({$and: [{campaignName: {$in: campaignNames}}, {uid:{$not: /==/}}, {locale: 'en_GB'}]}).count();
    else if (platform == "Viber" && gender == "all" && country == "united_kingdom")
        return Tracks.find({$and: [{campaignName: {$in: campaignNames}}, {uid: /==/}, {locale: 'en_GB'}]}).count();
    else if (platform == "all" && gender == "male" && country == "united_kingdom")
        return Tracks.find({$and: [{campaignName: {$in: campaignNames}}, {$or: [{gender: 'male'}, {$and: [{repeatView: '0'}, {gender: {$exists: false}}]}]}, {locale: 'en_GB'}]}).count();
    else if (platform == "all" && gender == "female" && country == "united_kingdom")
        return Tracks.find({$and: [{campaignName: {$in: campaignNames}}, {$or: [{gender: 'female'}, {$and: [{repeatView: {$gt: '0'}}, {gender: {$exists: false}}]}]}, {locale: 'en_GB'}]}).count();
    else if (platform == "Facebook" && gender == "male" && country == "united_kingdom")
        return Tracks.find({$and: [{campaignName: {$in: campaignNames}}, {uid:{$not: /==/}}, {$or: [{gender: 'male'}, {$and: [{repeatView: '0'}, {gender: {$exists: false}}]}]}, {locale: 'en_GB'}]}).count();
    else if (platform == "Facebook" && gender == "female" && country == "united_kingdom")
        return Tracks.find({$and: [{campaignName: {$in: campaignNames}}, {uid:{$not: /==/}}, {$or: [{gender: 'female'}, {$and: [{repeatView: {$gt: '0'}}, {gender: {$exists: false}}]}]}, {locale: 'en_GB'}]}).count();
    else if (platform == "Viber" && gender == "male" && country == "united_kingdom")
        return Tracks.find({$and: [{campaignName: {$in: campaignNames}}, {uid: /==/}, {$or: [{gender: 'male'}, {$and: [{repeatView: '0'}, {gender: {$exists: false}}]}]}, {locale: 'en_GB'}]}).count();
    else if (platform == "Viber" && gender == "female" && country == "united_kingdom")
        return Tracks.find({$and: [{campaignName: {$in: campaignNames}}, {uid: /==/}, {$or: [{gender: 'female'}, {$and: [{repeatView: {$gt: '0'}}, {gender: {$exists: false}}]}]}, {locale: 'en_GB'}]}).count();
    else if (platform == "all" && gender == "all" && country == "canada")
        return Tracks.find({$and: [{campaignName: {$in: campaignNames}}, {$or: [{locale: 'en_CA'}, {locale: 'fr_CA'}]}]}).count();
    else if (platform == "Facebook" && gender == "all" && country == "canada")
        return Tracks.find({$and: [{campaignName: {$in: campaignNames}}, {uid:{$not: /==/}}, {$or: [{locale: 'en_CA'}, {locale: 'fr_CA'}]}]}).count();
    else if (platform == "Viber" && gender == "all" && country == "canada")
        return Tracks.find({$and: [{campaignName: {$in: campaignNames}}, {uid: /==/}, {$or: [{locale: 'en_CA'}, {locale: 'fr_CA'}]}]}).count();
    else if (platform == "all" && gender == "male" && country == "canada")
        return Tracks.find({$and: [{campaignName: {$in: campaignNames}}, {$or: [{gender: 'male'}, {$and: [{repeatView: '0'}, {gender: {$exists: false}}]}]}, {$or: [{locale: 'en_CA'}, {locale: 'fr_CA'}]}]}).count();
    else if (platform == "all" && gender == "female" && country == "canada")
        return Tracks.find({$and: [{campaignName: {$in: campaignNames}}, {$or: [{gender: 'female'}, {$and: [{repeatView: {$gt: '0'}}, {gender: {$exists: false}}]}]}, {$or: [{locale: 'en_CA'}, {locale: 'fr_CA'}]}]}).count();
    else if (platform == "Facebook" && gender == "male" && country == "canada")
        return Tracks.find({$and: [{campaignName: {$in: campaignNames}}, {uid:{$not: /==/}}, {$or: [{gender: 'male'}, {$and: [{repeatView: '0'}, {gender: {$exists: false}}]}]}, {$or: [{locale: 'en_CA'}, {locale: 'fr_CA'}]}]}).count();
    else if (platform == "Facebook" && gender == "female" && country == "canada")
        return Tracks.find({$and: [{campaignName: {$in: campaignNames}}, {uid:{$not: /==/}}, {$or: [{gender: 'female'}, {$and: [{repeatView: {$gt: '0'}}, {gender: {$exists: false}}]}]}, {$or: [{locale: 'en_CA'}, {locale: 'fr_CA'}]}]}).count();
    else if (platform == "Viber" && gender == "male" && country == "canada")
        return Tracks.find({$and: [{campaignName: {$in: campaignNames}}, {uid: /==/}, {$or: [{gender: 'male'}, {$and: [{repeatView: '0'}, {gender: {$exists: false}}]}]}, {$or: [{locale: 'en_CA'}, {locale: 'fr_CA'}]}]}).count();
    else if (platform == "Viber" && gender == "female" && country == "canada")
        return Tracks.find({$and: [{campaignName: {$in: campaignNames}}, {uid: /==/}, {$or: [{gender: 'female'}, {$and: [{repeatView: {$gt: '0'}}, {gender: {$exists: false}}]}]}, {$or: [{locale: 'en_CA'}, {locale: 'fr_CA'}]}]}).count();
    else if (platform == "all" && gender == "all" && country == "france")
        return Tracks.find({$and: [{campaignName: {$in: campaignNames}}, {locale: 'fr_FR'}]}).count();
    else if (platform == "Facebook" && gender == "all" && country == "france")
        return Tracks.find({$and: [{campaignName: {$in: campaignNames}}, {uid:{$not: /==/}}, {locale: 'fr_FR'}]}).count();
    else if (platform == "Viber" && gender == "all" && country == "france")
        return Tracks.find({$and: [{campaignName: {$in: campaignNames}}, {uid: /==/}, {locale: 'fr_FR'}]}).count();
    else if (platform == "all" && gender == "male" && country == "france")
        return Tracks.find({$and: [{campaignName: {$in: campaignNames}}, {$or: [{gender: 'male'}, {$and: [{repeatView: '0'}, {gender: {$exists: false}}]}]}, {locale: 'fr_FR'}]}).count();
    else if (platform == "all" && gender == "female" && country == "france")
        return Tracks.find({$and: [{campaignName: {$in: campaignNames}}, {$or: [{gender: 'female'}, {$and: [{repeatView: {$gt: '0'}}, {gender: {$exists: false}}]}]}, {locale: 'fr_FR'}]}).count();
    else if (platform == "Facebook" && gender == "male" && country == "france")
        return Tracks.find({$and: [{campaignName: {$in: campaignNames}}, {uid:{$not: /==/}}, {$or: [{gender: 'male'}, {$and: [{repeatView: '0'}, {gender: {$exists: false}}]}]}, {locale: 'fr_FR'}]}).count();
    else if (platform == "Facebook" && gender == "female" && country == "france")
        return Tracks.find({$and: [{campaignName: {$in: campaignNames}}, {uid:{$not: /==/}}, {$or: [{gender: 'female'}, {$and: [{repeatView: {$gt: '0'}}, {gender: {$exists: false}}]}]}, {locale: 'fr_FR'}]}).count();
    else if (platform == "Viber" && gender == "male" && country == "france")
        return Tracks.find({$and: [{campaignName: {$in: campaignNames}}, {uid: /==/}, {$or: [{gender: 'male'}, {$and: [{repeatView: '0'}, {gender: {$exists: false}}]}]}, {locale: 'fr_FR'}]}).count();
    else if (platform == "Viber" && gender == "female" && country == "france")
        return Tracks.find({$and: [{campaignName: {$in: campaignNames}}, {uid: /==/}, {$or: [{gender: 'female'}, {$and: [{repeatView: {$gt: '0'}}, {gender: {$exists: false}}]}]}, {locale: 'fr_FR'}]}).count();
    else if (platform == "all" && gender == "all" && country == "germany")
        return Tracks.find({$and: [{campaignName: {$in: campaignNames}}, {locale: 'de_DE'}]}).count();
    else if (platform == "Facebook" && gender == "all" && country == "germany")
        return Tracks.find({$and: [{campaignName: {$in: campaignNames}}, {uid:{$not: /==/}}, {locale: 'de_DE'}]}).count();
    else if (platform == "Viber" && gender == "all" && country == "germany")
        return Tracks.find({$and: [{campaignName: {$in: campaignNames}}, {uid: /==/}, {locale: 'de_DE'}]}).count();
    else if (platform == "all" && gender == "male" && country == "germany")
        return Tracks.find({$and: [{campaignName: {$in: campaignNames}}, {$or: [{gender: 'male'}, {$and: [{repeatView: '0'}, {gender: {$exists: false}}]}]}, {locale: 'de_DE'}]}).count();
    else if (platform == "all" && gender == "female" && country == "germany")
        return Tracks.find({$and: [{campaignName: {$in: campaignNames}}, {$or: [{gender: 'female'}, {$and: [{repeatView: {$gt: '0'}}, {gender: {$exists: false}}]}]}, {locale: 'de_DE'}]}).count();
    else if (platform == "Facebook" && gender == "male" && country == "germany")
        return Tracks.find({$and: [{campaignName: {$in: campaignNames}}, {uid:{$not: /==/}}, {$or: [{gender: 'male'}, {$and: [{repeatView: '0'}, {gender: {$exists: false}}]}]}, {locale: 'de_DE'}]}).count();
    else if (platform == "Facebook" && gender == "female" && country == "germany")
        return Tracks.find({$and: [{campaignName: {$in: campaignNames}}, {uid:{$not: /==/}}, {$or: [{gender: 'female'}, {$and: [{repeatView: {$gt: '0'}}, {gender: {$exists: false}}]}]}, {locale: 'de_DE'}]}).count();
    else if (platform == "Viber" && gender == "male" && country == "germany")
        return Tracks.find({$and: [{campaignName: {$in: campaignNames}}, {uid: /==/}, {$or: [{gender: 'male'}, {$and: [{repeatView: '0'}, {gender: {$exists: false}}]}]}, {locale: 'de_DE'}]}).count();
    else if (platform == "Viber" && gender == "female" && country == "germany")
        return Tracks.find({$and: [{campaignName: {$in: campaignNames}}, {uid: /==/}, {$or: [{gender: 'female'}, {$and: [{repeatView: {$gt: '0'}}, {gender: {$exists: false}}]}]}, {locale: 'de_DE'}]}).count();*/
}

var platform = "all";
var gender = "all";
var country = "all";

var UsersListViewItems = function() {
    var dateToShow = pageSession.get("dateToShow");

    var currentRecord = pageSession.get("currentRecord");
    if (currentRecord == undefined)
        currentRecord = 0;

    var campaignNames = Campaigns.find().fetch().map(function (x) {
        return x.name;
    });

    var filteredData = [];
    for (var i = 0; i < raw.length; i++)
        if (campaignNames.indexOf(raw[i].campaignName) != -1)
            filteredData.push(raw[i]);

    var result = [];
    if (platform == "all" && gender == "all" && country == "all") {
        result = filteredData;
    }
    else if (platform == "all" && gender == "male" && country == "all") {
        for (var i = 0; i < filteredData.length; i++)
            if (filteredData[i].gender == "male")
                result.push(filteredData[i]);
    }
    else if (platform == "all" && gender == "female" && country == "all") {
        for (var i = 0; i < filteredData.length; i++)
            if (filteredData[i].gender == "female")
                result.push(filteredData[i]);
    }
    else if(platform == "Facebook" && gender == "all" && country == "all") {
        for (var i = 0; i < filteredData.length; i++)
            if (filteredData[i].uid.indexOf("==") == -1)
                result.push(filteredData[i]);
    }
    else if(platform == "Facebook" && gender == "male" && country == "all") {
        for (var i = 0; i < filteredData.length; i++)
        if (filteredData[i].uid.indexOf("==") == -1 && filteredData[i].gender == "male")
            result.push(filteredData[i]);
    }
    else if(platform == "Facebook" && gender == "female" && country == "all") {
        for (var i = 0; i < filteredData.length; i++)
            if (filteredData[i].uid.indexOf("==") == -1 && filteredData[i].gender == "female")
                result.push(filteredData[i]);
    }
    else if (platform == "Viber" && gender == "all" && country == "all") {
        for (var i = 0; i < filteredData.length; i++)
            if (filteredData[i].uid.indexOf("==") != -1)
                result.push(filteredData[i]);
    }
    else if (platform == "Viber" && gender == "male" && country == "all") {
        for (var i = 0; i < filteredData.length; i++)
        if (filteredData[i].uid.indexOf("==") != -1 && filteredData[i].gender == "male")
            result.push(filteredData[i]);
    }
    else if (platform == "Viber" && gender == "female" && country == "all") {
        for (var i = 0; i < filteredData.length; i++)
            if (filteredData[i].uid.indexOf("==") != -1 && filteredData[i].gender == "female")
                result.push(filteredData[i]);
    }
    else if (platform == "all" && gender == "all" && country == "united_states") {
        for (var i = 0; i < filteredData.length; i++)
            if (filteredData[i].locale == "en_US" || filteredData[i].locale == 'es_US')
                result.push(filteredData[i]);
    }
    else if (platform == "Facebook" && gender == "all" && country == "united_states") {
        for (var i = 0; i < filteredData.length; i++)
            if (filteredData[i].uid.indexOf("==") == -1 && (filteredData[i].locale == "en_US" || filteredData[i].locale == 'es_US'))
                result.push(filteredData[i]);
    }
    else if (platform == "Viber" && gender == "all" && country == "united_states") {
        for (var i = 0; i < filteredData.length; i++)
            if (filteredData[i].uid.indexOf("==") != -1 && (filteredData[i].locale == "en_US" || filteredData[i].locale == 'es_US'))
                result.push(filteredData[i]);
    }
    else if (platform == "all" && gender == "male" && country == "united_states")
        raw = Tracks.find({$and: [{createdAt: {$lt: dateToShow}}, {campaignName: {$in: campaignNames}}, {$or: [{gender: 'male'}, {$and: [{repeatView: '0'}, {gender: {$exists: false}}]}]}, {$or: [{locale: {$exists: false}}, {locale: 'en_US'}, {locale: 'es_US'}]}]}, {limit: 20, sort: {createdAt: -1}}).fetch();
    else if (platform == "all" && gender == "female" && country == "united_states")
        raw = Tracks.find({$and: [{createdAt: {$lt: dateToShow}}, {campaignName: {$in: campaignNames}}, {$or: [{gender: 'female'}, {$and: [{repeatView: {$gt: '0'}}, {gender: {$exists: false}}]}]}, {$or: [{locale: {$exists: false}}, {locale: 'en_US'}, {locale: 'es_US'}]}]}, {limit: 20, sort: {createdAt: -1}}).fetch();
    else if (platform == "Facebook" && gender == "male" && country == "united_states")
        raw = Tracks.find({$and: [{createdAt: {$lt: dateToShow}}, {campaignName: {$in: campaignNames}}, {uid:{$not: /==/}}, {$or: [{gender: 'male'}, {$and: [{repeatView: '0'}, {gender: {$exists: false}}]}]}, {$or: [{locale: {$exists: false}}, {locale: 'en_US'}, {locale: 'es_US'}]}]}, {limit: 20, sort: {createdAt: -1}}).fetch();
    else if (platform == "Facebook" && gender == "female" && country == "united_states")
        raw = Tracks.find({$and: [{createdAt: {$lt: dateToShow}}, {campaignName: {$in: campaignNames}}, {uid:{$not: /==/}}, {$or: [{gender: 'female'}, {$and: [{repeatView: {$gt: '0'}}, {gender: {$exists: false}}]}]}, {$or: [{locale: {$exists: false}}, {locale: 'en_US'}, {locale: 'es_US'}]}]}, {limit: 20, sort: {createdAt: -1}}).fetch();
    else if (platform == "Viber" && gender == "male" && country == "united_states")
        raw = Tracks.find({$and: [{createdAt: {$lt: dateToShow}}, {campaignName: {$in: campaignNames}}, {uid: /==/}, {$or: [{gender: 'male'}, {$and: [{repeatView: '0'}, {gender: {$exists: false}}]}]}, {$or: [{locale: {$exists: false}}, {locale: 'en_US'}, {locale: 'es_US'}]}]}, {limit: 20, sort: {createdAt: -1}}).fetch();
    else if (platform == "Viber" && gender == "female" && country == "united_states")
        raw = Tracks.find({$and: [{createdAt: {$lt: dateToShow}}, {campaignName: {$in: campaignNames}}, {uid: /==/}, {$or: [{gender: 'female'}, {$and: [{repeatView: {$gt: '0'}}, {gender: {$exists: false}}]}]}, {$or: [{locale: {$exists: false}}, {locale: 'en_US'}, {locale: 'es_US'}]}]}, {limit: 20, sort: {createdAt: -1}}).fetch();
    else if (platform == "all" && gender == "all" && country == "united_kingdom")
        raw = Tracks.find({$and: [{createdAt: {$lt: dateToShow}}, {campaignName: {$in: campaignNames}}, {locale: 'en_GB'}]}, {limit: 20, sort: {createdAt: -1}}).fetch();
    else if (platform == "Facebook" && gender == "all" && country == "united_kingdom")
        raw = Tracks.find({$and: [{createdAt: {$lt: dateToShow}}, {campaignName: {$in: campaignNames}}, {uid:{$not: /==/}}, {locale: 'en_GB'}]}, {limit: 20, sort: {createdAt: -1}}).fetch();
    else if (platform == "Viber" && gender == "all" && country == "united_kingdom")
        raw = Tracks.find({$and: [{createdAt: {$lt: dateToShow}}, {campaignName: {$in: campaignNames}}, {uid: /==/}, {locale: 'en_GB'}]}, {limit: 20, sort: {createdAt: -1}}).fetch();
    else if (platform == "all" && gender == "male" && country == "united_kingdom")
        raw = Tracks.find({$and: [{createdAt: {$lt: dateToShow}}, {campaignName: {$in: campaignNames}}, {$or: [{gender: 'male'}, {$and: [{repeatView: '0'}, {gender: {$exists: false}}]}]}, {locale: 'en_GB'}]}, {limit: 20, sort: {createdAt: -1}}).fetch();
    else if (platform == "all" && gender == "female" && country == "united_kingdom")
        raw = Tracks.find({$and: [{createdAt: {$lt: dateToShow}}, {campaignName: {$in: campaignNames}}, {$or: [{gender: 'female'}, {$and: [{repeatView: {$gt: '0'}}, {gender: {$exists: false}}]}]}, {locale: 'en_GB'}]}, {limit: 20, sort: {createdAt: -1}}).fetch();
    else if (platform == "Facebook" && gender == "male" && country == "united_kingdom")
        raw = Tracks.find({$and: [{createdAt: {$lt: dateToShow}}, {campaignName: {$in: campaignNames}}, {uid:{$not: /==/}}, {$or: [{gender: 'male'}, {$and: [{repeatView: '0'}, {gender: {$exists: false}}]}]}, {locale: 'en_GB'}]}, {limit: 20, sort: {createdAt: -1}}).fetch();
    else if (platform == "Facebook" && gender == "female" && country == "united_kingdom")
        raw = Tracks.find({$and: [{createdAt: {$lt: dateToShow}}, {campaignName: {$in: campaignNames}}, {uid:{$not: /==/}}, {$or: [{gender: 'female'}, {$and: [{repeatView: {$gt: '0'}}, {gender: {$exists: false}}]}]}, {locale: 'en_GB'}]}, {limit: 20, sort: {createdAt: -1}}).fetch();
    else if (platform == "Viber" && gender == "male" && country == "united_kingdom")
        raw = Tracks.find({$and: [{createdAt: {$lt: dateToShow}}, {campaignName: {$in: campaignNames}}, {uid: /==/}, {$or: [{gender: 'male'}, {$and: [{repeatView: '0'}, {gender: {$exists: false}}]}]}, {locale: 'en_GB'}]}, {limit: 20, sort: {createdAt: -1}}).fetch();
    else if (platform == "Viber" && gender == "female" && country == "united_kingdom")
        raw = Tracks.find({$and: [{createdAt: {$lt: dateToShow}}, {campaignName: {$in: campaignNames}}, {uid: /==/}, {$or: [{gender: 'female'}, {$and: [{repeatView: {$gt: '0'}}, {gender: {$exists: false}}]}]}, {locale: 'en_GB'}]}, {limit: 20, sort: {createdAt: -1}}).fetch();
    else if (platform == "all" && gender == "all" && country == "canada")
        raw = Tracks.find({$and: [{createdAt: {$lt: dateToShow}}, {campaignName: {$in: campaignNames}}, {$or: [{locale: 'en_CA'}, {locale: 'fr_CA'}]}]}, {limit: 20, sort: {createdAt: -1}}).fetch();
    else if (platform == "Facebook" && gender == "all" && country == "canada")
        raw = Tracks.find({$and: [{createdAt: {$lt: dateToShow}}, {campaignName: {$in: campaignNames}}, {uid:{$not: /==/}}, {$or: [{locale: 'en_CA'}, {locale: 'fr_CA'}]}]}, {limit: 20, sort: {createdAt: -1}}).fetch();
    else if (platform == "Viber" && gender == "all" && country == "canada")
        raw = Tracks.find({$and: [{createdAt: {$lt: dateToShow}}, {campaignName: {$in: campaignNames}}, {uid: /==/}, {$or: [{locale: 'en_CA'}, {locale: 'fr_CA'}]}]}, {limit: 20, sort: {createdAt: -1}}).fetch();
    else if (platform == "all" && gender == "male" && country == "canada")
        raw = Tracks.find({$and: [{createdAt: {$lt: dateToShow}}, {campaignName: {$in: campaignNames}}, {$or: [{gender: 'male'}, {$and: [{repeatView: '0'}, {gender: {$exists: false}}]}]}, {$or: [{locale: 'en_CA'}, {locale: 'fr_CA'}]}]}, {limit: 20, sort: {createdAt: -1}}).fetch();
    else if (platform == "all" && gender == "female" && country == "canada")
        raw = Tracks.find({$and: [{createdAt: {$lt: dateToShow}}, {campaignName: {$in: campaignNames}}, {$or: [{gender: 'female'}, {$and: [{repeatView: {$gt: '0'}}, {gender: {$exists: false}}]}]}, {$or: [{locale: 'en_CA'}, {locale: 'fr_CA'}]}]}, {limit: 20, sort: {createdAt: -1}}).fetch();
    else if (platform == "Facebook" && gender == "male" && country == "canada")
        raw = Tracks.find({$and: [{createdAt: {$lt: dateToShow}}, {campaignName: {$in: campaignNames}}, {uid:{$not: /==/}}, {$or: [{gender: 'male'}, {$and: [{repeatView: '0'}, {gender: {$exists: false}}]}]}, {$or: [{locale: 'en_CA'}, {locale: 'fr_CA'}]}]}, {limit: 20, sort: {createdAt: -1}}).fetch();
    else if (platform == "Facebook" && gender == "female" && country == "canada")
        raw = Tracks.find({$and: [{createdAt: {$lt: dateToShow}}, {campaignName: {$in: campaignNames}}, {uid:{$not: /==/}}, {$or: [{gender: 'female'}, {$and: [{repeatView: {$gt: '0'}}, {gender: {$exists: false}}]}]}, {$or: [{locale: 'en_CA'}, {locale: 'fr_CA'}]}]}, {limit: 20, sort: {createdAt: -1}}).fetch();
    else if (platform == "Viber" && gender == "male" && country == "canada")
        raw = Tracks.find({$and: [{createdAt: {$lt: dateToShow}}, {campaignName: {$in: campaignNames}}, {uid: /==/}, {$or: [{gender: 'male'}, {$and: [{repeatView: '0'}, {gender: {$exists: false}}]}]}, {$or: [{locale: 'en_CA'}, {locale: 'fr_CA'}]}]}, {limit: 20, sort: {createdAt: -1}}).fetch();
    else if (platform == "Viber" && gender == "female" && country == "canada")
        raw = Tracks.find({$and: [{createdAt: {$lt: dateToShow}}, {campaignName: {$in: campaignNames}}, {uid: /==/}, {$or: [{gender: 'female'}, {$and: [{repeatView: {$gt: '0'}}, {gender: {$exists: false}}]}]}, {$or: [{locale: 'en_CA'}, {locale: 'fr_CA'}]}]}, {limit: 20, sort: {createdAt: -1}}).fetch();
    else if (platform == "all" && gender == "all" && country == "france")
        raw = Tracks.find({$and: [{createdAt: {$lt: dateToShow}}, {campaignName: {$in: campaignNames}}, {locale: 'fr_FR'}]}, {limit: 20, sort: {createdAt: -1}}).fetch();
    else if (platform == "Facebook" && gender == "all" && country == "france")
        raw = Tracks.find({$and: [{createdAt: {$lt: dateToShow}}, {campaignName: {$in: campaignNames}}, {uid:{$not: /==/}}, {locale: 'fr_FR'}]}, {limit: 20, sort: {createdAt: -1}}).fetch();
    else if (platform == "Viber" && gender == "all" && country == "france")
        raw = Tracks.find({$and: [{createdAt: {$lt: dateToShow}}, {campaignName: {$in: campaignNames}}, {uid: /==/}, {locale: 'fr_FR'}]}, {limit: 20, sort: {createdAt: -1}}).fetch();
    else if (platform == "all" && gender == "male" && country == "france")
        raw = Tracks.find({$and: [{createdAt: {$lt: dateToShow}}, {campaignName: {$in: campaignNames}}, {$or: [{gender: 'male'}, {$and: [{repeatView: '0'}, {gender: {$exists: false}}]}]}, {locale: 'fr_FR'}]}, {limit: 20, sort: {createdAt: -1}}).fetch();
    else if (platform == "all" && gender == "female" && country == "france")
        raw = Tracks.find({$and: [{createdAt: {$lt: dateToShow}}, {campaignName: {$in: campaignNames}}, {$or: [{gender: 'female'}, {$and: [{repeatView: {$gt: '0'}}, {gender: {$exists: false}}]}]}, {locale: 'fr_FR'}]}, {limit: 20, sort: {createdAt: -1}}).fetch();
    else if (platform == "Facebook" && gender == "male" && country == "france")
        raw = Tracks.find({$and: [{createdAt: {$lt: dateToShow}}, {campaignName: {$in: campaignNames}}, {uid:{$not: /==/}}, {$or: [{gender: 'male'}, {$and: [{repeatView: '0'}, {gender: {$exists: false}}]}]}, {locale: 'fr_FR'}]}, {limit: 20, sort: {createdAt: -1}}).fetch();
    else if (platform == "Facebook" && gender == "female" && country == "france")
        raw = Tracks.find({$and: [{createdAt: {$lt: dateToShow}}, {campaignName: {$in: campaignNames}}, {uid:{$not: /==/}}, {$or: [{gender: 'female'}, {$and: [{repeatView: {$gt: '0'}}, {gender: {$exists: false}}]}]}, {locale: 'fr_FR'}]}, {limit: 20, sort: {createdAt: -1}}).fetch();
    else if (platform == "Viber" && gender == "male" && country == "france")
        raw = Tracks.find({$and: [{createdAt: {$lt: dateToShow}}, {campaignName: {$in: campaignNames}}, {uid: /==/}, {$or: [{gender: 'male'}, {$and: [{repeatView: '0'}, {gender: {$exists: false}}]}]}, {locale: 'fr_FR'}]}, {limit: 20, sort: {createdAt: -1}}).fetch();
    else if (platform == "Viber" && gender == "female" && country == "france")
        raw = Tracks.find({$and: [{createdAt: {$lt: dateToShow}}, {campaignName: {$in: campaignNames}}, {uid: /==/}, {$or: [{gender: 'female'}, {$and: [{repeatView: {$gt: '0'}}, {gender: {$exists: false}}]}]}, {locale: 'fr_FR'}]}, {limit: 20, sort: {createdAt: -1}}).fetch();
    else if (platform == "all" && gender == "all" && country == "germany")
        raw = Tracks.find({$and: [{createdAt: {$lt: dateToShow}}, {campaignName: {$in: campaignNames}}, {locale: 'de_DE'}]}, {limit: 20, sort: {createdAt: -1}}).fetch();
    else if (platform == "Facebook" && gender == "all" && country == "germany")
        raw = Tracks.find({$and: [{createdAt: {$lt: dateToShow}}, {campaignName: {$in: campaignNames}}, {uid:{$not: /==/}}, {locale: 'de_DE'}]}, {limit: 20, sort: {createdAt: -1}}).fetch();
    else if (platform == "Viber" && gender == "all" && country == "germany")
        raw = Tracks.find({$and: [{createdAt: {$lt: dateToShow}}, {campaignName: {$in: campaignNames}}, {uid: /==/}, {locale: 'de_DE'}]}, {limit: 20, sort: {createdAt: -1}}).fetch();
    else if (platform == "all" && gender == "male" && country == "germany")
        raw = Tracks.find({$and: [{createdAt: {$lt: dateToShow}}, {campaignName: {$in: campaignNames}}, {$or: [{gender: 'male'}, {$and: [{repeatView: '0'}, {gender: {$exists: false}}]}]}, {locale: 'de_DE'}]}, {limit: 20, sort: {createdAt: -1}}).fetch();
    else if (platform == "all" && gender == "female" && country == "germany")
        raw = Tracks.find({$and: [{createdAt: {$lt: dateToShow}}, {campaignName: {$in: campaignNames}}, {$or: [{gender: 'female'}, {$and: [{repeatView: {$gt: '0'}}, {gender: {$exists: false}}]}]}, {locale: 'de_DE'}]}, {limit: 20, sort: {createdAt: -1}}).fetch();
    else if (platform == "Facebook" && gender == "male" && country == "germany")
        raw = Tracks.find({$and: [{createdAt: {$lt: dateToShow}}, {campaignName: {$in: campaignNames}}, {uid:{$not: /==/}}, {$or: [{gender: 'male'}, {$and: [{repeatView: '0'}, {gender: {$exists: false}}]}]}, {locale: 'de_DE'}]}, {limit: 20, sort: {createdAt: -1}}).fetch();
    else if (platform == "Facebook" && gender == "female" && country == "germany")
        raw = Tracks.find({$and: [{createdAt: {$lt: dateToShow}}, {campaignName: {$in: campaignNames}}, {uid:{$not: /==/}}, {$or: [{gender: 'female'}, {$and: [{repeatView: {$gt: '0'}}, {gender: {$exists: false}}]}]}, {locale: 'de_DE'}]}, {limit: 20, sort: {createdAt: -1}}).fetch();
    else if (platform == "Viber" && gender == "male" && country == "germany")
        raw = Tracks.find({$and: [{createdAt: {$lt: dateToShow}}, {campaignName: {$in: campaignNames}}, {uid: /==/}, {$or: [{gender: 'male'}, {$and: [{repeatView: '0'}, {gender: {$exists: false}}]}]}, {locale: 'de_DE'}]}, {limit: 20, sort: {createdAt: -1}}).fetch();
    else if (platform == "Viber" && gender == "female" && country == "germany")
        raw = Tracks.find({$and: [{createdAt: {$lt: dateToShow}}, {campaignName: {$in: campaignNames}}, {uid: /==/}, {$or: [{gender: 'female'}, {$and: [{repeatView: {$gt: '0'}}, {gender: {$exists: false}}]}]}, {locale: 'de_DE'}]}, {limit: 20, sort: {createdAt: -1}}).fetch();

    return result.slice(currentRecord, currentRecord + 20);
};


Template.UsersListView.rendered = function() {
	pageSession.set("UsersListViewStyle", "table");
	
};

Template.UsersListView.events({
});

Template.UsersListView.helpers({
	"isEmpty": function() {
		return !this.tracks || this.tracks.count() == 0;
	},
	"isNotEmpty": function() {
		return this.tracks && this.tracks.count() > 0;
	},
	"isNotFound": function() {
		return this.tracks && pageSession.get("UsersListViewSearchString") && UsersListViewItems(this.tracks).length == 0;
	},
	"searchString": function() {
		return pageSession.get("UsersListsViewSearchString");
	},
	"viewAsTable": function() {
		return pageSession.get("UsersListViewStyle") == "table";
	},
	"viewAsList": function() {
		return pageSession.get("UsersListViewStyle") == "list";
	},
	"viewAsGallery": function() {
		return pageSession.get("UsersListViewStyle") == "gallery";
	}
});


Template.UsersListViewTable.rendered = function() {
	
};

var index = 0;

Template.UsersListViewTable.events({
    "click #next-page-button": function(e, t) {
        e.preventDefault();
        if(index == NumberOfUsers())
            return;
        pageSession.set("dateToShow", new Date());
        var currentRecord = pageSession.get("currentRecord");
        if (currentRecord == undefined)
            currentRecord = 0;
        pageSession.set("currentRecord", currentRecord + 20);
    },
    "click #previous-page-button": function(e, t) {
        e.preventDefault();
        if (index <= 20)
            return;
        var numberOfUsers = NumberOfUsers();
        if (index == numberOfUsers)
            index -= numberOfUsers % 20 + 20;
        else
            index -= 40;
        pageSession.set("dateToShow", new Date());
        var currentRecord = pageSession.get("currentRecord");
        if (currentRecord == undefined || currentRecord < 20)
            return;
        pageSession.set("currentRecord", currentRecord - 20);
    },
    "change #platform": function(e, t) {
        index = 0;
        var selectedValue = t.$("#platform").val();
        if (selectedValue == "Facebook")
           platform = "Facebook";
        else if (selectedValue == "Viber")
            platform = "Viber";
        else
            platform = "all";
        pageSession.set("dateToShow", new Date());
        pageSession.set("currentRecord", 0);
    },
    "change #gender": function(e, t) {
        index = 0;
        var selectedValue = t.$("#gender").val();
        if (selectedValue == "Male")
            gender = "male";
        else if (selectedValue == "Female")
            gender = "female";
        else
            gender = "all";
        pageSession.set("dateToShow", new Date());
        pageSession.set("currentRecord", 0);
    },
    "change #country": function(e, t) {
        index = 0;
        country = t.$("#country").val();
        pageSession.set("dateToShow", new Date());
        pageSession.set("currentRecord", 0);
    }
});

Template.UsersListViewTable.helpers({
	"tableItems": function() {
		return UsersListViewItems();
	},
    "numberOfUsers": function() {
	    return NumberOfUsers();
    }
});


Template.UsersListViewTableItems.rendered = function() {
	
};

Template.UsersListViewTableItems.events({
	"click td": function(e, t) {
        e.preventDefault();
        var me = this;
        var platform = findPlatform(t.data.uid);
        var country = localeToCountry(t.data.locale);
        var interactions = findInteractions(t.data.message);
        var gender = findGender(t.data.gender, t.data.repeatView);
        var tracking = convertTracking(t.data.message);
        var urls = getConversionsUrls(t.data.uid);
        var subscription =findSubscription(t.data.uid);
        var name = getName(t.data.firstName, t.data.lastName);
        bootbox.dialog({
            title: "<font color=\"black\">User Details</font>",
            message: "<br><br><font size=\"4\"><b>Personal Information</b></font><br><br><b>Name:</b> " + name + "<br><br><b>Gender:</b> " + gender + "<br><br><b>Country:</b> " + country + "<br><br><br><br><font size=\"4\"><b>User Engagement</b></font><br><br><b>Platform:</b> " + platform + "<br><br><b>UID:</b> " + t.data.uid + "<br><br><b>Campaign Name:</b> " + t.data.campaignName + "<br><br><b>Number of Clicks:</b> " + interactions + "<br><br><b>Tracking:</b> " + tracking + "<br><br><b>Conversion Links:</b> " + urls + "<br><br><b>Subscription:</b> " + subscription + "<br><br><b>Input:</b> ",
            animate: false,
            buttons: {
                success: {
                    label: "Close",
                    className: "btn-primary",
                }
            }
        });
        return false;
	},

	"click #view-button": function(e, t) {
		e.preventDefault();
		var me = this;
        var platform = findPlatform(t.data.uid);
        var country = localeToCountry(t.data.locale);
        var interactions = findInteractions(t.data.message);
        var gender = findGender(t.data.gender, t.data.repeatView);
        var tracking = convertTracking(t.data.message);
        var urls = getConversionsUrls(t.data.uid);
        var subscription =findSubscription(t.data.uid);
        var name = getName(t.data.firstName, t.data.lastName);
		bootbox.dialog({
            title: "<font color=\"black\">User Details</font>",
            message: "<br><br><font size=\"4\"><b>Personal Information</b></font><br><br><b>Name:</b> " + name + "<br><br><b>Gender:</b> " + gender + "<br><br><b>Country:</b> " + country + "<br><br><br><br><font size=\"4\"><b>User Engagement</b></font><br><br><b>Platform:</b> " + platform + "<br><br><b>UID:</b> " + t.data.uid + "<br><br><b>Campaign Name:</b> " + t.data.campaignName + "<br><br><b>Number of Clicks:</b> " + interactions + "<br><br><b>Tracking:</b> " + tracking + "<br><br><b>Conversion Links:</b> " + urls + "<br><br><b>Subscription:</b> " + subscription + "<br><br><b>Input:</b> ",
			animate: false,
			buttons: {
				success: {
					label: "Close",
					className: "btn-primary",
				}
			}
		});
		return false;
	}

});

Template.UsersListViewTableItems.helpers({

    "index" : function() { return ++index; },

    "findGender": function(gender, message) { return findGender(gender, message); },

    "findPlatform": function(uid) { return findPlatform(uid); },

    "localeToCountry": function(locale) { return localeToCountry(locale); },

    "findInteractions" : function(message) { return findInteractions(message); },

    "getName" : function(firstName, lastName) { return getName(firstName, lastName); }

});