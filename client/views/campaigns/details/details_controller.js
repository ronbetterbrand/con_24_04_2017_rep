
this.CampaignsDetailsController = RouteController.extend({
	template: "CampaignsDetails",
	

	yieldTemplates: {
		/*YIELD_TEMPLATES*/
	},

	onBeforeAction: function() {


		this.next();
	},

	action: function() {

		var spinner;

		if( this.isReady() )
		{
			this.render();
			console.log("2finish loading...");

		}
		else
		{
			this.render("loading");
			var target = document.getElementById('content');
			spinner = new Spinner().spin(target);
			setTimeout(function(){
				spinner.stop();
			}, 1000);
			console.log("loading...");
		}
		/*ACTION_FUNCTION*/
	},

	isReady: function() {
		

		var subs = [
			Meteor.subscribe("campaign", this.params.campaignId),
			//Meteor.subscribe("tracks"),
			//Meteor.subscribe("proxytracks"),
			Meteor.subscribe("analytics")
		];
		var ready = true;
		_.each(subs, function(sub) {
			if(!sub.ready())
				ready = false;
		});
		return ready;
	},

	data: function() {
		

		return {
			params: this.params || {},
			campaign: Campaigns.findOne( {_id:this.params.campaignId}, {} ),
			//tracks: Tracks.find({}, {}),
			//proxytracks: ProxyTracks.find({}, {}),
            analytics: Analytics.find({}, {})
		};
		/*DATA_FUNCTION*/
	},

	onAfterAction: function() {
		
	}
});