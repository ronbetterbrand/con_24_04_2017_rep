var pageSession = new ReactiveDict();

var labelsDay = function () {
    return ["00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23"]
}
var labelsWeek = function () {
    var array = [];
    var currentDate = new Date();
    var date = new Date();
    var currentDay = currentDate.getDay();
    var dateOfSunday = currentDate.getDate() - currentDay;
    var day = new Date(date.setDate(dateOfSunday)).getDate();
    array.push("Sunday " + day + ordinalNumber(day));
    date = new Date();
    day = new Date(date.setDate(dateOfSunday + 1)).getDate();
    array.push("Monday " + day + ordinalNumber(day));
    date = new Date();
    day = new Date(date.setDate(dateOfSunday + 2)).getDate();
    array.push("Tuesday " + day + ordinalNumber(day));
    date = new Date();
    day = new Date(date.setDate(dateOfSunday + 3)).getDate();
    array.push("Wednesday " + day + ordinalNumber(day));
    date = new Date();
    day = new Date(date.setDate(dateOfSunday + 4)).getDate();
    array.push("Thursday " + day + ordinalNumber(day));
    date = new Date();
    day = new Date(date.setDate(dateOfSunday + 5)).getDate();
    array.push("Friday " + day + ordinalNumber(day));
    date = new Date();
    day = new Date(date.setDate(dateOfSunday + 6)).getDate();
    array.push("Saturday " + day + ordinalNumber(day));

    return array;
}

var labelsMonth = function () {
    var array = [];
    var currentDate = new Date();
    var numberOfDays = new Date(currentDate.getFullYear(), currentDate.getMonth()+1, 0).getDate();

    for (var i=1; i<=numberOfDays; i++)
        array.push(i);

    return array;
}
var ordinalNumber = function(day) {
    if (day==1 || day==21 || day==31)
        return "st";
    else if(day==2 || day==22)
        return "nd";
    else if(day==3 || day==23)
        return "rd";
    else
        return "th";
}

var Views = function(time, thisCampaignName) {
    var views = 0;
    if (time == 'Today') {
        views = Analytics.find({campaignName: thisCampaignName}, {limit: 1}).fetch().map(function (x) {
            return x.viewsDay;
        });
    }
    else if (time == 'Yesterday') {
        views = Analytics.find({campaignName: thisCampaignName}, {limit: 1}).fetch().map(function (x) {
            return x.viewsPreviousDay;
        });
    }
    else if (time == 'ThisWeek') {
        views = Analytics.find({campaignName: thisCampaignName}, {limit: 1}).fetch().map(function (x) {
            return x.viewsWeek;
        });
    }
    else if (time == 'LastWeek') {
        views = Analytics.find({campaignName: thisCampaignName}, {limit: 1}).fetch().map(function (x) {
            return x.viewsPreviousWeek;
        });
    }
    else if (time == 'ThisMonth') {
        views = Analytics.find({campaignName: thisCampaignName}, {limit: 1}).fetch().map(function (x) {
            return x.viewsMonth;
        });
    }
    else if (time == 'LastMonth') {
        views = Analytics.find({campaignName: thisCampaignName}, {limit: 1}).fetch().map(function (x) {
            return x.viewsPreviousMonth;
        });
    }
    else if (time == 'All') {
        views = Analytics.find({campaignName: thisCampaignName}, {limit: 1}).fetch().map(function (x) {
            return x.views;
        });
    }

    return views;
}

var ViewsViber = function(time, thisCampaignName) {
    var views = 0;
    if (time == 'Today') {
        views = Analytics.find({campaignName: thisCampaignName}, {limit: 1}).fetch().map(function (x) {
            return Math.round(x.viewsViberDay + x.uniqueUsersViber / 50 / 7 / 4 * 5);
        });
    }
    else if (time == 'Yesterday') {
        views = Analytics.find({campaignName: thisCampaignName}, {limit: 1}).fetch().map(function (x) {
            return Math.round(x.viewsViberPreviousDay + x.uniqueUsersViber / 50 / 7 / 4 * 5);
        });
    }
    else if (time == 'ThisWeek') {
        views = Analytics.find({campaignName: thisCampaignName}, {limit: 1}).fetch().map(function (x) {
            return Math.round(x.viewsViberWeek + x.uniqueUsersViber / 50 / 4 * 5);
        });
    }
    else if (time == 'LastWeek') {
        views = Analytics.find({campaignName: thisCampaignName}, {limit: 1}).fetch().map(function (x) {
            return Math.round(x.viewsViberPreviousWeek + x.uniqueUsersViber / 50 / 4 * 5);
        });
    }
    else if (time == 'ThisMonth') {
        views = Analytics.find({campaignName: thisCampaignName}, {limit: 1}).fetch().map(function (x) {
            return Math.round(x.viewsViberMonth + x.uniqueUsersViber / 50 * 5);
        });
    }
    else if (time == 'LastMonth') {
        views = Analytics.find({campaignName: thisCampaignName}, {limit: 1}).fetch().map(function (x) {
            return Math.round(x.viewsViberPreviousMonth + x.uniqueUsersViber / 50 * 5);
        });
    }
    else if (time == 'All') {
        views = Analytics.find({campaignName: thisCampaignName}, {limit: 1}).fetch().map(function (x) {
            return x.viewsViber + x.uniqueUsersViber * 5;
        });
    }

    return views;
}

var Interactions = function(time, thisCampaignName) {
    var interactions = 0;
    if (time == 'Today') {
        interactions = Analytics.find({campaignName: thisCampaignName}, {limit: 1}).fetch().map(function (x) {
            return Math.round(x.uniqueUsersDay * 0.07);

        });
    }
    else if (time == 'Yesterday') {
        interactions = Analytics.find({campaignName: thisCampaignName}, {limit: 1}).fetch().map(function (x) {
            return Math.round(x.uniqueUsersPreviousDay * 0.07);
        });
    }
    else if (time == 'ThisWeek') {
        interactions = Analytics.find({campaignName: thisCampaignName}, {limit: 1}).fetch().map(function (x) {
            return Math.round(x.uniqueUsersWeek * 0.07);
        });
    }
    else if (time == 'LastWeek') {
        interactions = Analytics.find({campaignName: thisCampaignName}, {limit: 1}).fetch().map(function (x) {
            return Math.round(x.uniqueUsersPreviousWeek * 0.07);
        });
    }
    else if (time == 'ThisMonth') {
        interactions = Analytics.find({campaignName: thisCampaignName}, {limit: 1}).fetch().map(function (x) {
            return Math.round(x.uniqueUsersMonth * 0.07);
        });
    }
    else if (time == 'LastMonth') {
        interactions = Analytics.find({campaignName: thisCampaignName}, {limit: 1}).fetch().map(function (x) {
            return Math.round(x.uniqueUsersPreviousMonth * 0.07);
        });
    }
    else if (time == 'All') {
        interactions = Analytics.find({campaignName: thisCampaignName}, {limit: 1}).fetch().map(function (x) {
            return Math.round(x.uniqueUsers * 0.07);
        });
    }

    return interactions;
}

var InteractionsViber = function(time, thisCampaignName) {
    var interactions = 0;
    if (time == 'Today') {
        interactions = Analytics.find({campaignName: thisCampaignName}, {limit: 1}).fetch().map(function (x) {
            return Math.round(x.uniqueUsersViber / 50 / 7 / 4 * 0.07);
        });
    }
    else if (time == 'Yesterday') {
        interactions = Analytics.find({campaignName: thisCampaignName}, {limit: 1}).fetch().map(function (x) {
            return Math.round(x.uniqueUsersViber / 50 / 7 / 4 * 0.07);
        });
    }
    else if (time == 'ThisWeek') {
        interactions = Analytics.find({campaignName: thisCampaignName}, {limit: 1}).fetch().map(function (x) {
            return Math.round(x.uniqueUsersViber / 50 / 4 * 0.07);
        });
    }
    else if (time == 'LastWeek') {
        interactions = Analytics.find({campaignName: thisCampaignName}, {limit: 1}).fetch().map(function (x) {
            return Math.round(x.uniqueUsersViber / 50 / 4 * 0.07);
        });
    }
    else if (time == 'ThisMonth') {
        interactions = Analytics.find({campaignName: thisCampaignName}, {limit: 1}).fetch().map(function (x) {
            return Math.round(x.uniqueUsersViber / 50 * 0.07);
        });
    }
    else if (time == 'LastMonth') {
        interactions = Analytics.find({campaignName: thisCampaignName}, {limit: 1}).fetch().map(function (x) {
            return Math.round(x.uniqueUsersViber / 50 * 0.07);
        });
    }
    else if (time == 'All') {
        interactions = Analytics.find({campaignName: thisCampaignName}, {limit: 1}).fetch().map(function (x) {
            return Math.round(x.uniqueUsersViber * 0.07);
        });
    }

    return interactions;
}

var UniqueUsers = function(time, thisCampaignName ) {
    var uniqueUsers = 0;
    if (time == 'Today') {
        uniqueUsers = Analytics.find({campaignName: thisCampaignName}, {limit: 1}).fetch().map(function (x) {
            return x.uniqueUsersDay;
        });
    }
    if (time == 'Yesterday') {
        uniqueUsers = Analytics.find({campaignName: thisCampaignName}, {limit: 1}).fetch().map(function (x) {
            return x.uniqueUsersPreviousDay;
        });
    }
    else if (time == 'ThisWeek') {
        uniqueUsers = Analytics.find({campaignName: thisCampaignName}, {limit: 1}).fetch().map(function (x) {
            return x.uniqueUsersWeek;
        });
    }
    else if (time == 'LastWeek') {
        uniqueUsers = Analytics.find({campaignName: thisCampaignName}, {limit: 1}).fetch().map(function (x) {
            return x.uniqueUsersPreviousWeek;
        });
    }
    else if (time == 'ThisMonth') {
        uniqueUsers = Analytics.find({campaignName: thisCampaignName}, {limit: 1}).fetch().map(function (x) {
            return x.uniqueUsersMonth;
        });
    }
    else if (time == 'LastMonth') {
        uniqueUsers = Analytics.find({campaignName: thisCampaignName}, {limit: 1}).fetch().map(function (x) {
            return x.uniqueUsersPreviousMonth;
        });
    }
    else if (time == 'All') {
        uniqueUsers = Analytics.find({campaignName: thisCampaignName}, {limit: 1}).fetch().map(function (x) {
            return x.uniqueUsers;
        });
    }

    return uniqueUsers;
}

var UniqueUsersViber = function(time, thisCampaignName ) {
    var uniqueUsers = 0;
    if (time == 'Today') {
        uniqueUsers = Analytics.find({campaignName: thisCampaignName}, {limit: 1}).fetch().map(function (x) {
            return Math.round(x.uniqueUsersViberDay + x.uniqueUsersViber/ 50 / 7 / 4);
        });
    }
    if (time == 'Yesterday') {
        uniqueUsers = Analytics.find({campaignName: thisCampaignName}, {limit: 1}).fetch().map(function (x) {
            return Math.round(x.uniqueUsersViberPreviousDay + x.uniqueUsersViber/ 50/ 7 / 4);
        });
    }
    else if (time == 'ThisWeek') {
        uniqueUsers = Analytics.find({campaignName: thisCampaignName}, {limit: 1}).fetch().map(function (x) {
            return Math.round(x.uniqueUsersViberWeek + x.uniqueUsersViber / 50 / 4).toFixed(2);
        });
    }
    else if (time == 'LastWeek') {
        uniqueUsers = Analytics.find({campaignName: thisCampaignName}, {limit: 1}).fetch().map(function (x) {
            return Math.round(x.uniqueUsersViberPreviousWeek + x.uniqueUsersViber / 50 / 4);
        });
    }
    else if (time == 'ThisMonth') {
        uniqueUsers = Analytics.find({campaignName: thisCampaignName}, {limit: 1}).fetch().map(function (x) {
            return Math.round(x.uniqueUsersViberMonth + x.uniqueUsersViber / 50);
        });
    }
    else if (time == 'LastMonth') {
        uniqueUsers = Analytics.find({campaignName: thisCampaignName}, {limit: 1}).fetch().map(function (x) {
            return Math.round(x.uniqueUsersViberPreviousMonth + x.uniqueUsersViber / 50);
        });
    }
    else if (time == 'All') {
        uniqueUsers = Analytics.find({campaignName: thisCampaignName}, {limit: 1}).fetch().map(function (x) {
            return x.uniqueUsersViber;
        });
    }

    return uniqueUsers;
}

var RepeatedUsers = function(time, thisCampaignName){
    var repeatedUsers = 0;
    if (time == 'Today') {
        repeatedUsers = Analytics.find({campaignName: thisCampaignName}, {limit: 1}).fetch().map(function (x) {
            return x.repeatedUsersDay;
        });
    }
    if (time == 'Yesterday') {
        repeatedUsers = Analytics.find({campaignName: thisCampaignName}, {limit: 1}).fetch().map(function (x) {
            return x.repeatedUsersPreviousDay;
        });
    }
    else if (time == 'ThisWeek') {
        repeatedUsers = Analytics.find({campaignName: thisCampaignName}, {limit: 1}).fetch().map(function (x) {
            return x.repeatedUsersWeek;
        });
    }
    else if (time == 'LastWeek') {
        repeatedUsers = Analytics.find({campaignName: thisCampaignName}, {limit: 1}).fetch().map(function (x) {
            return x.repeatedUsersPreviousWeek;
        });
    }
    else if (time == 'ThisMonth') {
        repeatedUsers = Analytics.find({campaignName: thisCampaignName}, {limit: 1}).fetch().map(function (x) {
            return x.repeatedUsersMonth;
        });
    }
    else if (time == 'LastMonth') {
        repeatedUsers = Analytics.find({campaignName: thisCampaignName}, {limit: 1}).fetch().map(function (x) {
            return x.repeatedUsersPreviousMonth;
        });
    }
    else if (time == 'All') {
        repeatedUsers = Analytics.find({campaignName: thisCampaignName}, {limit: 1}).fetch().map(function (x) {
            return x.repeatedUsers;
        });
    }

    return repeatedUsers;
}

var RepeatedUsersViber = function(time, thisCampaignName){
    var repeatedUsers = 0;
    if (time == 'Today') {
        repeatedUsers = Analytics.find({campaignName: thisCampaignName}, {limit: 1}).fetch().map(function (x) {
            return Math.round(x.repeatedUsersViberDay + x.uniqueUsersViber / 50 / 7 / 4 / 2);
        });
    }
    if (time == 'Yesterday') {
        repeatedUsers = Analytics.find({campaignName: thisCampaignName}, {limit: 1}).fetch().map(function (x) {
            return Math.round(x.repeatedUsersViberPreviousDay + x.uniqueUsersViber / 50 / 7 / 4 / 2);
        });
    }
    else if (time == 'ThisWeek') {
        repeatedUsers = Analytics.find({campaignName: thisCampaignName}, {limit: 1}).fetch().map(function (x) {
            return Math.round(x.repeatedUsersViberWeek + x.uniqueUsersViber / 50 / 4 / 2);
        });
    }
    else if (time == 'LastWeek') {
        repeatedUsers = Analytics.find({campaignName: thisCampaignName}, {limit: 1}).fetch().map(function (x) {
            return Math.round(x.repeatedUsersViberPreviousWeek + x.uniqueUsersViber / 50 / 4 / 2);
        });
    }
    else if (time == 'ThisMonth') {
        repeatedUsers = Analytics.find({campaignName: thisCampaignName}, {limit: 1}).fetch().map(function (x) {
            return Math.round(x.repeatedUsersViberMonth + x.uniqueUsersViber / 50 / 2);
        });
    }
    else if (time == 'LastMonth') {
        repeatedUsers = Analytics.find({campaignName: thisCampaignName}, {limit: 1}).fetch().map(function (x) {
            return Math.round(x.repeatedUsersViberPreviousMonth + x.uniqueUsersViber / 50 / 2);
        });
    }
    else if (time == 'All') {
        repeatedUsers = Analytics.find({campaignName: thisCampaignName}, {limit: 1}).fetch().map(function (x) {
            return x.repeatedUsersViber + x.uniqueUsersViber / 2;
        });
    }

    return repeatedUsers;
}

var TimeSpentPerUser = function(time, thisCampaignName) {
    var timeSpentPerUser = 0;
    if (time == 'Today') {
        timeSpentPerUser = Analytics.find({campaignName: thisCampaignName}, {limit: 1}).fetch().map(function (x) {
            return x.timeSpentPerUserDay;
        });
    }
    if (time == 'Yesterday') {
        timeSpentPerUser = Analytics.find({campaignName: thisCampaignName}, {limit: 1}).fetch().map(function (x) {
            return x.timeSpentPerUserPreviousDay;
        });
    }
    else if (time == 'ThisWeek') {
        timeSpentPerUser = Analytics.find({campaignName: thisCampaignName}, {limit: 1}).fetch().map(function (x) {
            return x.timeSpentPerUserWeek;
        });
    }
    else if (time == 'LastWeek') {
        timeSpentPerUser = Analytics.find({campaignName: thisCampaignName}, {limit: 1}).fetch().map(function (x) {
            return x.timeSpentPerUserPreviousWeek;
        });
    }
    else if (time == 'ThisMonth') {
        timeSpentPerUser = Analytics.find({campaignName: thisCampaignName}, {limit: 1}).fetch().map(function (x) {
            return x.timeSpentPerUserMonth;
        });
    }
    else if (time == 'LastMonth') {
        timeSpentPerUser = Analytics.find({campaignName: thisCampaignName}, {limit: 1}).fetch().map(function (x) {
            return x.timeSpentPerUserPreviousMonth;
        });
    }
    else if (time == 'All') {
        timeSpentPerUser = Analytics.find({campaignName: thisCampaignName}, {limit: 1}).fetch().map(function (x) {
            return x.timeSpentPerUser;
        });
    }

    return timeSpentPerUser;
}

var TimeSpentPerUserViber = function(time, thisCampaignName) {
    var timeSpentPerUser = 0;
    if (time == 'Today') {
        timeSpentPerUser = Analytics.find({campaignName: thisCampaignName}, {limit: 1}).fetch().map(function (x) {
            return x.timeSpentPerUserViberDay;
        });
    }
    if (time == 'Yesterday') {
        timeSpentPerUser = Analytics.find({campaignName: thisCampaignName}, {limit: 1}).fetch().map(function (x) {
            return x.timeSpentPerUserViberPreviousDay;
        });
    }
    else if (time == 'ThisWeek') {
        timeSpentPerUser = Analytics.find({campaignName: thisCampaignName}, {limit: 1}).fetch().map(function (x) {
            return x.timeSpentPerUserViberWeek;
        });
    }
    else if (time == 'LastWeek') {
        timeSpentPerUser = Analytics.find({campaignName: thisCampaignName}, {limit: 1}).fetch().map(function (x) {
            return x.timeSpentPerUserViberPreviousWeek;
        });
    }
    else if (time == 'ThisMonth') {
        timeSpentPerUser = Analytics.find({campaignName: thisCampaignName}, {limit: 1}).fetch().map(function (x) {
            return x.timeSpentPerUserViberMonth;
        });
    }
    else if (time == 'LastMonth') {
        timeSpentPerUser = Analytics.find({campaignName: thisCampaignName}, {limit: 1}).fetch().map(function (x) {
            return x.timeSpentPerUserViberPreviousMonth;
        });
    }
    else if (time == 'All') {
        timeSpentPerUser = Analytics.find({campaignName: thisCampaignName}, {limit: 1}).fetch().map(function (x) {
            return x.timeSpentPerUserViber;
        });
    }

    return timeSpentPerUser;
}

var RepeatUsePerUser = function (time, thisCampaignName) {
    var repeatUsePerUser = 0;
    if (time == 'Today') {
        repeatUsePerUser = Analytics.find({campaignName: thisCampaignName}, {limit: 1}).fetch().map(function (x) {
            return x.repeatUsePerUserDay;
        });
    }
    if (time == 'Yesterday') {
        repeatUsePerUser = Analytics.find({campaignName: thisCampaignName}, {limit: 1}).fetch().map(function (x) {
            return x.repeatUsePerUserPreviousDay;
        });
    }
    else if (time == 'ThisWeek') {
        repeatUsePerUser = Analytics.find({campaignName: thisCampaignName}, {limit: 1}).fetch().map(function (x) {
            return x.repeatUsePerUserWeek;
        });
    }
    else if (time == 'LastWeek') {
        repeatUsePerUser = Analytics.find({campaignName: thisCampaignName}, {limit: 1}).fetch().map(function (x) {
            return x.repeatUsePerUserPreviousWeek;
        });
    }
    else if (time == 'ThisMonth') {
        repeatUsePerUser = Analytics.find({campaignName: thisCampaignName}, {limit: 1}).fetch().map(function (x) {
            return x.repeatUsePerUserMonth;
        });
    }
    else if (time == 'LastMonth') {
        repeatUsePerUser = Analytics.find({campaignName: thisCampaignName}, {limit: 1}).fetch().map(function (x) {
            return x.repeatUsePerUserPreviousMonth;
        });
    }
    else if (time == 'All') {
        repeatUsePerUser = Analytics.find({campaignName: thisCampaignName}, {limit: 1}).fetch().map(function (x) {
            return x.repeatUsePerUser;
        });
    }

    return repeatUsePerUser;
}

var RepeatUsePerUserViber = function (time, thisCampaignName) {
    var repeatUsePerUser = (ViewsViber(time, thisCampaignName) / UniqueUsersViber(time, thisCampaignName)).toFixed(2);

    return repeatUsePerUser;
}

var Males = function(time, thisCampaignName) {
    var males = 0;
    if (time == 'Today') {
        males = Analytics.find({campaignName: thisCampaignName}, {limit: 1}).fetch().map(function (x) {
            return x.malesDay;
        });
    }
    else if (time == 'ThisWeek') {
        males = Analytics.find({campaignName: thisCampaignName}, {limit: 1}).fetch().map(function (x) {
            return x.malesWeek;
        });
    }
    else if (time == 'ThisMonth') {
        males = Analytics.find({campaignName: thisCampaignName}, {limit: 1}).fetch().map(function (x) {
            return x.malesMonth;
        });
    }

    return parseInt(males);
}
var MalesPercentage = function(time, thisCampaignName) {
    if(Gender(time, thisCampaignName) == 0)
        return "Not available";
    return (Males(time, thisCampaignName) / Gender(time, thisCampaignName) * 100).toFixed(2) + "%";
}
var Females = function(time, thisCampaignName) {
    var females = 0;
    if (time == 'Today') {
        females = Analytics.find({campaignName: thisCampaignName}, {limit: 1}).fetch().map(function (x) {
            return x.femalesDay;
        });
    }
    else if (time == 'ThisWeek') {
        females = Analytics.find({campaignName: thisCampaignName}, {limit: 1}).fetch().map(function (x) {
            return x.femalesWeek;
        });
    }
    else if (time == 'ThisMonth') {
        females = Analytics.find({campaignName: thisCampaignName}, {limit: 1}).fetch().map(function (x) {
            return x.femalesMonth;
        });
    }

    return parseInt(females);
}
var FemalesPercentage = function(time, thisCampaignName) {
    if(Gender(time, thisCampaignName) == 0)
        return "Not available";
    return (Females(time, thisCampaignName) / Gender(time, thisCampaignName) * 100).toFixed(2) + "%";
}
var Gender = function(time, thisCampaignName) {
    return Males(time, thisCampaignName) + Females(time, thisCampaignName);
}

var CountriesNames = function(time, thisCampaignName) {
    var countriesArray = {};
    if (time == 'Today') {
        countriesArray = Analytics.find({campaignName: thisCampaignName}, {limit: 1}).fetch().map(function (x) {
            return x.countriesArrayDay;
        });
    }
    else if (time == 'ThisWeek') {
        countriesArray = Analytics.find({campaignName: thisCampaignName}, {limit: 1}).fetch().map(function (x) {
            return x.countriesArrayWeek;
        });
    }
    else if (time == 'ThisMonth') {
        countriesArray = Analytics.find({campaignName: thisCampaignName}, {limit: 1}).fetch().map(function (x) {
            return x.countriesArrayMonth;
        });
    }

    if (countriesArray[0] == undefined)
        return {};

    var countriesNames = Object.keys(countriesArray[0]);

    return countriesNames;
}
var CountriesValues = function(time, thisCampaignName) {
    var countriesArray = {};
    if (time == 'Today') {
        countriesArray = Analytics.find({campaignName: thisCampaignName}, {limit: 1}).fetch().map(function (x) {
            return x.countriesArrayDay;
        });
    }
    else if (time == 'ThisWeek') {
        countriesArray = Analytics.find({campaignName: thisCampaignName}, {limit: 1}).fetch().map(function (x) {
            return x.countriesArrayWeek;
        });
    }
    else if (time == 'ThisMonth') {
        countriesArray = Analytics.find({campaignName: thisCampaignName}, {limit: 1}).fetch().map(function (x) {
            return x.countriesArrayMonth;
        });
    }

    var countriesValues = [];
    for(var countriesName in countriesArray[0])
        countriesValues.push(countriesArray[0][countriesName]);

    return countriesValues;
}

var setTimeArray=function(time, timeType) {
    if (timeType == "Day") {

        var start = new Date(), end = new Date();
        start.setHours(time, 0, 0, 0);
        end.setHours(time, 59, 59, 999);
        return {start: start, end: end};
    }
    if (timeType == "Week") {
        var day=time.replace(/[^0-9]/g,'');

        var start = new Date(), end = new Date();
        start.setDate(day);
        end.setDate(day);
        start.setHours(0, 0, 0, 0);
        end.setHours(23, 59, 59, 999);
        return {start: start, end: end};
    }
    if (timeType == "Month") {

        var start = new Date(), end = new Date();
        start.setDate(time);
        end.setDate(time);
        start.setHours(0, 0, 0, 0);
        end.setHours(23, 59, 59, 999);
        return {start: start, end: end};
    }
}

var DataArray= function(fnName,timeType, cn) {
    var uniqueUsersViber = Analytics.find({campaignName: cn}, {limit: 1}).fetch().map(function (x) {
        return x.uniqueUsersViber;
    });
    var arr = [];
    if (timeType == 'Day') {
        if (fnName == 'Views') {
            arr = Analytics.find({campaignName: cn}, {limit: 1}).fetch().map(function (x) {
                return x.viewsArrayDay;
            });
        }
        else if (fnName == 'ViewsViber') {
            arr = Analytics.find({campaignName: cn}, {limit: 1}).fetch().map(function (x) {
                return x.viewsArrayViberDay;
            });
            for (var i = 0; i < arr[0].length; i++)
                arr[0][i] += Math.round(uniqueUsersViber / 50 / 7 / 4 / 24 * 5 + Math.round((i + 29.3) * i) % 30);
        }
        else if (fnName == 'Interactions') {
            arr = Analytics.find({campaignName: cn}, {limit: 1}).fetch().map(function (x) {
                return x.uniqueUsersArrayDay;
            });
            for (var i = 0; i < arr[0].length; i++)
                arr[0][i] = Math.round(arr[0][i] * 0.07);
        }
        else if (fnName == 'InteractionsViber') {
            arr = Analytics.find({campaignName: cn}, {limit: 1}).fetch().map(function (x) {
                return x.uniqueUsersArrayViberDay;
            });
            for (var i = 0; i < arr[0].length; i++) {
                arr[0][i] += Math.round(uniqueUsersViber / 50 / 7 / 4 / 24 + Math.round((i + 39.3) * i) % 30);
                arr[0][i] = Math.round(arr[0][i] * 0.07);
            }
        }
        else if (fnName == 'UniqueUsers') {
            arr = Analytics.find({campaignName: cn}, {limit: 1}).fetch().map(function (x) {
                return x.uniqueUsersArrayDay;
            });
        }
        else if (fnName == 'UniqueUsersViber') {
            arr = Analytics.find({campaignName: cn}, {limit: 1}).fetch().map(function (x) {
                return x.uniqueUsersArrayViberDay;
            });
            for (var i = 0; i < arr[0].length; i++)
                arr[0][i] += Math.round(uniqueUsersViber / 50 / 7 / 4 / 24 + Math.round((i + 49.3) * i) % 30);
        }
        else if (fnName == 'RepeatedUsers') {
            arr = Analytics.find({campaignName: cn}, {limit: 1}).fetch().map(function (x) {
                return x.repeatedUsersArrayDay;
            });
        }
        else if (fnName == 'RepeatedUsersViber') {
            arr = Analytics.find({campaignName: cn}, {limit: 1}).fetch().map(function (x) {
                return x.repeatedUsersArrayViberDay;
            });
            for (var i = 0; i < arr[0].length; i++)
                arr[0][i] += Math.round(uniqueUsersViber / 50 / 7 / 4 / 24 / 2 + Math.round((i + 59.3) * i) % 30);
        }
        else if (fnName == 'TimeSpentPerUser') {
            arr = Analytics.find({campaignName: cn}, {limit: 1}).fetch().map(function (x) {
                return x.timeSpentPerUserArrayDay;
            });
        }
        else if (fnName == 'TimeSpentPerUserViber') {
            arr = Analytics.find({campaignName: cn}, {limit: 1}).fetch().map(function (x) {
                return x.timeSpentPerUserArrayViberDay;
            });
        }
        else if (fnName == 'RepeatUsePerUser') {
            arr = Analytics.find({campaignName: cn}, {limit: 1}).fetch().map(function (x) {
                return x.repeatUsePerUserArrayDay;
            });
        }
        else if (fnName == 'RepeatUsePerUserViber') {
            arr = Analytics.find({campaignName: cn}, {limit: 1}).fetch().map(function (x) {
                return x.repeatUsePerUserArrayViberDay;
            });
            for (var i = 0; i < arr[0].length; i++)
                arr[0][i] += Math.round(uniqueUsersViber / 50 / 7 / 4 / 24 + Math.round((i + 69.3) * i) % 30);
        }
    }
    else  if (timeType == 'Week') {
        if (fnName == 'Views') {
            arr = Analytics.find({campaignName: cn}, {limit: 1}).fetch().map(function (x) {
                return x.viewsArrayWeek;
            });
        }
        else if (fnName == 'ViewsViber') {
            arr = Analytics.find({campaignName: cn}, {limit: 1}).fetch().map(function (x) {
                return x.viewsArrayViberWeek;
            });
            for (var i = 0; i < arr[0].length; i++)
                arr[0][i] += Math.round(uniqueUsersViber / 50 / 4 / 7 * 5 + Math.round((i + 9.3) * i) % 30);
        }
        else if (fnName == 'Interactions') {
            arr = Analytics.find({campaignName: cn}, {limit: 1}).fetch().map(function (x) {
                return x.uniqueUsersArrayWeek;
            });
            for (var i = 0; i < arr[0].length; i++)
                arr[0][i] = Math.round(arr[0][i] * 0.07);
        }
        else if (fnName == 'InteractionsViber') {
            arr = Analytics.find({campaignName: cn}, {limit: 1}).fetch().map(function (x) {
                return x.uniqueUsersArrayViberWeek;
            });
            for (var i = 0; i < arr[0].length; i++) {
                    arr[0][i] += Math.round(uniqueUsersViber / 50 / 4 / 7 + Math.round((i + 19.3) * i) % 30);
                    arr[0][i] = Math.round(arr[0][i] * 0.07);
                }
        }
        else if (fnName == 'UniqueUsers') {
            arr = Analytics.find({campaignName: cn}, {limit: 1}).fetch().map(function (x) {
                return x.uniqueUsersArrayWeek;
            });
        }
        else if (fnName == 'UniqueUsersViber') {
            arr = Analytics.find({campaignName: cn}, {limit: 1}).fetch().map(function (x) {
                return x.uniqueUsersArrayViberWeek;
            });
            for (var i = 0; i < arr[0].length; i++)
                arr[0][i] += Math.round(uniqueUsersViber / 50 / 4 / 7 + Math.round((i + 29.3) * i) % 30);
        }
        else if (fnName == 'RepeatedUsers') {
            arr = Analytics.find({campaignName: cn}, {limit: 1}).fetch().map(function (x) {
                return x.repeatedUsersArrayWeek;
            });
        }
        else if (fnName == 'RepeatedUsersViber') {
            arr = Analytics.find({campaignName: cn}, {limit: 1}).fetch().map(function (x) {
                return x.repeatedUsersArrayViberWeek;
            });
            for (var i = 0; i < arr[0].length; i++)
                arr[0][i] += Math.round(uniqueUsersViber / 50 / 4 / 7 / 2 + Math.round((i + 39.3) * i) % 30);
        }
        else if (fnName == 'TimeSpentPerUser') {
            arr = Analytics.find({campaignName: cn}, {limit: 1}).fetch().map(function (x) {
                return x.timeSpentPerUserArrayWeek;
            });
        }
        else if (fnName == 'TimeSpentPerUserViber') {
            arr = Analytics.find({campaignName: cn}, {limit: 1}).fetch().map(function (x) {
                return x.timeSpentPerUserArrayViberWeek;
            });
        }
        else if (fnName == 'RepeatUsePerUser') {
            arr = Analytics.find({campaignName: cn}, {limit: 1}).fetch().map(function (x) {
                return x.repeatUsePerUserArrayWeek;
            });
        }
        else if (fnName == 'RepeatUsePerUserViber') {
            arr = Analytics.find({campaignName: cn}, {limit: 1}).fetch().map(function (x) {
                return x.repeatUsePerUserArrayViberWeek;
            });
            for (var i = 0; i < arr[0].length; i++)
                arr[0][i] += Math.round(uniqueUsersViber / 50 / 4 / 7 + Math.round((i + 49.3) * i) % 30);
        }
    }
    else  if (timeType == 'Month') {
        if (fnName == 'Views') {
            arr = Analytics.find({campaignName: cn}, {limit: 1}).fetch().map(function (x) {
                return x.viewsArrayMonth;
            });
        }
        else if (fnName == 'ViewsViber') {
            arr = Analytics.find({campaignName: cn}, {limit: 1}).fetch().map(function (x) {
                return x.viewsArrayViberMonth;
            });
            for (var i = 0; i < arr[0].length; i++)
                arr[0][i] += Math.round(uniqueUsersViber / 50 / 30 * 5 + Math.round((i + 39.3) * i) % 30);
        }
        else if (fnName == 'Interactions') {
            arr = Analytics.find({campaignName: cn}, {limit: 1}).fetch().map(function (x) {
                return x.uniqueUsersArrayMonth;
            });
            for (var i = 0; i < arr[0].length; i++)
                arr[0][i] = Math.round(arr[0][i] * 0.07);
        }
        else if (fnName == 'InteractionsViber') {
            arr = Analytics.find({campaignName: cn}, {limit: 1}).fetch().map(function (x) {
                return x.uniqueUsersArrayViberMonth;
            });
            for (var i = 0; i < arr[0].length; i++) {
                arr[0][i] += Math.round(uniqueUsersViber / 50 / 30 + Math.round((i + 49.3) * i) % 30);
                arr[0][i] = Math.round(arr[0][i] * 0.07);
            }
        }
        else if (fnName == 'UniqueUsers') {
            arr = Analytics.find({campaignName: cn}, {limit: 1}).fetch().map(function (x) {
                return x.uniqueUsersArrayMonth;
            });
        }
        else if (fnName == 'UniqueUsersViber') {
            arr = Analytics.find({campaignName: cn}, {limit: 1}).fetch().map(function (x) {
                return x.uniqueUsersArrayViberMonth;
            });
            for (var i = 0; i < arr[0].length; i++)
                arr[0][i] += Math.round(uniqueUsersViber / 50 / 30 + Math.round((i + 59.3) * i) % 30);
        }
        else if (fnName == 'RepeatedUsers') {
            arr = Analytics.find({campaignName: cn}, {limit: 1}).fetch().map(function (x) {
                return x.repeatedUsersArrayMonth;
            });
        }
        else if (fnName == 'RepeatedUsersViber') {
            arr = Analytics.find({campaignName: cn}, {limit: 1}).fetch().map(function (x) {
                return x.repeatedUsersArrayViberMonth;
            });
            for (var i = 0; i < arr[0].length; i++)
                arr[0][i] += Math.round(uniqueUsersViber / 50 / 30 / 2 + Math.round((i + 69.3) * i) % 30);
        }
        else if (fnName == 'TimeSpentPerUser') {
            arr = Analytics.find({campaignName: cn}, {limit: 1}).fetch().map(function (x) {
                return x.timeSpentPerUserArrayMonth;
            });
        }
        else if (fnName == 'TimeSpentPerUserViber') {
            arr = Analytics.find({campaignName: cn}, {limit: 1}).fetch().map(function (x) {
                return x.timeSpentPerUserArrayViberMonth;
            });
        }
        else if (fnName == 'RepeatUsePerUser') {
            arr = Analytics.find({campaignName: cn}, {limit: 1}).fetch().map(function (x) {
                return x.repeatUsePerUserArrayMonth;
            });
        }
        else if (fnName == 'RepeatUsePerUserViber') {
            arr = Analytics.find({campaignName: cn}, {limit: 1}).fetch().map(function (x) {
                return x.repeatUsePerUserArrayViberMonth;
            });
            for (var i = 0; i < arr[0].length; i++)
                arr[0][i] += Math.round(uniqueUsersViber / 50 / 30 + Math.round((i + 79.3) * i) % 30);
        }
    }

    return arr[0];
}

Template.CampaignsDetails.rendered = function() {

    var start = new Date();
    start.setHours(0,0,0,0);

    var end = new Date();
    end.setHours(23,59,59,999);

    var cn = document.getElementById('cn').value;

    var dayViewsData= {
        labels: labelsDay(),
        datasets: [
            {
                label: "Line 1",
                fillColor: "rgba(159,159,159,0.5)",
                strokeColor: "rgba(159,159,159,1)",
                pointColor: "rgba(159,159,159,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(159,159,159,1)",
                data: DataArray("Views","Day", cn)

            }
        ]
    };

    var dayViewsDataViber = {
        labels: labelsDay(),
        datasets: [
            {
                label: "Line 1",
                fillColor: "rgba(159,159,159,0.5)",
                strokeColor: "rgba(159,159,159,1)",
                pointColor: "rgba(159,159,159,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(159,159,159,1)",
                data: DataArray("ViewsViber","Day", cn)

            }
        ]
    };

    var dayInteractionsData = {
        labels: labelsDay(),
        datasets: [
            {
                label: "Line 1",
                fillColor: "rgba(159,159,159,0.5)",
                strokeColor: "rgba(159,159,159,1)",
                pointColor: "rgba(159,159,159,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(159,159,159,1)",
                data: DataArray("Interactions","Day", cn)

            }
        ]
    };

    var dayInteractionsDataViber = {
        labels: labelsDay(),
        datasets: [
            {
                label: "Line 1",
                fillColor: "rgba(159,159,159,0.5)",
                strokeColor: "rgba(159,159,159,1)",
                pointColor: "rgba(159,159,159,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(159,159,159,1)",
                data: DataArray("InteractionsViber","Day", cn)

            }
        ]
    };

    var dayUniqueUsersData = {
        labels: labelsDay(),
        datasets: [
            {
                label: "Line 1",
                fillColor: "rgba(159,159,159,0.5)",
                strokeColor: "rgba(159,159,159,1)",
                pointColor: "rgba(159,159,159,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(159,159,159,1)",
                data: DataArray("UniqueUsers","Day", cn)

            }
        ]
    };

    var dayUniqueUsersDataViber = {
        labels: labelsDay(),
        datasets: [
            {
                label: "Line 1",
                fillColor: "rgba(159,159,159,0.5)",
                strokeColor: "rgba(159,159,159,1)",
                pointColor: "rgba(159,159,159,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(159,159,159,1)",
                data: DataArray("UniqueUsersViber","Day", cn)

            }
        ]
    };

    var dayRepeatedUsersData = {
        labels: labelsDay(),
        datasets: [
            {
                label: "Line 1",
                fillColor: "rgba(159,159,159,0.5)",
                strokeColor: "rgba(159,159,159,1)",
                pointColor: "rgba(159,159,159,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(159,159,159,1)",
                data: DataArray("RepeatedUsers","Day", cn)

            }
        ]
    };

    var dayRepeatedUsersDataViber = {
        labels: labelsDay(),
        datasets: [
            {
                label: "Line 1",
                fillColor: "rgba(159,159,159,0.5)",
                strokeColor: "rgba(159,159,159,1)",
                pointColor: "rgba(159,159,159,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(159,159,159,1)",
                data: DataArray("RepeatedUsersViber","Day", cn)

            }
        ]
    };

    var dayTimeSpentPerUserData = {
        labels: labelsDay(),
        datasets: [
            {
                label: "Line 1",
                fillColor: "rgba(159,159,159,0.5)",
                strokeColor: "rgba(159,159,159,1)",
                pointColor: "rgba(159,159,159,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(159,159,159,1)",
                data: DataArray("TimeSpentPerUser","Day", cn)

            }
        ]
    };

    var dayTimeSpentPerUserDataViber = {
        labels: labelsDay(),
        datasets: [
            {
                label: "Line 1",
                fillColor: "rgba(159,159,159,0.5)",
                strokeColor: "rgba(159,159,159,1)",
                pointColor: "rgba(159,159,159,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(159,159,159,1)",
                data: DataArray("TimeSpentPerUserViber","Day", cn)

            }
        ]
    };

    var dayRepeatUsePerUserData = {
        labels: labelsDay(),
        datasets: [
            {
                label: "Line 1",
                fillColor: "rgba(159,159,159,0.5)",
                strokeColor: "rgba(159,159,159,1)",
                pointColor: "rgba(159,159,159,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(159,159,159,1)",
                data: DataArray("RepeatUsePerUser","Day", cn)

            }
        ]
    };

    var dayRepeatUsePerUserDataViber = {
        labels: labelsDay(),
        datasets: [
            {
                label: "Line 1",
                fillColor: "rgba(159,159,159,0.5)",
                strokeColor: "rgba(159,159,159,1)",
                pointColor: "rgba(159,159,159,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(159,159,159,1)",
                data: DataArray("RepeatUsePerUserViber","Day", cn)

            }
        ]
    };

    var weekViewsData = {
        labels: labelsWeek(),
        datasets: [
            {
                label: "Line 1",
                fillColor: "rgba(159,159,159,0.5)",
                strokeColor: "rgba(159,159,159,1)",
                pointColor: "rgba(159,159,159,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(159,159,159,1)",
                data: DataArray("Views","Week", cn)

            }
        ]
    };

    var weekViewsDataViber = {
        labels: labelsWeek(),
        datasets: [
            {
                label: "Line 1",
                fillColor: "rgba(159,159,159,0.5)",
                strokeColor: "rgba(159,159,159,1)",
                pointColor: "rgba(159,159,159,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(159,159,159,1)",
                data: DataArray("ViewsViber","Week", cn)

            }
        ]
    };

    var weekInteractionsData = {
        labels: labelsWeek(),
        datasets: [
            {
                label: "Line 1",
                fillColor: "rgba(159,159,159,0.5)",
                strokeColor: "rgba(159,159,159,1)",
                pointColor: "rgba(159,159,159,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(159,159,159,1)",
                data: DataArray("Interactions","Week", cn)

            }
        ]
    };

    var weekInteractionsDataViber = {
        labels: labelsWeek(),
        datasets: [
            {
                label: "Line 1",
                fillColor: "rgba(159,159,159,0.5)",
                strokeColor: "rgba(159,159,159,1)",
                pointColor: "rgba(159,159,159,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(159,159,159,1)",
                data: DataArray("InteractionsViber","Week", cn)

            }
        ]
    };

    var weekUniqueUsersData = {
        labels: labelsWeek(),
        datasets: [
            {
                label: "Line 1",
                fillColor: "rgba(159,159,159,0.5)",
                strokeColor: "rgba(159,159,159,1)",
                pointColor: "rgba(159,159,159,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(159,159,159,1)",
                data: DataArray("UniqueUsers","Week", cn)

            }
        ]
    };

    var weekUniqueUsersDataViber = {
        labels: labelsWeek(),
        datasets: [
            {
                label: "Line 1",
                fillColor: "rgba(159,159,159,0.5)",
                strokeColor: "rgba(159,159,159,1)",
                pointColor: "rgba(159,159,159,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(159,159,159,1)",
                data: DataArray("UniqueUsersViber","Week", cn)

            }
        ]
    };

    var weekRepeatedUsersData = {
        labels: labelsWeek(),
        datasets: [
            {
                label: "Line 1",
                fillColor: "rgba(159,159,159,0.5)",
                strokeColor: "rgba(159,159,159,1)",
                pointColor: "rgba(159,159,159,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(159,159,159,1)",
                data: DataArray("RepeatedUsers","Week", cn)

            }
        ]
    };

    var weekRepeatedUsersDataViber = {
        labels: labelsWeek(),
        datasets: [
            {
                label: "Line 1",
                fillColor: "rgba(159,159,159,0.5)",
                strokeColor: "rgba(159,159,159,1)",
                pointColor: "rgba(159,159,159,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(159,159,159,1)",
                data: DataArray("RepeatedUsersViber","Week", cn)

            }
        ]
    };

    var weekTimeSpentPerUserData = {
        labels: labelsWeek(),
        datasets: [
            {
                label: "Line 1",
                fillColor: "rgba(159,159,159,0.5)",
                strokeColor: "rgba(159,159,159,1)",
                pointColor: "rgba(159,159,159,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(159,159,159,1)",
                data: DataArray("TimeSpentPerUser","Week", cn)
            }
        ]
    };

    var weekTimeSpentPerUserDataViber = {
        labels: labelsWeek(),
        datasets: [
            {
                label: "Line 1",
                fillColor: "rgba(159,159,159,0.5)",
                strokeColor: "rgba(159,159,159,1)",
                pointColor: "rgba(159,159,159,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(159,159,159,1)",
                data: DataArray("TimeSpentPerUserViber","Week", cn)
            }
        ]
    };

    var weekRepeatUsePerUserData = {
        labels: labelsWeek(),
        datasets: [
            {
                label: "Line 1",
                fillColor: "rgba(159,159,159,0.5)",
                strokeColor: "rgba(159,159,159,1)",
                pointColor: "rgba(159,159,159,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(159,159,159,1)",
                data: DataArray("RepeatUsePerUser","Week", cn)

            }
        ]
    };

    var weekRepeatUsePerUserDataViber = {
        labels: labelsWeek(),
        datasets: [
            {
                label: "Line 1",
                fillColor: "rgba(159,159,159,0.5)",
                strokeColor: "rgba(159,159,159,1)",
                pointColor: "rgba(159,159,159,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(159,159,159,1)",
                data: DataArray("RepeatUsePerUserViber","Week", cn)

            }
        ]
    };

    var monthViewsData = {
        labels: labelsMonth(),
        datasets: [
            {
               label: "Line 1",
               fillColor: "rgba(159,159,159,0.5)",
               strokeColor: "rgba(159,159,159,1)",
               pointColor: "rgba(159,159,159,1)",
               pointStrokeColor: "#fff",
               pointHighlightFill: "#fff",
               pointHighlightStroke: "rgba(159,159,159,1)",
               data: DataArray("Views","Month", cn)

            }
        ]
    };

    var monthViewsDataViber = {
        labels: labelsMonth(),
        datasets: [
            {
                label: "Line 1",
                fillColor: "rgba(159,159,159,0.5)",
                strokeColor: "rgba(159,159,159,1)",
                pointColor: "rgba(159,159,159,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(159,159,159,1)",
                data: DataArray("ViewsViber","Month", cn)

            }
        ]
    };

    var monthInteractionsData = {
        labels: labelsMonth(),
        datasets: [
            {
                label: "Line 1",
                fillColor: "rgba(159,159,159,0.5)",
                strokeColor: "rgba(159,159,159,1)",
                pointColor: "rgba(159,159,159,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(159,159,159,1)",
                data: DataArray("Interactions","Month", cn)

            }
        ]
    };

    var monthInteractionsDataViber = {
        labels: labelsMonth(),
        datasets: [
            {
                label: "Line 1",
                fillColor: "rgba(159,159,159,0.5)",
                strokeColor: "rgba(159,159,159,1)",
                pointColor: "rgba(159,159,159,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(159,159,159,1)",
                data: DataArray("InteractionsViber","Month", cn)

            }
        ]
    };

    var monthUniqueUsersData = {
        labels: labelsMonth(),
        datasets: [
            {
                label: "Line 1",
                fillColor: "rgba(159,159,159,0.5)",
                strokeColor: "rgba(159,159,159,1)",
                pointColor: "rgba(159,159,159,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(159,159,159,1)",
                data: DataArray("UniqueUsers","Month", cn)

            }
        ]
    };

    var monthUniqueUsersDataViber = {
        labels: labelsMonth(),
        datasets: [
            {
                label: "Line 1",
                fillColor: "rgba(159,159,159,0.5)",
                strokeColor: "rgba(159,159,159,1)",
                pointColor: "rgba(159,159,159,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(159,159,159,1)",
                data: DataArray("UniqueUsersViber","Month", cn)

            }
        ]
    };

    var monthRepeatedUsersData = {
        labels: labelsMonth(),
        datasets: [
            {
                label: "Line 1",
                fillColor: "rgba(159,159,159,0.5)",
                strokeColor: "rgba(159,159,159,1)",
                pointColor: "rgba(159,159,159,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(159,159,159,1)",
                data: DataArray("RepeatedUsers","Month", cn)
            }
        ]
    };

    var monthRepeatedUsersDataViber = {
        labels: labelsMonth(),
        datasets: [
            {
                label: "Line 1",
                fillColor: "rgba(159,159,159,0.5)",
                strokeColor: "rgba(159,159,159,1)",
                pointColor: "rgba(159,159,159,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(159,159,159,1)",
                data: DataArray("RepeatedUsersViber","Month", cn)
            }
        ]
    };

    var monthTimeSpentPerUserData = {
        labels: labelsMonth(),
        datasets: [
            {
                label: "Line 1",
                fillColor: "rgba(159,159,159,0.5)",
                strokeColor: "rgba(159,159,159,1)",
                pointColor: "rgba(159,159,159,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(159,159,159,1)",
                data: DataArray("TimeSpentPerUser","Month", cn)

            }
        ]
    };

    var monthTimeSpentPerUserDataViber = {
        labels: labelsMonth(),
        datasets: [
            {
                label: "Line 1",
                fillColor: "rgba(159,159,159,0.5)",
                strokeColor: "rgba(159,159,159,1)",
                pointColor: "rgba(159,159,159,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(159,159,159,1)",
                data: DataArray("TimeSpentPerUserViber","Month", cn)

            }
        ]
    };

    var monthRepeatUsePerUserData = {
        labels: labelsMonth(),
        datasets: [
            {
                label: "Line 1",
                fillColor: "rgba(159,159,159,0.5)",
                strokeColor: "rgba(159,159,159,1)",
                pointColor: "rgba(159,159,159,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(159,159,159,1)",
                data: DataArray("RepeatUsePerUser","Month", cn)

            }
        ]
    };

    var monthRepeatUsePerUserDataViber = {
        labels: labelsMonth(),
        datasets: [
            {
                label: "Line 1",
                fillColor: "rgba(159,159,159,0.5)",
                strokeColor: "rgba(159,159,159,1)",
                pointColor: "rgba(159,159,159,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(159,159,159,1)",
                data: DataArray("RepeatUsePerUserViber","Month", cn)

            }
        ]
    };

    var lineOptions = {
        scaleShowGridLines : true,
        scaleGridLineColor : "rgba(0,0,0,.05)",
        scaleGridLineWidth : 1,
        bezierCurve : true,
        bezierCurveTension : 0.4,
        pointDot : true,
        pointDotRadius : 4,
        pointDotStrokeWidth : 1,
        pointHitDetectionRadius : 20,
        datasetStroke : true,
        datasetStrokeWidth : 1,
        datasetFill : true,
        responsive: true
    };

    var ctx = document.getElementById("lineOptions-by-chat-platform1").getContext("2d");
    var myNewChart = new Chart(ctx).Line(dayViewsData, lineOptions);
    var ctx = document.getElementById("lineOptions-by-chat-platform2").getContext("2d");
    var myNewChart = new Chart(ctx).Line(dayInteractionsData, lineOptions);
    var ctx = document.getElementById("lineOptions-by-chat-platform3").getContext("2d");
    var myNewChart = new Chart(ctx).Line(dayUniqueUsersData, lineOptions);
    var ctx = document.getElementById("lineOptions-by-chat-platform4").getContext("2d");
    var myNewChart = new Chart(ctx).Line(dayRepeatedUsersData, lineOptions);
    var ctx = document.getElementById("lineOptions-by-chat-platform5").getContext("2d");
    var myNewChart = new Chart(ctx).Line(dayTimeSpentPerUserData, lineOptions);
    var ctx = document.getElementById("lineOptions-by-chat-platform6").getContext("2d");
    var myNewChart = new Chart(ctx).Line(dayRepeatUsePerUserData, lineOptions);

    var ctx = document.getElementById("lineOptions-by-chat-platform7").getContext("2d");
    var myNewChart = new Chart(ctx).Line(monthViewsData, lineOptions);
    var ctx = document.getElementById("lineOptions-by-chat-platform8").getContext("2d");
    var myNewChart = new Chart(ctx).Line(monthInteractionsData, lineOptions);
    var ctx = document.getElementById("lineOptions-by-chat-platform9").getContext("2d");
    var myNewChart = new Chart(ctx).Line(monthUniqueUsersData, lineOptions);
    var ctx = document.getElementById("lineOptions-by-chat-platform10").getContext("2d");
    var myNewChart = new Chart(ctx).Line(monthRepeatedUsersData, lineOptions);
    var ctx = document.getElementById("lineOptions-by-chat-platform11").getContext("2d");
    var myNewChart = new Chart(ctx).Line(monthTimeSpentPerUserData, lineOptions);
    var ctx = document.getElementById("lineOptions-by-chat-platform12").getContext("2d");
    var myNewChart = new Chart(ctx).Line(monthRepeatUsePerUserData, lineOptions);

    var ctx = document.getElementById("lineOptions-by-chat-platform13").getContext("2d");
    var myNewChart = new Chart(ctx).Line(weekViewsData, lineOptions);
    var ctx = document.getElementById("lineOptions-by-chat-platform14").getContext("2d");
    var myNewChart = new Chart(ctx).Line(weekInteractionsData, lineOptions);
    var ctx = document.getElementById("lineOptions-by-chat-platform15").getContext("2d");
    var myNewChart = new Chart(ctx).Line(weekUniqueUsersData, lineOptions);
    var ctx = document.getElementById("lineOptions-by-chat-platform16").getContext("2d");
    var myNewChart = new Chart(ctx).Line(weekRepeatedUsersData, lineOptions);
    var ctx = document.getElementById("lineOptions-by-chat-platform17").getContext("2d");
    var myNewChart = new Chart(ctx).Line(weekTimeSpentPerUserData, lineOptions);
    var ctx = document.getElementById("lineOptions-by-chat-platform18").getContext("2d");
    var myNewChart = new Chart(ctx).Line(weekRepeatUsePerUserData, lineOptions);

    var ctx = document.getElementById("lineOptions-by-chat-platform1v").getContext("2d");
    var myNewChart = new Chart(ctx).Line(dayViewsDataViber, lineOptions);
    var ctx = document.getElementById("lineOptions-by-chat-platform2v").getContext("2d");
    var myNewChart = new Chart(ctx).Line(dayInteractionsDataViber, lineOptions);
    var ctx = document.getElementById("lineOptions-by-chat-platform3v").getContext("2d");
    var myNewChart = new Chart(ctx).Line(dayUniqueUsersDataViber, lineOptions);
    var ctx = document.getElementById("lineOptions-by-chat-platform4v").getContext("2d");
    var myNewChart = new Chart(ctx).Line(dayRepeatedUsersDataViber, lineOptions);
    var ctx = document.getElementById("lineOptions-by-chat-platform5v").getContext("2d");
    var myNewChart = new Chart(ctx).Line(dayTimeSpentPerUserDataViber, lineOptions);
    var ctx = document.getElementById("lineOptions-by-chat-platform6v").getContext("2d");
    var myNewChart = new Chart(ctx).Line(dayRepeatUsePerUserDataViber, lineOptions);

    var ctx = document.getElementById("lineOptions-by-chat-platform1wv").getContext("2d");
    var myNewChart = new Chart(ctx).Line(weekViewsDataViber, lineOptions);
    var ctx = document.getElementById("lineOptions-by-chat-platform2wv").getContext("2d");
    var myNewChart = new Chart(ctx).Line(weekInteractionsDataViber, lineOptions);
    var ctx = document.getElementById("lineOptions-by-chat-platform3wv").getContext("2d");
    var myNewChart = new Chart(ctx).Line(weekUniqueUsersDataViber, lineOptions);
    var ctx = document.getElementById("lineOptions-by-chat-platform4wv").getContext("2d");
    var myNewChart = new Chart(ctx).Line(weekRepeatedUsersDataViber, lineOptions);
    var ctx = document.getElementById("lineOptions-by-chat-platform5wv").getContext("2d");
    var myNewChart = new Chart(ctx).Line(weekTimeSpentPerUserDataViber, lineOptions);
    var ctx = document.getElementById("lineOptions-by-chat-platform6wv").getContext("2d");
    var myNewChart = new Chart(ctx).Line(weekRepeatUsePerUserDataViber, lineOptions);

    var ctx = document.getElementById("lineOptions-by-chat-platform1mv").getContext("2d");
    var myNewChart = new Chart(ctx).Line(monthViewsDataViber, lineOptions);
    var ctx = document.getElementById("lineOptions-by-chat-platform2mv").getContext("2d");
    var myNewChart = new Chart(ctx).Line(monthInteractionsDataViber, lineOptions);
    var ctx = document.getElementById("lineOptions-by-chat-platform3mv").getContext("2d");
    var myNewChart = new Chart(ctx).Line(monthUniqueUsersDataViber, lineOptions);
    var ctx = document.getElementById("lineOptions-by-chat-platform4mv").getContext("2d");
    var myNewChart = new Chart(ctx).Line(monthRepeatedUsersDataViber, lineOptions);
    var ctx = document.getElementById("lineOptions-by-chat-platform5mv").getContext("2d");
    var myNewChart = new Chart(ctx).Line(monthTimeSpentPerUserDataViber, lineOptions);
    var ctx = document.getElementById("lineOptions-by-chat-platform6mv").getContext("2d");
    var myNewChart = new Chart(ctx).Line(monthRepeatUsePerUserDataViber, lineOptions);

    var usersWhoFinishedChart = [
        {
            value: 55404,
            color:"#0066cb",
            highlight: "#0066cb",
            label: "Finished"
        },
        {
            value: 90396,
            color: "#9f9f9f",
            highlight: "#9f9f9f",
            label: "Not Finished"
        }
    ];

    var doughnutDataGenderDay = [
        {
            value: Males("Today", document.getElementById('cn').value),
            color:"#0066cb",
            highlight: "#0066cb",
            label: "Male"
        },
        {
            value: Females("Today", document.getElementById('cn').value),
            color: "#9f9f9f",
            highlight: "#9f9f9f",
            label: "Female"
        }
    ];

    var doughnutDataGendervDay = [
        {
            value: 509,
            color:"#0066cb",
            highlight: "#0066cb",
            label: "Male"
        },
        {
            value: 412,
            color: "#9f9f9f",
            highlight: "#9f9f9f",
            label: "Female"
        }
    ];

    var doughnutDataGenderWeek = [
        {
            value: Males("ThisWeek", document.getElementById('cn').value),
            color:"#0066cb",
            highlight: "#0066cb",
            label: "Male"
        },
        {
            value: Females("ThisWeek", document.getElementById('cn').value),
            color: "#9f9f9f",
            highlight: "#9f9f9f",
            label: "Female"
        }
    ];

    var doughnutDataGendervWeek = [
        {
            value: 3282,
            color:"#0066cb",
            highlight: "#0066cb",
            label: "Male"
        },
        {
            value: 2035,
            color: "#9f9f9f",
            highlight: "#9f9f9f",
            label: "Female"
        }
    ];

    var doughnutDataGenderMonth = [
        {
            value: Males("ThisMonth", document.getElementById('cn').value),
            color:"#0066cb",
            highlight: "#0066cb",
            label: "Male"
        },
        {
            value: Females("ThisMonth", document.getElementById('cn').value),
            color: "#9f9f9f",
            highlight: "#9f9f9f",
            label: "Female"
        }
    ];

    var doughnutDataGendervMonth = [
        {
            value: 14237,
            color:"#0066cb",
            highlight: "#0066cb",
            label: "Male"
        },
        {
            value: 11926,
            color: "#9f9f9f",
            highlight: "#9f9f9f",
            label: "Female"
        }
    ];

    var doughnutData = [
        {
            value: 36450,
            color:"#0066cb",
            highlight: "#0066cb",
            label: "Key Clicks"
        },
        {
            value: 109350,
            color: "#9f9f9f",
            highlight: "#9f9f9f",
            label: "Key Views"
        }
    ];

    var doughnutData1 = [
        {
            value: 37597,
            color:"#0066cb",
            highlight: "#0066cb",
            label: "Used"
        },
        {
            value: 49838,
            color: "#9f9f9f",
            highlight: "#9f9f9f",
            label: "Not Used"
        }
    ];

    var doughnutData2 = [
        {
            value: 1127910,
            color:"#0066cb",
            highlight: "#0066cb",
            label: "Used"
        },
        {
            value: 1495140,
            color: "#9f9f9f",
            highlight: "#9f9f9f",
            label: "Not Used"
        }
    ];

    var doughnutData3 = [
        {
            value: 263179,
            color:"#0066cb",
            highlight: "#0066cb",
            label: "Used"
        },
        {
            value: 348866,
            color: "#9f9f9f",
            highlight: "#9f9f9f",
            label: "Not Used"
        }
    ];


    var doughnutOptions = {
        segmentShowStroke: true,
        segmentStrokeColor: "#fff",
        segmentStrokeWidth: 2,
        percentageInnerCutout: 0, // This is 0 for Pie charts
        animationSteps: 100,
        animationEasing: "easeOutBounce",
        animateRotate: true,
        animateScale: false,
        responsive: true
    };

    // var ctx = document.getElementById("usersWhoFinishedChartDaily").getContext("2d");
    // var myNewChart = new Chart(ctx).Doughnut(usersWhoFinishedChart, doughnutOptions);
    // var ctx = document.getElementById("usersWhoFinishedChartWeekly").getContext("2d");
    // var myNewChart = new Chart(ctx).Doughnut(usersWhoFinishedChart, doughnutOptions);
    // var ctx = document.getElementById("usersWhoFinishedChartMonthly").getContext("2d");
    // var myNewChart = new Chart(ctx).Doughnut(usersWhoFinishedChart, doughnutOptions);
    //
    // var ctx = document.getElementById("ConversionRateChartDaily").getContext("2d");
    // var myNewChart = new Chart(ctx).Doughnut(doughnutData, doughnutOptions);
    // var ctx = document.getElementById("ConversionRateChartWeekly").getContext("2d");
    // var myNewChart = new Chart(ctx).Doughnut(doughnutData, doughnutOptions);
    // var ctx = document.getElementById("ConversionRateChartMonthly").getContext("2d");
    // var myNewChart = new Chart(ctx).Doughnut(doughnutData, doughnutOptions);
    //
    var ctx = document.getElementById("doughnutChartGender3d").getContext("2d");
    var myNewChart = new Chart(ctx).Doughnut(doughnutDataGenderDay, doughnutOptions);
    //var ctx = document.getElementById("doughnutChartGenderv3d").getContext("2d");
    //var myNewChart = new Chart(ctx).Doughnut(doughnutDataGendervDay, doughnutOptions);
    var ctx = document.getElementById("doughnutChartGender3w").getContext("2d");
    var myNewChart = new Chart(ctx).Doughnut(doughnutDataGenderWeek, doughnutOptions);
    //var ctx = document.getElementById("doughnutChartGenderv3w").getContext("2d");
    //var myNewChart = new Chart(ctx).Doughnut(doughnutDataGendervWeek, doughnutOptions);
    var ctx = document.getElementById("doughnutChartGender3m").getContext("2d");
    var myNewChart = new Chart(ctx).Doughnut(doughnutDataGenderMonth, doughnutOptions);
    //var ctx = document.getElementById("doughnutChartGenderv3m").getContext("2d");
    //var myNewChart = new Chart(ctx).Doughnut(doughnutDataGendervMonth, doughnutOptions);
    //var ctx = document.getElementById("doughnutChart3d").getContext("2d");
    //var myNewChart = new Chart(ctx).Doughnut(doughnutData1, doughnutOptions);
    //var ctx = document.getElementById("doughnutChart3w").getContext("2d");
    //var myNewChart = new Chart(ctx).Doughnut(doughnutData3, doughnutOptions);
    //var ctx = document.getElementById("doughnutChart3m").getContext("2d");
    //var myNewChart = new Chart(ctx).Doughnut(doughnutData2, doughnutOptions);
    //var ctx = document.getElementById("doughnutChart3dv").getContext("2d");
    //var myNewChart = new Chart(ctx).Doughnut(doughnutData1, doughnutOptions);
    //var ctx = document.getElementById("doughnutChart3wv").getContext("2d");
    //var myNewChart = new Chart(ctx).Doughnut(doughnutData3, doughnutOptions);
    //var ctx = document.getElementById("doughnutChart3mv").getContext("2d");
    //var myNewChart = new Chart(ctx).Doughnut(doughnutData2, doughnutOptions);


   // Options for gender and Age
    var barOptions = {
        scaleBeginAtZero : true,
        scaleShowGridLines : true,
        scaleGridLineColor : "rgba(0,0,0,.05)",
        scaleGridLineWidth : 1,
        barShowStroke : true,
        barStrokeWidth : 1,
        barValueSpacing : 7,
        barDatasetSpacing : 1,
        responsive:true
    };

    var barDataLocationDaily = {
        labels: CountriesNames("Today", document.getElementById('cn').value),
        datasets: [
            {
                label: "",
                fillColor: "rgba(76,148,218,1)",
                strokeColor: "rgba(76,148,218,1)",
                highlightFill: "rgba(76,148,218,0.75)",
                highlightStroke: "rgba(76,148,218,1)",
                data: CountriesValues("ThisMonth", document.getElementById('cn').value)
            }
        ]
    };

    var barDataLocationvDaily = {
        labels: ['United States','France','Canada','Germany', 'New Zealand', 'South Africa', 'Australia', 'Russia', 'Turkey', 'Spain'],
        datasets: [
            {
                label: "",
                fillColor: "rgba(76,148,218,1)",
                strokeColor: "rgba(76,148,218,1)",
                highlightFill: "rgba(76,148,218,0.75)",
                highlightStroke: "rgba(76,148,218,1)",
                data: [974, 609, 445, 433, 404, 364, 352, 319, 270, 211]
            }
        ]
    };

    var barDataLocationWeekly = {
        labels: CountriesNames("ThisWeek", document.getElementById('cn').value),
        datasets: [
            {
                label: "",
                fillColor: "rgba(76,148,218,1)",
                strokeColor: "rgba(76,148,218,1)",
                highlightFill: "rgba(76,148,218,0.75)",
                highlightStroke: "rgba(76,148,218,1)",
                data: CountriesValues("ThisMonth", document.getElementById('cn').value)
            }
        ]
    };

    var barDataLocationvWeekly = {
        labels: ['United States','Mexico','Canada', 'South Africa', 'Germany', 'New Zealand', 'Australia', 'France', 'Turkey', 'Sweden'],
        datasets: [
            {
                label: "",
                fillColor: "rgba(76,148,218,1)",
                strokeColor: "rgba(76,148,218,1)",
                highlightFill: "rgba(76,148,218,0.75)",
                highlightStroke: "rgba(76,148,218,1)",
                data: [4934, 2809, 1745, 1433, 1304, 1224, 1152, 1119, 970, 811]
            }
        ]
    };

    var barDataLocationMonthly = {
        labels: CountriesNames("ThisMonth", document.getElementById('cn').value),
        datasets: [
            {
                label: "",
                fillColor: "rgba(76,148,218,1)",
                strokeColor: "rgba(76,148,218,1)",
                highlightFill: "rgba(76,148,218,0.75)",
                highlightStroke: "rgba(76,148,218,1)",
                data: CountriesValues("ThisMonth", document.getElementById('cn').value)
            }
        ]
    };

    var barDataLocationvMonthly = {
        labels: ['United States','Mexico','Germany', 'Canada', 'South Africa', 'Germany', 'France', 'Japan', 'Belgium', 'Turkey'],
        datasets: [
            {
                label: "",
                fillColor: "rgba(76,148,218,1)",
                strokeColor: "rgba(76,148,218,1)",
                highlightFill: "rgba(76,148,218,0.75)",
                highlightStroke: "rgba(76,148,218,1)",
                data: [16934, 12809, 11745, 11433, 9304, 9224, 6152, 5719, 5370, 5011]
            }
        ]
    };


    var barData2 = {
        labels: ["Tell me more", "Show me more title", "Return to main menu", "etc..."],
        datasets: [
            {
                label: "",
                fillColor: "rgba(76,148,218,1)",
                strokeColor: "rgba(76,148,218,1)",
                highlightFill: "rgba(76,148,218,0.75)",
                highlightStroke: "rgba(76,148,218,1)",
                data: [80, 35, 97, 15]
            }
        ]
    };
    var barData1 = {
        labels: ["Drama", "Comedy", "Documentary", "Surprise Me!"],
        datasets: [
            {
                label: "Usage",
                fillColor: "rgba(76,148,218,1)",
                strokeColor: "rgba(76,148,218,1)",
                highlightFill: "rgba(76,148,218,0.75)",
                highlightStroke: "rgba(76,148,218,1)",
                data: [35, 75, 60, 90]
            },
            {
                label: "Conversion",
                fillColor: "rgba(233,127,117,1)",
                strokeColor: "rgba(233,127,117,1)",
                highlightFill: "rgba(233,127,117,0.75)",
                highlightStroke: "rgba(233,127,117,1)",
                data: [20, 47, 35, 55]
            }
        ]
    };

    var barData2w = {
        labels: ["Tell me more", "Show me more title", "Return to main menu", "etc..."],
        datasets: [
            {
                label: "",
                fillColor: "rgba(76,148,218,1)",
                strokeColor: "rgba(76,148,218,1)",
                highlightFill: "rgba(76,148,218,0.75)",
                highlightStroke: "rgba(76,148,218,1)",
                data: [560, 245, 679, 105]
            }
        ]
    };
    var barData1w = {
        labels: ["Drama", "Comedy", "Documentary", "Surprise Me!"],
        datasets: [
            {
                label: "Usage",
                fillColor: "rgba(76,148,218,1)",
                strokeColor: "rgba(76,148,218,1)",
                highlightFill: "rgba(76,148,218,0.75)",
                highlightStroke: "rgba(76,148,218,1)",
                data: [245, 525, 420, 630]
            },
            {
                label: "Conversion",
                fillColor: "rgba(233,127,117,1)",
                strokeColor: "rgba(233,127,117,1)",
                highlightFill: "rgba(233,127,117,0.75)",
                highlightStroke: "rgba(233,127,117,1)",
                data: [140, 329, 245, 385]
            }
        ]
    };


    var barData2m = {
        labels: ["Tell me more", "Show me more title", "Return to main menu", "etc..."],
        datasets: [
            {
                label: "",
                fillColor: "rgba(76,148,218,1)",
                strokeColor: "rgba(76,148,218,1)",
                highlightFill: "rgba(76,148,218,0.75)",
                highlightStroke: "rgba(76,148,218,1)",
                data: [2400, 1050, 2910, 450]
            }
        ]
    };
    var barData1m = {
        labels: ["Drama", "Comedy", "Documentary", "Surprise Me!"],
        datasets: [
            {
                label: "Usage",
                fillColor: "rgba(76,148,218,1)",
                strokeColor: "rgba(76,148,218,1)",
                highlightFill: "rgba(76,148,218,0.75)",
                highlightStroke: "rgba(76,148,218,1)",
                data: [1050, 2250, 1800, 2700]
            },
            {
                label: "Conversion",
                fillColor: "rgba(233,127,117,1)",
                strokeColor: "rgba(233,127,117,1)",
                highlightFill: "rgba(233,127,117,0.75)",
                highlightStroke: "rgba(233,127,117,1)",
                data: [600, 1410, 1050, 1650]
            }
        ]
    };
    var barData3 = {
        labels: ["-12", "13-17", "18-24", "25-34", "35-44", "45-54", "55-64","65+"],
        datasets: [
            {
                label: "Male",
                fillColor: "rgba(76,148,218,1)",
                strokeColor: "rgba(76,148,218,1)",
                highlightFill: "rgba(76,148,218,0.75)",
                highlightStroke: "rgba(76,148,218,1)",
                data: [550, 1900, 2800, 3400,2750, 2136, 990,600]
            },
            {
                label: "Female",
                fillColor: "rgba(233,127,117,1)",
                strokeColor: "rgba(233,127,117,1)",
                highlightFill: "rgba(233,127,117,0.75)",
                highlightStroke: "rgba(233,127,117,1)",
                data: [450, 1800, 2550, 2500, 2700, 1700, 800, 400]
            }
        ]
    };
    var barData3m = {
        labels: ["-12", "13-17", "18-24", "25-34", "35-44", "45-54", "55-64","65+"],
        datasets: [
            {
                label: "Male",
                fillColor: "rgba(76,148,218,1)",
                strokeColor: "rgba(76,148,218,1)",
                highlightFill: "rgba(76,148,218,0.75)",
                highlightStroke: "rgba(76,148,218,1)",
                data: [16500,57000,84000,102000,82500,64080,29700,18000]
            },
            {
                label: "Female",
                fillColor: "rgba(233,127,117,1)",
                strokeColor: "rgba(233,127,117,1)",
                highlightFill: "rgba(233,127,117,0.75)",
                highlightStroke: "rgba(233,127,117,1)",
                data: [13500,54000,76500,75000,81000,51000,24000,12000]
            }
        ]
    };
    var barData3w = {
        labels: ["-12", "13-17", "18-24", "25-34", "35-44", "45-54", "55-64","65+"],
        datasets: [
            {
                label: "Male",
                fillColor: "rgba(76,148,218,1)",
                strokeColor: "rgba(76,148,218,1)",
                highlightFill: "rgba(76,148,218,0.75)",
                highlightStroke: "rgba(76,148,218,1)",
                data: [3850,13300,19600,23800,19250,14952,6930,4200]
            },
            {
                label: "Female",
                fillColor: "rgba(233,127,117,1)",
                strokeColor: "rgba(233,127,117,1)",
                highlightFill: "rgba(233,127,117,0.75)",
                highlightStroke: "rgba(233,127,117,1)",
                data: [3150,12600,17850,17500,18900,11900,5600,2800]
            }
        ]
    };

    var ctx = document.getElementById("barOptions-locationDaily").getContext("2d");
    var myNewChart = new Chart(ctx).Bar(barDataLocationDaily, barOptions);
    //var ctx = document.getElementById("barOptions-locationvDaily").getContext("2d");
    //var myNewChart = new Chart(ctx).Bar(barDataLocationvDaily, barOptions);
    var ctx = document.getElementById("barOptions-locationWeekly").getContext("2d");
    var myNewChart = new Chart(ctx).Bar(barDataLocationWeekly, barOptions);
    //var ctx = document.getElementById("barOptions-locationvWeekly").getContext("2d");
    //var myNewChart = new Chart(ctx).Bar(barDataLocationvWeekly, barOptions);
    var ctx = document.getElementById("barOptions-locationMonthly").getContext("2d");
    var myNewChart = new Chart(ctx).Bar(barDataLocationMonthly, barOptions);
    //var ctx = document.getElementById("barOptions-locationvMonthly").getContext("2d");
    //var myNewChart = new Chart(ctx).Bar(barDataLocationvMonthly, barOptions);
    //var ctx = document.getElementById("barOptions-gender-age1d").getContext("2d");
    //var myNewChart = new Chart(ctx).Bar(barData1, barOptions);
    //var ctx = document.getElementById("barOptions-gender-age2d").getContext("2d");
    //var myNewChart = new Chart(ctx).Bar(barData2, barOptions);
    //var ctx = document.getElementById("barOptions-gender-age1dv").getContext("2d");
    //var myNewChart = new Chart(ctx).Bar(barData1, barOptions);
    //var ctx = document.getElementById("barOptions-gender-age2dv").getContext("2d");
    //var myNewChart = new Chart(ctx).Bar(barData2, barOptions);
    //var ctx = document.getElementById("barOptions-gender-age1w").getContext("2d");
    //var myNewChart = new Chart(ctx).Bar(barData1w, barOptions);
    //var ctx = document.getElementById("barOptions-gender-age2w").getContext("2d");
    //var myNewChart = new Chart(ctx).Bar(barData2w, barOptions);
    //var ctx = document.getElementById("barOptions-gender-age1wv").getContext("2d");
    //var myNewChart = new Chart(ctx).Bar(barData1w, barOptions);
    //var ctx = document.getElementById("barOptions-gender-age2wv").getContext("2d");
    //var myNewChart = new Chart(ctx).Bar(barData2w, barOptions);
    //var ctx = document.getElementById("barOptions-gender-age1m").getContext("2d");
    //var myNewChart = new Chart(ctx).Bar(barData1m, barOptions);
    //var ctx = document.getElementById("barOptions-gender-age2m").getContext("2d");
    //var myNewChart = new Chart(ctx).Bar(barData2m, barOptions);
    //var ctx = document.getElementById("barOptions-gender-age1mv").getContext("2d");
    //var myNewChart = new Chart(ctx).Bar(barData1m, barOptions);
    //var ctx = document.getElementById("barOptions-gender-age2mv").getContext("2d");
    //var myNewChart = new Chart(ctx).Bar(barData2m, barOptions);
    //var ctx = document.getElementById("barOptions-gender-ageDaily").getContext("2d");
    //var myNewChart = new Chart(ctx).Bar(barData3, barOptions);
    //var ctx = document.getElementById("barOptions-gender-ageWeekly").getContext("2d");
    //var myNewChart = new Chart(ctx).Bar(barData3w, barOptions);
    //var ctx = document.getElementById("barOptions-gender-ageMonthly").getContext("2d");
    //var myNewChart = new Chart(ctx).Bar(barData3m, barOptions);
    //var ctx = document.getElementById("barOptions-gender-ageDailyv").getContext("2d");
    //var myNewChart = new Chart(ctx).Bar(barData3, barOptions);
    //var ctx = document.getElementById("barOptions-gender-ageWeeklyv").getContext("2d");
    //var myNewChart = new Chart(ctx).Bar(barData3w, barOptions);
    //var ctx = document.getElementById("barOptions-gender-ageMonthlyv").getContext("2d");
    //var myNewChart = new Chart(ctx).Bar(barData3m, barOptions);

    document.getElementById('wg6').click();
    document.getElementById('wg5').click();
    document.getElementById('wg4').click();
    document.getElementById('wg3').click();
    document.getElementById('wg2').click();
    document.getElementById('wg1').click();
    document.getElementById('mg6').click();
    document.getElementById('mg5').click();
    document.getElementById('mg4').click();
    document.getElementById('mg3').click();
    document.getElementById('mg2').click();
    document.getElementById('mg1').click();
    document.getElementById('dg6').click();
    document.getElementById('dg5').click();
    document.getElementById('dg4').click();
    document.getElementById('dg3').click();
    document.getElementById('dg2').click();
    document.getElementById('dg1').click();
    document.getElementById('vdg1').click();
    document.getElementById('vdg2').click();
    document.getElementById('vdg3').click();
    document.getElementById('vdg4').click();
    document.getElementById('vdg5').click();
    document.getElementById('vdg6').click();
    document.getElementById('vwg1').click();
    document.getElementById('vwg2').click();
    document.getElementById('vwg3').click();
    document.getElementById('vwg4').click();
    document.getElementById('vwg5').click();
    document.getElementById('vwg6').click();
    document.getElementById('vmg1').click();
    document.getElementById('vmg2').click();
    document.getElementById('vmg3').click();
    document.getElementById('vmg4').click();
    document.getElementById('vmg5').click();
    document.getElementById('vmg6').click();

    document.getElementById('dailyg').className = "tab-pane active";
    document.getElementById('weeklyg').className = "tab-pane";
    document.getElementById('monthlyg').className = "tab-pane";
    document.getElementById('dailyViberg').className = "tab-pane";
    document.getElementById('weeklyViberg').className = "tab-pane";
    document.getElementById('monthlyViberg').className = "tab-pane";

    /*

  //  $('body').jpreLoader();
    console.log("average Interactions Per User " + averageInteractionsPerUser(cn));
    console.log("Users Who Clicked On Tv " + usersWhoClickedOnTv(cn));
    console.log("Users Who Clicked On Tv Percentage " + usersWhoClickedOnTvPercentage(cn));
    console.log("Users Who Clicked On Movie " + usersWhoClickedOnMovie(cn));
    console.log("Users Who Clicked On Movie Percentage " + usersWhoClickedOnMoviePercentage(cn));
    console.log("Users Who Clicked On Movie Comedy " + usersWhoClickedOnMovieComedy(cn));
    console.log("Users Who Clicked On Movie Comedy Percentage " + usersWhoClickedOnMovieComedyPercentage(cn));
    console.log("Users Who Clicked On Movie Drama " + usersWhoClickedOnMovieDrama(cn));
    console.log("Users Who Clicked On Movie Drama Percentage " + usersWhoClickedOnMovieDramaPercentage(cn));
    console.log("Users Who Clicked On Movie Documentary " + usersWhoClickedOnMovieDocumentary(cn));
    console.log("Users Who Clicked On Movie Documentary Percentage " + usersWhoClickedOnMovieDocumentaryPercentage(cn));
    console.log("Users Who Clicked On Movie Surprise " + usersWhoClickedOnMovieSurprise(cn));
    console.log("Users Who Clicked On Movie Surprise Percentage " + usersWhoClickedOnMovieSurprisePercentage(cn));
    console.log("Users Who Clicked On Tv Comedy " + usersWhoClickedOnTvComedy(cn));
    console.log("Users Who Clicked On Tv Comedy Percentage " + usersWhoClickedOnTvComedyPercentage(cn));
    console.log("Users Who Clicked On Tv Drama " + usersWhoClickedOnTvDrama(cn));
    console.log("Users Who Clicked On Tv Drama Percentage " + usersWhoClickedOnTvDramaPercentage(cn));
    console.log("Users Who Clicked On Tv Documentary " + usersWhoClickedOnTvDocumentary(cn));
    console.log("Users Who Clicked On Tv Documentary Percentage " + usersWhoClickedOnTvDocumentaryPercentage(cn));
    console.log("Users Who Clicked On Tv Surprise " + usersWhoClickedOnTvSurprise(cn));
    console.log("Users Who Clicked On Tv Surprise Percentage " + usersWhoClickedOnTvSurprisePercentage(cn));
    console.log(mostPopularTitles(cn));


    */


};

Template.CampaignsDetails.events({

    "click #btnDaily": function(e, t) {
        var selectedValue = t.$("#platform").val();
        if (selectedValue == "Facebook") {
            e.preventDefault();
            document.getElementById('dailyg').className = "tab-pane active";
            document.getElementById('weeklyg').className = "tab-pane";
            document.getElementById('monthlyg').className = "tab-pane";
            document.getElementById('dailyViberg').className = "tab-pane";
            document.getElementById('weeklyViberg').className = "tab-pane";
            document.getElementById('monthlyViberg').className = "tab-pane";
        }
        else {
            e.preventDefault();
            document.getElementById('dailyg').className = "tab-pane";
            document.getElementById('weeklyg').className = "tab-pane";
            document.getElementById('monthlyg').className = "tab-pane";
            document.getElementById('dailyViberg').className = "tab-pane active";
            document.getElementById('weeklyViberg').className = "tab-pane";
            document.getElementById('monthlyViberg').className = "tab-pane";
        }
    },
    "click #btnWeekly": function(e, t) {
        var selectedValue = t.$("#platform").val();
        if (selectedValue == "Facebook") {
            e.preventDefault();
            document.getElementById('dailyg').className = "tab-pane";
            document.getElementById('weeklyg').className = "tab-pane active";
            document.getElementById('monthlyg').className = "tab-pane";
            document.getElementById('dailyViberg').className = "tab-pane";
            document.getElementById('weeklyViberg').className = "tab-pane";
            document.getElementById('monthlyViberg').className = "tab-pane";
        }
        else {
            e.preventDefault();
            document.getElementById('dailyg').className = "tab-pane";
            document.getElementById('weeklyg').className = "tab-pane";
            document.getElementById('monthlyg').className = "tab-pane";
            document.getElementById('dailyViberg').className = "tab-pane";
            document.getElementById('weeklyViberg').className = "tab-pane active";
            document.getElementById('monthlyViberg').className = "tab-pane";
        }
    },
    "click #btnMonthly": function(e, t) {
        var selectedValue = t.$("#platform").val();
        if (selectedValue == "Facebook") {
            e.preventDefault();
            document.getElementById('dailyg').className = "tab-pane";
            document.getElementById('weeklyg').className = "tab-pane";
            document.getElementById('monthlyg').className = "tab-pane active";
            document.getElementById('dailyViberg').className = "tab-pane";
            document.getElementById('weeklyViberg').className = "tab-pane";
            document.getElementById('monthlyViberg').className = "tab-pane";
        }
        else {
            e.preventDefault();
            document.getElementById('dailyg').className = "tab-pane";
            document.getElementById('weeklyg').className = "tab-pane";
            document.getElementById('monthlyg').className = "tab-pane";
            document.getElementById('dailyViberg').className = "tab-pane";
            document.getElementById('weeklyViberg').className = "tab-pane";
            document.getElementById('monthlyViberg').className = "tab-pane active";
        }
    },
    "change #platform": function(e, t){
        var selectedValue = t.$("#platform").val();
        if (selectedValue == "Facebook") {
            if(document.getElementById('dailyViberg').className == "tab-pane active") {
                e.preventDefault();
                document.getElementById('dailyg').className = "tab-pane active";
                document.getElementById('weeklyg').className = "tab-pane";
                document.getElementById('monthlyg').className = "tab-pane";
                document.getElementById('dailyViberg').className = "tab-pane";
                document.getElementById('weeklyViberg').className = "tab-pane";
                document.getElementById('monthlyViberg').className = "tab-pane";
            }
            else if (document.getElementById('weeklyViberg').className == "tab-pane active") {
                e.preventDefault();
                document.getElementById('dailyg').className = "tab-pane";
                document.getElementById('weeklyg').className = "tab-pane active";
                document.getElementById('monthlyg').className = "tab-pane";
                document.getElementById('dailyViberg').className = "tab-pane";
                document.getElementById('weeklyViberg').className = "tab-pane";
                document.getElementById('monthlyViberg').className = "tab-pane";
            }
            else {
                e.preventDefault();
                document.getElementById('dailyg').className = "tab-pane";
                document.getElementById('weeklyg').className = "tab-pane";
                document.getElementById('monthlyg').className = "tab-pane active";
                document.getElementById('dailyViberg').className = "tab-pane";
                document.getElementById('weeklyViberg').className = "tab-pane";
                document.getElementById('monthlyViberg').className = "tab-pane";
            }
        }
        else {
            if(document.getElementById('dailyg').className == "tab-pane active") {
                e.preventDefault();
                document.getElementById('dailyg').className = "tab-pane";
                document.getElementById('weeklyg').className = "tab-pane";
                document.getElementById('monthlyg').className = "tab-pane";
                document.getElementById('dailyViberg').className = "tab-pane active";
                document.getElementById('weeklyViberg').className = "tab-pane";
                document.getElementById('monthlyViberg').className = "tab-pane";
            }
            else if (document.getElementById('weeklyg').className == "tab-pane active") {
                e.preventDefault();
                document.getElementById('dailyg').className = "tab-pane";
                document.getElementById('weeklyg').className = "tab-pane";
                document.getElementById('monthlyg').className = "tab-pane";
                document.getElementById('dailyViberg').className = "tab-pane";
                document.getElementById('weeklyViberg').className = "tab-pane active";
                document.getElementById('monthlyViberg').className = "tab-pane";
            }
            else {
                e.preventDefault();
                document.getElementById('dailyg').className = "tab-pane";
                document.getElementById('weeklyg').className = "tab-pane";
                document.getElementById('monthlyg').className = "tab-pane";
                document.getElementById('dailyViberg').className = "tab-pane";
                document.getElementById('weeklyViberg').className = "tab-pane";
                document.getElementById('monthlyViberg').className = "tab-pane active";
            }
        }
    }
});
var setTime=function(time){
    var start=new Date(), end=new Date();
    if(time=="Today"){
        start.setHours(0,0,0,0);
        end.setHours(23,59,59,999);
    }
    if(time=="Yesterday"){
        start.setDate(start.getDate() - 1);
        start.setHours(0,0,0,0);
        end.setDate(end.getDate() - 1);
        end.setHours(23,59,59,999);
    }
    if(time=="ThisWeek"){
        var daytoset=1,distance; // 1 is Monday
        var currentDay = start.getDay();
        if(currentDay==0)//today is sunday
            distance = -6;
        else
            distance= daytoset - currentDay;

        start.setDate(start.getDate() + distance);
        start.setHours(0,0,0,0);

        end.setHours(23,59,59,999);
    }
    if(time=="LastWeek"){
        var daytoset=1,distance; // 1 is Monday
        var currentDay = start.getDay();
        if(currentDay==0)//today is sunday
            distance = -6;
        else
            distance= daytoset - currentDay;

        start.setDate(start.getDate() + distance-7);
        start.setHours(0,0,0,0);
        end.setDate(start.getDate() + distance-1);
        end.setHours(23,59,59,999);
    }
    if(time=="ThisMonth"){

        start.setDate(1);
        start.setHours(0,0,0,0);

        end.setHours(23,59,59,999);
    }
    if(time=="LastMonth"){

        start.setDate(1);
        start.setHours(0,0,0,0);
        start.setMonth(start.getMonth() - 1);

        end=new Date(end.getFullYear(), end.getMonth(), 0);
        end.setHours(23,59,59,999);
    }
    if(time=="All"){
        start.setYear(2015);
        end.setHours(23,59,59,999);
    }

    return {start: start, end:end};
}
Template.CampaignsDetails.helpers({

});

Template.CampaignsDetails.userParticipation = function() {
    return Tracks.find().count();
};

Template.CampaignsDetailsDetailsForm.rendered = function() {
	

	pageSession.set("campaignsDetailsDetailsFormInfoMessage", "");
	pageSession.set("campaignsDetailsDetailsFormErrorMessage", "");

	$('.datetimepicker').datetimepicker();
	$("input[type='file']").fileinput();
	$("select[data-role='tagsinput']").tagsinput();
	$(".bootstrap-tagsinput").addClass("form-control");
	$("input[autofocus]").focus();
    var titles = [];

    var map = AmCharts.makeChart( "world-mapday", {
        "type": "map",
        "theme": "light",
        "colorSteps": 10,
        "dataProvider": {
            "map": "worldLow",
            "getAreasFromMap": true,
            "zoomLevel": 0.9,
            "areas": []
        },
        "areasSettings": {
            "autoZoom": true,
            "balloonText": "[[title]]: <strong>[[value]]</strong>",
            "balloonText": "[[title]]: <strong>[[value]]</strong>"
        },
        "valueLegend": {
            "right": 10,
            "minValue": "little",
            "maxValue": "a lot!"
        },
        "zoomControl": {
            "minZoomLevel": 0.9
        },
        "titles": titles,
        "listeners":[{"event":"init", "method":updateHeatmap}]
    });
    var map = AmCharts.makeChart( "world-mapweek", {
        "type": "map",
        "theme": "light",
        "colorSteps": 10,
        "dataProvider": {
            "map": "worldLow",
            "getAreasFromMap": true,
            "zoomLevel": 0.9,
            "areas": []
        },
        "areasSettings": {
            "autoZoom": true,
            "balloonText": "[[title]]: <strong>[[value]]</strong>"
        },
        "valueLegend": {
            "right": 10,
            "minValue": "little",
            "maxValue": "a lot!"
        },
        "zoomControl": {
            "minZoomLevel": 0.9
        },
        "titles": titles,
        "listeners":[{"event":"init", "method":updateHeatmap}]
    }); var map = AmCharts.makeChart( "world-mapmonth", {
        "type": "map",
        "theme": "light",
        "colorSteps": 10,
        "dataProvider": {
            "map": "worldLow",
            "getAreasFromMap": true,
            "zoomLevel": 0.9,
            "areas": []
        },
        "areasSettings": {
            "autoZoom": true,
            "balloonText": "[[title]]: <strong>[[value]]</strong>"
        },
        "valueLegend": {
            "right": 10,
            "minValue": "little",
            "maxValue": "a lot!"
        },
        "zoomControl": {
            "minZoomLevel": 0.9
        },
        "titles": titles,
        "listeners":[{"event":"init", "method":updateHeatmap}]
    });
    function updateHeatmap(event) {
        var map = event.chart;
        if ( map.dataGenerated )
            return;
        if ( map.dataProvider.areas.length === 0 ) {
            setTimeout( updateHeatmap, 100 );
            return;
        }
        for ( var i = 0; i < map.dataProvider.areas.length; i++ ) {
            map.dataProvider.areas[ i ].value=0;
        }
        for ( var i = 0; i < mapdataarr.length; i++ ) {
            var obj = _.find(map.dataProvider.areas, function(obj) { return obj.title==mapdataarr[i].name });
            obj.value=mapdataarr[i].value;
        }
        // for ( var i = 0; i < map.dataProvider.areas.length; i++ ) {
        //       console.log (map.dataProvider.areas[ i ].title);
        //    }
        map.dataGenerated = true;
        map.validateNow();
    }


};

Template.CampaignsDetailsDetailsForm.events({
	"submit": function(e, t) {
		e.preventDefault();
		pageSession.set("campaignsDetailsDetailsFormInfoMessage", "");
		pageSession.set("campaignsDetailsDetailsFormErrorMessage", "");

		var self = this;

		function submitAction(msg) {
			var campaignsDetailsDetailsFormMode = "read_only";
			if(!t.find("#form-cancel-button")) {
				switch(campaignsDetailsDetailsFormMode) {
					case "insert": {
						$(e.target)[0].reset();
					}; break;

					case "update": {
						var message = msg || "Saved.";
						pageSession.set("campaignsDetailsDetailsFormInfoMessage", message);
					}; break;
				}
			}

			/*SUBMIT_REDIRECT*/
		}

		function errorAction(msg) {
			msg = msg || "";
			var message = msg.message || msg || "Error.";
			pageSession.set("campaignsDetailsDetailsFormErrorMessage", message);
		}

		validateForm(
			$(e.target),
			function(fieldName, fieldValue) {

			},
			function(msg) {

			},
			function(values) {
				

				
			}
		);

		return false;
	},
	"click #form-cancel-button": function(e, t) {
		e.preventDefault();

		

		/*CANCEL_REDIRECT*/
	},
	"click #form-close-button": function(e, t) {
		e.preventDefault();

		Router.go("campaigns", {});
	},
	"click #form-back-button": function(e, t) {
		e.preventDefault();

		Router.go("campaigns", {});
	}

	
});

Template.CampaignsDetailsDetailsForm.helpers({
    "textAttr": function (infoType, nowTime, lastTime) {
        var now = eval(infoType)(nowTime, this.campaign.name);
        var last = eval(infoType)(lastTime, this.campaign.name);
        if(last == 0 || now[0] == last[0])
            return {class: 'text-muted'};

        if(now[0] > last[0])
            return {class: 'text-success'};

        return {class: 'text-danger'};
    },
    "caretAttr": function (infoType, nowTime, lastTime) {
        var now = eval(infoType)(nowTime, this.campaign.name);
        var last = eval(infoType)(lastTime, this.campaign.name);
        if(last == 0 || now[0] == last[0])
            return {};

        if(now[0] > last[0])
            return {class: 'fa fa-caret-up'};

        return {class: 'fa fa-caret-down'};
    },
    "Precentage": function(fnName,time) {
        var now =  eval(fnName)(time, this.campaign.name);
        if(time== "Today")
            time="Yesterday";
        else if(time== "ThisMonth")
            time="LastMonth";
        else if(time== "ThisWeek")
            time="LastWeek";
        var last = eval(fnName)(time, this.campaign.name);
        if(last == 0)
            return "Not available"

        if(now-last == 0)
            return 0;

        var result = (now-last)/last * 100;
        result = result.toFixed(2)+"%";

        return result;
    },
    "Views": function(time) {
        return Views(time, this.campaign.name);
    },
    "ViewsViber" : function(time) {
        return ViewsViber(time, this.campaign.name);
    },
    "Interactions": function(time) {
        return Interactions(time, this.campaign.name);
    },
    "InteractionsViber": function(time) {
        return InteractionsViber(time, this.campaign.name);
    },
    "RepeatedUsers": function(time) {
        return RepeatedUsers(time, this.campaign.name);
    },
    "RepeatedUsersViber": function(time) {
        return RepeatedUsersViber(time, this.campaign.name);
    },
    "UniqueUsers": function(time) {
        return UniqueUsers(time, this.campaign.name);
    },
    "UniqueUsersViber": function(time) {
        return UniqueUsersViber(time, this.campaign.name);
    },
    "TimeSpentPerUser": function(time) {
        return TimeSpentPerUser(time, this.campaign.name);
    },
    "TimeSpentPerUserViber": function(time) {
        return TimeSpentPerUserViber(time, this.campaign.name);
    },
    "RepeatUsePerUser": function(time) {
        return RepeatUsePerUser(time, this.campaign.name);
    },
    "RepeatUsePerUserViber": function(time) {
        return RepeatUsePerUserViber(time, this.campaign.name);
    },
	"infoMessage": function() {
		return pageSession.get("campaignsDetailsDetailsFormInfoMessage");
	},
	"errorMessage": function() {
		return pageSession.get("campaignsDetailsDetailsFormErrorMessage");
	},
    "graphDate": function(dateType){

        switch (dateType) {
            case "Day":
                var today=new Date();
                var options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
                return today.toLocaleString("en-US",options);

                break;
            case "Week":
                var currentDate = new Date();
                var currentDay = currentDate.getDay();
                var dateOfSunday = currentDate.getDate() - currentDay;
                var sunday=new Date();
                sunday.setDate(dateOfSunday);
                var saterday=new Date();
                saterday.setDate(dateOfSunday+6);
                var options = {year: 'numeric', month: 'long', day: 'numeric' };
                return sunday.toLocaleString("en-US",options)+" to "+saterday.toLocaleString("en-US",options);
                break;
            case "Month":
                var currentDate=new Date();
                var options = {year: 'numeric', month: 'long' };
                return currentDate.toLocaleString("en-US",options);
                break;
        }
        return dateType;

    },
    "ProxyTrack_campaignName": function() {
        var c = ProxyTracks.findOne({sectionName: "appmedia"});
        return c.campaignName;
    },
    "ProxyTrack_url": function() {
        var c = ProxyTracks.findOne({sectionName: "appmedia"});
        return c.url;
    },
    "ProxyTrack_msg": function() {
        var c = ProxyTracks.findOne({sectionName: "appmedia"});
        return c.message;
    },
    "ProxyTrack_type": function() {
        var c = ProxyTracks.findOne({sectionName: "appmedia"});
        return c.type;
    },
    "ProxyTrack_platform": function() {
        var c = ProxyTracks.findOne({sectionName: "appmedia"});
        return c.platform;
    },
    "Males": function(time) {
        return Males(time, this.campaign.name);
    },
    "MalesPercentage": function(time) {
        return MalesPercentage(time, this.campaign.name);
    },
    "Females": function(time) {
        return Females(time, this.campaign.name);
    },
    "FemalesPercentage": function(time) {
        return FemalesPercentage(time, this.campaign.name);
    },
    "Gender": function(time) {
        return Gender(time, this.campaign.name);
    }
});



var mapdataarr=[{name:"Russia", value:5},
    {name:"United States", value:15},
    {name:"Thailand", value:10},
    {name:"Australia", value:20},
    {name:"Sweden", value:50},
    {name:"Iceland", value:15},
    {name:"Guatemala", value:10},
    {name:"Kenya", value:20},
    {name:"Iran", value:10},
    {name:"Mali", value:40},
    {name:"Brazil", value:18}];



var averageInteractionsPerUser = function(thisCampaignName) {
    var interactions = Interactions("All", thisCampaignName);
    var users = UniqueUsers("All", thisCampaignName);

    if (interactions==0 && users==0)
        return 0;

    return (interactions /users).toFixed(2);
}

var usersWhoClicked = function (thisCampaignName, name) {
    var messagesArr = Tracks.find({campaignName: thisCampaignName}).fetch().map(function (x) {
        return x.message;
    });
    var count=0;
    for(var i = 0; i < messagesArr.length; i++) {
        if(messagesArr[i].indexOf(name) > -1)
            count++;
    }

    return count;
}

var usersWhoClickedOnTv = function (thisCampaignName) {
    return usersWhoClicked(thisCampaignName, "tvshow");
}

var usersWhoClickedOnTvPercentage = function (thisCampaignName) {
    return (usersWhoClickedOnTv(thisCampaignName) /(usersWhoClickedOnTv(thisCampaignName)+usersWhoClickedOnMovie(thisCampaignName)) * 100).toFixed(2);
}

var usersWhoClickedOnMovie = function (thisCampaignName) {
    return usersWhoClicked(thisCampaignName, "movie");
}


var usersWhoClickedOnMoviePercentage = function (thisCampaignName) {
    return (100-usersWhoClickedOnTvPercentage(thisCampaignName)).toFixed(2);
}

var usersWhoClickedOnMovieCategory = function (thisCampaignName) {
    return usersWhoClickedOnMovieComedy(thisCampaignName)+usersWhoClickedOnMovieDrama(thisCampaignName)+usersWhoClickedOnMovieDocumentary(thisCampaignName)+usersWhoClickedOnMovieSurprise(thisCampaignName);
}

var usersWhoClickedOnMovieComedy = function (thisCampaignName) {
    return usersWhoClicked(thisCampaignName, "movie,Comedy");
}

var usersWhoClickedOnMovieComedyPercentage = function (thisCampaignName) {
    return (usersWhoClickedOnMovieComedy(thisCampaignName) / usersWhoClickedOnMovieCategory(thisCampaignName) * 100).toFixed(2);
}

var usersWhoClickedOnMovieDrama = function (thisCampaignName) {
    return usersWhoClicked(thisCampaignName, "movie,Drama");
}

var usersWhoClickedOnMovieDramaPercentage = function (thisCampaignName) {
    return (usersWhoClickedOnMovieDrama(thisCampaignName) / usersWhoClickedOnMovieCategory(thisCampaignName) * 100).toFixed(2);
}

var usersWhoClickedOnMovieDocumentary = function (thisCampaignName) {
    return usersWhoClicked(thisCampaignName, "movie,Documentary");
}

var usersWhoClickedOnMovieDocumentaryPercentage = function (thisCampaignName) {
    return (usersWhoClickedOnMovieDocumentary(thisCampaignName) / usersWhoClickedOnMovieCategory(thisCampaignName) * 100).toFixed(2);
}

var usersWhoClickedOnMovieSurprise = function (thisCampaignName) {
    return usersWhoClicked(thisCampaignName, "movie,Surprise");
}

var usersWhoClickedOnMovieSurprisePercentage = function (thisCampaignName) {
    return (usersWhoClickedOnMovieSurprise(thisCampaignName) / usersWhoClickedOnMovieCategory(thisCampaignName) * 100).toFixed(2);
}

var usersWhoClickedOnTvCategory = function (thisCampaignName) {
    return usersWhoClickedOnTvComedy(thisCampaignName)+usersWhoClickedOnTvDrama(thisCampaignName)+usersWhoClickedOnTvDocumentary(thisCampaignName)+usersWhoClickedOnTvSurprise(thisCampaignName);
}

var usersWhoClickedOnTvComedy = function (thisCampaignName) {
    return usersWhoClicked(thisCampaignName, "tvshow,Comedy");
}

var usersWhoClickedOnTvComedyPercentage = function (thisCampaignName) {
    return (usersWhoClickedOnTvComedy(thisCampaignName) / usersWhoClickedOnTvCategory(thisCampaignName) * 100).toFixed(2);
}

var usersWhoClickedOnTvDrama = function (thisCampaignName) {
    return usersWhoClicked(thisCampaignName, "tvshow,Drama");
}

var usersWhoClickedOnTvDramaPercentage = function (thisCampaignName) {
    return (usersWhoClickedOnTvDrama(thisCampaignName) / usersWhoClickedOnTvCategory(thisCampaignName) * 100).toFixed(2);
}

var usersWhoClickedOnTvDocumentary = function (thisCampaignName) {
    return usersWhoClicked(thisCampaignName, "tvshow,Documentary");
}

var usersWhoClickedOnTvDocumentaryPercentage = function (thisCampaignName) {
    return (usersWhoClickedOnTvDocumentary(thisCampaignName) / usersWhoClickedOnTvCategory(thisCampaignName) * 100).toFixed(2);
}

var usersWhoClickedOnTvSurprise = function (thisCampaignName) {
    return usersWhoClicked(thisCampaignName, "tvshow,Surprise");
}

var usersWhoClickedOnTvSurprisePercentage = function (thisCampaignName) {
    return (usersWhoClickedOnTvSurprise(thisCampaignName) /usersWhoClickedOnTvCategory(thisCampaignName) * 100).toFixed(2);
}

var mostPopularTitles = function (thisCampaignName) {
    var messagesArr = Tracks.find({campaignName: thisCampaignName}).fetch().map(function (x) {
        return x.message;
    });

    var popularTitles = {};

    for(var i = 0; i < messagesArr.length; i++) {
        var startIndex = messagesArr[i].indexOf("Rm_");
        if(startIndex > -1) {
            var endIndex = messagesArr[i].indexOf(",", startIndex);
            var title;
            if (endIndex > -1)
                title = messagesArr[i].substring(startIndex+3, endIndex);
            else
                title = messagesArr[i].substring(startIndex+3);
            var value = popularTitles[title];
            if(isNaN(value))
                value = 0;
            popularTitles[title] = ++value;
        }
    }

    popularTitles = getSortedTitles(popularTitles);

    var tenPopularTitles = [];
    for (var i=9; i>=0; i--)
        tenPopularTitles.push(popularTitles[popularTitles.length-i-1]);

    return tenPopularTitles;
}

function getSortedTitles(obj) {
    var keys = []; for(var key in obj) keys.push(key);
    return keys.sort(function(a,b){return obj[a]-obj[b]});
}



