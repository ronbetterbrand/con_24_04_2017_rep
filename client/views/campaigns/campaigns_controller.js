this.CampaignsController = RouteController.extend({
	template: "Campaigns",


	yieldTemplates: {
		/*YIELD_TEMPLATES*/
	},

	onBeforeAction: function() {
		this.next();
	},

	action: function() {
		if(this.isReady()) { this.render(); } else { this.render("loading"); }
		/*ACTION_FUNCTION*/
	},

	isReady: function() {


		var subs = [
			Meteor.subscribe("campaigns"),
			Meteor.subscribe("cms_campaign_list")
		];
		var ready = true;
		_.each(subs, function(sub) {
			if(!sub.ready())
				ready = false;
		});
		return ready;
	},

	data: function() {


		return {
			params: this.params || {},
			campaigns: Campaigns.find({}, {}, {limit: 10}),
			cms_campaign_list: CmsCampaigns.find({}, {sort:[["campaignName","desc"]]})
		};
		/*DATA_FUNCTION*/
	},

	onAfterAction: function() {

	}
});