var pageSession = new ReactiveDict();
pageSession.set("showHelp", true);
Template.Campaigns.onCreated(function(){
	console.log("onCreated");

});

Template.Campaigns.onRendered(function(){
	if(!pageSession.get("showHelp")) {
		$("#chatsuite-introdution").addClass("perhide");
	}
});


Template.Campaigns.rendered = function() {

	console.log("rendered");
	$('[data-toggle="tooltip"]').tooltip();


	// Options for Campaigns View top box chart

    var chartIncomeData = [
        {
            label: "line",
            data: [ [1, 10], [2, 26], [3, 16], [4, 36], [5, 32], [6, 51] ]
        }
    ];

    var chartIncomeOptions = {
        series: {
            lines: {
                show: true,
                lineWidth: 0,
                fill: true,
                fillColor: "#0BBBEF"

            }
        },
        colors: ["0066cb"],
        grid: {
            show: false
        },
        legend: {
            show: false
        }
    };

    ///???$.plot($("#campaigns-views-chart"), chartIncomeData, chartIncomeOptions);

};

Template.Campaigns.events({

    'click .showhide': function(event){
        event.preventDefault();
        var hpanel = $(event.target).closest('div.hpanel');
        var icon = $(event.target).closest('i');
        var body = hpanel.find('div.panel-body');
        var footer = hpanel.find('div.panel-footer');
        body.slideToggle(300);
        footer.slideToggle(200);

        // Toggle icon from up to down
        icon.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
        hpanel.toggleClass('').toggleClass('panel-collapse');
        setTimeout(function () {
            hpanel.resize();
            hpanel.find('[id^=map-]').resize();
        }, 50);
    },

    'click .closebox': function(event){
        event.preventDefault();
        var hpanel = $(event.target).closest('div.hpanel');
        hpanel.remove();
    },

    'click .run-tour-campaigns': function(){
        // Instance the tour
        var tourCampaigns = new Tour({
            backdrop: true,
            onShown: function(tourCampaigns) {

                // ISSUE    - https://github.com/sorich87/bootstrap-tour/issues/189
                // FIX      - https://github.com/sorich87/bootstrap-tour/issues/189#issuecomment-49007822

                // You have to write your used animated effect class
                // Standard animated class
                $('.animated').removeClass('fadeIn');
                // Animate class from animate-panel plugin
                $('.animated-panel').removeClass('fadeIn');

            },
            steps: [
                {
                    element: ".tour-1",
                    title: "Campaigns Status",
                    content: "Quick view of your campaign's status - Active and live, or under construction.",
                    placement: "top"

                },
                {
                    element: ".tour-2",
                    title: "Campaign Views",
                    content: "Number of users that viewed your campaign pre engagement.",
                    placement: "top"

                },
                {
                    element: ".tour-3",
                    title: "Most Popular Campaign",
                    content: "Monitoring your success over time.",
                    placement: "top"

                },
                {
                    element: ".tour-4",
                    title: "Most Active Chat Platform",
                    content: "Monitoring views by platform.",
                    placement: "top"

                },
                {
                    element: ".tour-5",
                    title: "List of Campaigns created",
                    content: "A quick view of your campaigns list. Use the icons on the right to manage your campaign: Set up your campaign specifications; edit the content of your campaign; view statistics and analytics of your campaign.",
                    placement: "top"

                }/*,
                {
                    element: ".tour-6",
                    title: "Search your campaigns",
                    content: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit.",
                    placement: "bottom"

                },
                {
                    element: ".tour-7",
                    title: "Export your results",
                    content: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit.",
                    placement: "bottom"

                },
                {
                    element: ".tour-8",
                    title: "Create a new campaign!",
                    content: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit.",
                    placement: "bottom"

                }*/
            ]});

        // Initialize the tour
        tourCampaigns.init();
        tourCampaigns.restart();
    },

    "submit #dataview-controls": function(e, t) {
		return false;
	},

	"click #dataview-search-button": function(e, t) {
		e.preventDefault();
		var form = $(e.currentTarget).parent();
		if(form) {
			var searchInput = form.find("#dataview-search-input");
			if(searchInput) {
				searchInput.focus();
				var searchString = searchInput.val();
				pageSession.set("CampaignsViewSearchString", searchString);
			}

		}
		return false;
	},

	"keydown #dataview-search-input": function(e, t) {
		if(e.which === 13)
		{
			e.preventDefault();
			var form = $(e.currentTarget).parent();
			if(form) {
				var searchInput = form.find("#dataview-search-input");
				if(searchInput) {
					var searchString = searchInput.val();
					pageSession.set("CampaignsViewSearchString", searchString);
				}

			}
			return false;
		}

		if(e.which === 27)
		{
			e.preventDefault();
			var form = $(e.currentTarget).parent();
			if(form) {
				var searchInput = form.find("#dataview-search-input");
				if(searchInput) {
					searchInput.val("");
					pageSession.set("CampaignsViewSearchString", "");
				}

			}
			return false;
		}

		return true;
	},

	"click #dataview-insert-button": function(e, t) {
		e.preventDefault();
		Router.go("campaigns.insert", {});
	},

	"click #dataview-export-default": function(e, t) {
		e.preventDefault();
		CampaignsViewExport(this.campaigns, "csv");
	},

	"click #dataview-export-csv": function(e, t) {
		e.preventDefault();
		CampaignsViewExport(this.campaigns, "csv");
	},

	"click #dataview-export-tsv": function(e, t) {
		e.preventDefault();
		CampaignsViewExport(this.campaigns, "tsv");
	},

	"click #dataview-export-json": function(e, t) {
		e.preventDefault();
		CampaignsViewExport(this.campaigns, "json");
	},
	"click .close": function(e, t) {

		pageSession.set("showHelp",false)
	}

});

Template.Campaigns.helpers({
	"isEmpty": function() {
		return !this.campaigns || this.campaigns.count() == 0;
	},
	"isNotEmpty": function() {
		return this.campaigns && this.campaigns.count() > 0;
	},
	"isNotFound": function() {
		return this.campaigns && pageSession.get("CampaignsViewSearchString") && CampaignsViewItems(this.campaigns).length == 0;
	}
});

var CampaignsViewItems = function(cursor) {
	if(!cursor) {
		return [];
	}

	var searchString = pageSession.get("CampaignsViewSearchString");
	var sortBy = pageSession.get("CampaignsViewSortBy");
	var sortAscending = pageSession.get("CampaignsViewSortAscending");
	if(typeof(sortAscending) == "undefined") sortAscending = true;

	var raw = cursor.fetch();

	// filter
	var filtered = [];
	if(!searchString || searchString == "") {
		filtered = raw;
	} else {
		searchString = searchString.replace(".", "\\.");
		var regEx = new RegExp(searchString, "i");
		var searchFields = ["name", "description", "sdate", "edate", "active"];
		filtered = _.filter(raw, function(item) {
			var match = false;
			_.each(searchFields, function(field) {
				var value = (getPropertyValue(field, item) || "") + "";

				match = match || (value && value.match(regEx));
				if(match) {
					return false;
				}
			})
			return match;
		});
	}

	// sort
	if(sortBy) {
		filtered = _.sortBy(filtered, sortBy);

		// descending?
		if(!sortAscending) {
			filtered = filtered.reverse();
		}
	}

	return filtered;
};

var CampaignsViewExport = function(cursor, fileType) {
	var data = CampaignsViewItems(cursor);
	var exportFields = ["name", "description", "sdate", "edate", "active"];

	var str = convertArrayOfObjects(data, exportFields, fileType);

	var filename = "export." + fileType;

	downloadLocalResource(str, filename, "application/octet-stream");
};


Template.CampaignsView.rendered = function() {
	pageSession.set("CampaignsViewStyle", "table");
	
};

Template.CampaignsView.events({
	/*"submit #dataview-controls": function(e, t) {
		return false;
	},

	"click #dataview-search-button": function(e, t) {
		e.preventDefault();
		var form = $(e.currentTarget).parent();
		if(form) {
			var searchInput = form.find("#dataview-search-input");
			if(searchInput) {
				searchInput.focus();
				var searchString = searchInput.val();
				pageSession.set("CampaignsViewSearchString", searchString);
			}

		}
		return false;
	},

	"keydown #dataview-search-input": function(e, t) {
		if(e.which === 13)
		{
			e.preventDefault();
			var form = $(e.currentTarget).parent();
			if(form) {
				var searchInput = form.find("#dataview-search-input");
				if(searchInput) {
					var searchString = searchInput.val();
					pageSession.set("CampaignsViewSearchString", searchString);
				}

			}
			return false;
		}

		if(e.which === 27)
		{
			e.preventDefault();
			var form = $(e.currentTarget).parent();
			if(form) {
				var searchInput = form.find("#dataview-search-input");
				if(searchInput) {
					searchInput.val("");
					pageSession.set("CampaignsViewSearchString", "");
				}

			}
			return false;
		}

		return true;
	},

	"click #dataview-insert-button": function(e, t) {
		e.preventDefault();
		Router.go("campaigns.insert", {});
	},

	"click #dataview-export-default": function(e, t) {
		e.preventDefault();
		CampaignsViewExport(this.campaigns, "csv");
	},

	"click #dataview-export-csv": function(e, t) {
		e.preventDefault();
		CampaignsViewExport(this.campaigns, "csv");
	},

	"click #dataview-export-tsv": function(e, t) {
		e.preventDefault();
		CampaignsViewExport(this.campaigns, "tsv");
	},

	"click #dataview-export-json": function(e, t) {
		e.preventDefault();
		CampaignsViewExport(this.campaigns, "json");
	}*/

	
});

Template.CampaignsView.helpers({

	

	"isEmpty": function() {
		return !this.campaigns || this.campaigns.count() == 0;
	},
	"isNotEmpty": function() {
		return this.campaigns && this.campaigns.count() > 0;
	},
	"isNotFound": function() {
		return this.campaigns && pageSession.get("CampaignsViewSearchString") && CampaignsViewItems(this.campaigns).length == 0;
	},
	"searchString": function() {
		return pageSession.get("CampaignsViewSearchString");
	},
	"viewAsTable": function() {
		return pageSession.get("CampaignsViewStyle") == "table";
	},
	"viewAsList": function() {
		return pageSession.get("CampaignsViewStyle") == "list";
	},
	"viewAsGallery": function() {
		return pageSession.get("CampaignsViewStyle") == "gallery";
	}

	
});


Template.CampaignsViewTable.rendered = function() {
	
};

Template.CampaignsViewTable.events({
	"click .th-sortable": function(e, t) {
		console.log(" click sort");
		e.preventDefault();
		var oldSortBy = pageSession.get("CampaignsViewSortBy");
		var newSortBy = $(e.target).attr("data-sort");

		pageSession.set("CampaignsViewSortBy", newSortBy);
		if(oldSortBy == newSortBy) {
			var sortAscending = pageSession.get("CampaignsViewSortAscending") || false;
			pageSession.set("CampaignsViewSortAscending", !sortAscending);
		} else {
			pageSession.set("CampaignsViewSortAscending", true);
		}
	}
});

Template.CampaignsViewTable.helpers({
	"tableItems": function() {
		return CampaignsViewItems(this.campaigns);
	}
});


Template.CampaignsViewTableItems.rendered = function() {
	
};

Template.CampaignsViewTableItems.events({
	"click td": function(e, t) {
		e.preventDefault();
		
		Router.go("campaigns.details", {campaignId: this._id});
		//Router.go("campaigns.edit", {campaignId: this._id});
		return false;
	},

	"click .inline-checkbox": function(e, t) {
		e.preventDefault();

		if(!this || !this._id) return false;

		var fieldName = $(e.currentTarget).attr("data-field");
		if(!fieldName) return false;

		var values = {};
		values[fieldName] = !this[fieldName];

		Campaigns.update({ _id: this._id }, { $set: values });

		return false;
	},

	"click #delete-button": function(e, t) {
		e.preventDefault();
		var me = this;
		bootbox.dialog({
			message: "Are you sure you want to delete this campaign?",
			title: "Delete Campaign",
			animate: false,
			buttons: {
				success: {
					label: "Yes",
					className: "btn-primary",
					callback: function() {
						Campaigns.remove({ _id: me._id });
						var campcmsid=CmsCampaigns.findOne({ campaignName: me.name })._id
						CmsCampaigns.remove({  _id: campcmsid })
					}
				},
				danger: {
					label: "No",
					className: "btn-default"
				}
			}
		});
		return false;
	},
	"click #edit-button": function(e, t) {
		e.preventDefault();
		Router.go("campaigns.edit", {campaignId: this._id});
		return false;
	},
	"click #editor-button": function(e, t) {
		e.preventDefault();

		Router.go("campaigns.editor", {campaignId: this._id});
		return false;
	}


});

Template.CampaignsViewTableItems.helpers({
	"checked": function(value) { return value ? "checked" : "" }
	

	
});