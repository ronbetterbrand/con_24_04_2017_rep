this.SectionsDetailsController = RouteController.extend({
	template: "SectionsDetails",


	yieldTemplates: {
		/*YIELD_TEMPLATES*/
	},

	onBeforeAction: function() {
		this.next();
	},

	action: function() {
		Router.go("cms.sections", {});
		///console.log("we don't have - sections.details.items");
		//this.redirect('sections.details', this.params || {}, { replaceState: true });
		//this.next();
		///???this.redirect('sections.details.items', this.params || {}, { replaceState: true });
		/*ACTION_FUNCTION*/
	},

	isReady: function() {


		var subs = [
			Meteor.subscribe("cms_section_details", this.params.sectionId)
		];
		var ready = true;
		_.each(subs, function(sub) {
			if(!sub.ready())
				ready = false;
		});
		return ready;
	},

	data: function() {


		var data = {
			params: this.params || {},
			cms_section_details: CmsSections.findOne({_id:this.params.sectionId}, {})
		};




		return data;
	},

	onAfterAction: function() {

	}
});