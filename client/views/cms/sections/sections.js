var pageSession = new ReactiveDict();

function getParameterByName(name) {
	name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
		results = regex.exec(location.search);
	return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

Template.Sections.rendered = function() {
	
};

Template.Sections.events({
	"click #dataview-insert-button": function(e, t) {
		e.preventDefault();
		Router.go("cms.sections.insert", {_id: 1}, {query: this.params.query});
	},
	"submit #dataview-controls": function(e, t) {
		return false;
	},

	"click #dataview-search-button": function(e, t) {
		e.preventDefault();
		var form = $(e.currentTarget).parent();
		if(form) {
			var searchInput = form.find("#dataview-search-input");
			if(searchInput) {
				searchInput.focus();
				var searchString = searchInput.val();
				pageSession.set("SectionsViewSearchString", searchString);
			}

		}
		return false;
	},

	"keydown #dataview-search-input": function(e, t) {
		if(e.which === 13)
		{
			e.preventDefault();
			var form = $(e.currentTarget).parent();
			if(form) {
				var searchInput = form.find("#dataview-search-input");
				if(searchInput) {
					var searchString = searchInput.val();
					pageSession.set("SectionsViewSearchString", searchString);
				}

			}
			return false;
		}

		if(e.which === 27)
		{
			e.preventDefault();
			var form = $(e.currentTarget).parent();
			if(form) {
				var searchInput = form.find("#dataview-search-input");
				if(searchInput) {
					searchInput.val("");
					pageSession.set("SectionsViewSearchString", "");
				}

			}
			return false;
		}

		return true;
	}
	
});

Template.Sections.helpers({
	"isNotEmpty": function() {
		return this.cms_section_list && this.cms_section_list.count() > 0;
	},
	"campaignName": function() {
	return this.params.query.id;
}
	
});

var SectionsViewItems = function(cursor) {
	if(!cursor) {
		return [];
	}

	var searchString = pageSession.get("SectionsViewSearchString");
	var sortBy = pageSession.get("SectionsViewSortBy");
	var sortAscending = pageSession.get("SectionsViewSortAscending");
	if(typeof(sortAscending) == "undefined") sortAscending = true;

	var raw = cursor.fetch();

	// filter
	var filtered = [];
	if(!searchString || searchString == "") {
		filtered = raw;
	} else {
		searchString = searchString.replace(".", "\\.");
		var regEx = new RegExp(searchString, "i");
		var searchFields = ["cardID","cardType","sectionName","campaignName", "date"];
		filtered = _.filter(raw, function(item) {
			var match = false;
			_.each(searchFields, function(field) {
				var value = (getPropertyValue(field, item) || "") + "";

				match = match || (value && value.match(regEx));
				if(match) {
					return false;
				}
			})
			return match;
		});
	}

	// sort
	if(sortBy) {
		filtered = _.sortBy(filtered, sortBy);

		// descending?
		if(!sortAscending) {
			filtered = filtered.reverse();
		}
	}

	return filtered;
};

var SectionsViewExport = function(cursor, fileType) {
	var data = SectionsViewItems(cursor);
	var exportFields = ["cardID","cardType","sectionName","campaignName", "date"];

	var str = convertArrayOfObjects(data, exportFields, fileType);

	var filename = "export." + fileType;

	downloadLocalResource(str, filename, "application/octet-stream");
}


Template.SectionsView.rendered = function() {
	pageSession.set("SectionsViewStyle", "table");
	
};

Template.SectionsView.events({
	"submit #dataview-controls": function(e, t) {
		return false;
	},

	"click #dataview-search-button": function(e, t) {
		e.preventDefault();
		var form = $(e.currentTarget).parent();
		if(form) {
			var searchInput = form.find("#dataview-search-input");
			if(searchInput) {
				searchInput.focus();
				var searchString = searchInput.val();
				pageSession.set("SectionsViewSearchString", searchString);
			}

		}
		return false;
	},

	"keydown #dataview-search-input": function(e, t) {
		if(e.which === 13)
		{
			e.preventDefault();
			var form = $(e.currentTarget).parent();
			if(form) {
				var searchInput = form.find("#dataview-search-input");
				if(searchInput) {
					var searchString = searchInput.val();
					pageSession.set("SectionsViewSearchString", searchString);
				}

			}
			return false;
		}

		if(e.which === 27)
		{
			e.preventDefault();
			var form = $(e.currentTarget).parent();
			if(form) {
				var searchInput = form.find("#dataview-search-input");
				if(searchInput) {
					searchInput.val("");
					pageSession.set("SectionsViewSearchString", "");
				}

			}
			return false;
		}

		return true;
	},



	"click #dataview-export-default": function(e, t) {
		e.preventDefault();
		SectionsViewExport(this.cms_section_list, "csv");
	},

	"click #dataview-export-csv": function(e, t) {
		e.preventDefault();
		SectionsViewExport(this.cms_section_list, "csv");
	},

	"click #dataview-export-tsv": function(e, t) {
		e.preventDefault();
		SectionsViewExport(this.cms_section_list, "tsv");
	},

	"click #dataview-export-json": function(e, t) {
		e.preventDefault();
		SectionsViewExport(this.cms_section_list, "json");
	}

	
});

Template.SectionsView.helpers({

	"insertButtonClass": function() {
		return CmsSections.userCanInsert(Meteor.userId(), {}) ? "" : "hidden";
	},

	"isEmpty": function() {
		return !this.cms_section_list || this.cms_section_list.count() == 0;
	},
	"isNotEmpty": function() {
		return this.cms_section_list && this.cms_section_list.count() > 0;
	},
	"isNotFound": function() {
		return this.cms_section_list && pageSession.get("SectionsViewSearchString") && SectionsViewItems(this.cms_section_list).length == 0;
	},
	"searchString": function() {
		return pageSession.get("SectionsViewSearchString");
	},
	"viewAsTable": function() {
		return pageSession.get("SectionsViewStyle") == "table";
	},
	"viewAsList": function() {
		return pageSession.get("SectionsViewStyle") == "list";
	},
	"viewAsGallery": function() {
		return pageSession.get("SectionsViewStyle") == "gallery";
	}

	
});


Template.SectionsViewTable.rendered = function() {

};

Template.SectionsViewTable.events({
	"click .th-sortable": function(e, t) {
		e.preventDefault();
		var oldSortBy = pageSession.get("SectionsViewSortBy");
		var newSortBy = $(e.target).attr("data-sort");

		pageSession.set("SectionsViewSortBy", newSortBy);
		if(oldSortBy == newSortBy) {
			var sortAscending = pageSession.get("SectionsViewSortAscending") || false;
			pageSession.set("SectionsViewSortAscending", !sortAscending);
		} else {
			pageSession.set("SectionsViewSortAscending", true);
		}
	}
});



Template.SectionsViewTable.helpers({
	"tableItems": function()
	{
		var id = getParameterByName('id');
        return SectionsViewItems(CmsSections.find({ campaignName: id  }));
	}
});


Template.SectionsViewTableItems.rendered = function() {
	
};

Template.SectionsViewTableItems.events({
	"click td": function(e, t) {
        var url = window.location.href;
		var cid = url.substring(url.indexOf('&') + 5) +"/items";
		var query = "sname=" + t.data.sectionName;
	    e.preventDefault();
	    Router.go("cms.details", {campaignId: cid}, {query: query});
		return false;
	},
	"click .inline-checkbox": function(e, t) {
		e.preventDefault();

		if(!this || !this._id) return false;

		var fieldName = $(e.currentTarget).attr("data-field");
		if(!fieldName) return false;

		var values = {};
		values[fieldName] = !this[fieldName];

		CmsSections.update({ _id: this._id }, { $set: values });

		return false;
	},

	"click #delete-button": function(e, t) {
		e.preventDefault();
		var me = this;
		bootbox.dialog({
			message: "Delete? Are you sure?",
			title: "Delete",
			animate: false,
			buttons: {
				success: {
					label: "Yes",
					className: "btn-success",
					callback: function() {
						CmsSections.remove({ _id: me._id });
					}
				},
				danger: {
					label: "No",
					className: "btn-default"
				}
			}
		});
		return false;
	},
	"click #edit-button": function(e, t) {
		e.preventDefault();
		Router.go("cms.sections.edit", {sectionId: this._id},{query: UI._parentData(1).params.query});
		return false;
	}
});

Template.SectionsViewTableItems.helpers({
	"checked": function(value) { return value ? "checked" : "" }, 
	"editButtonClass": function() {
		return CmsSections.userCanUpdate(Meteor.userId(), this) ? "" : "hidden";
	},

	"deleteButtonClass": function() {
		return CmsSections.userCanRemove(Meteor.userId(), this) ? "" : "hidden";
	}
});
