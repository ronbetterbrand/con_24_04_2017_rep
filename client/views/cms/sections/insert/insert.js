var pageSession = new ReactiveDict();
pageSession.set("showHelp", true);
Template.SectionsInsert.rendered = function() {

};

Template.SectionsInsert.events({

});

Template.SectionsInsert.helpers({

});


Template.SectionsInsertInsertForm.onRendered(function(){
	if(!pageSession.get("showHelp")) {
		$("#chatsuite-introdution").addClass("perhide");
	}
});

Template.SectionsInsertInsertForm.rendered = function() {

	//console.log("here...");

	pageSession.set("sectionsInsertInsertFormInfoMessage", "");
	pageSession.set("sectionsInsertInsertFormErrorMessage", "");

	$(".input-group.date").each(function() {
		var format = $(this).find("input[type='text']").attr("data-format");

		if(format) {
			format = format.toLowerCase();
		}
		else {
			format = "mm/dd/yyyy";
		}

		$(this).datepicker({
			autoclose: true,
			todayHighlight: true,
			todayBtn: true,
			forceParse: false,
			keyboardNavigation: false,
			format: format
		});
	});

	$("input[type='file']").fileinput();
	$("select[data-role='tagsinput']").tagsinput();
	$(".bootstrap-tagsinput").addClass("form-control");
	$("input[autofocus]").focus();

	//$('.input-group.sectionName').validate({
	//	rules: {
	//		sectionName: {
	//			required: true,
	//			minlength: 6
	//		}
	//	},messages: {
	//		sectionName: {
	//			required: "You must enter an sectionName address."
	//		}
	//	}
	//});

};

Template.SectionsInsertInsertForm.events({
	"submit": function(e, t) {
		e.preventDefault();
		pageSession.set("sectionsInsertInsertFormInfoMessage", "");
		pageSession.set("sectionsInsertInsertFormErrorMessage", "");

		var self = this;
		var isApply = -1;

		function submitAction(msg) {
			var sectionsInsertInsertFormMode = "insert";
			if(!t.find("#form-cancel-button")) {
				switch(sectionsInsertInsertFormMode) {
					case "insert": {
						$(e.target)[0].reset();
					}; break;

					case "update": {
						var message = msg || "Saved.";
						pageSession.set("sectionsInsertInsertFormInfoMessage", message);
					}; break;
				}
			}

			Router.go("cms.sections.details", {sectionId: newId});
		}

		function errorAction(msg) {
			msg = msg || "";
			var message = msg.message || msg || "Error.";
			pageSession.set("sectionsInsertInsertFormErrorMessage", message);
		}

		validateForm(
			$(e.target),
			function(fieldName, fieldValue) {
				//console.log("f="+fieldName+" v="+fieldValue);

				//var c = CmsSections.findOne({"sectionName": fieldValue});
				//console.log ("c="+ c.campaignName);

				if( fieldName=="sectionName"){

					if(fieldValue.toString().length < 3){
						bootbox.alert("Section name length >=3 ", function() {
							//Router.go("/sections?id="+"c2", {});
							return false;
						});
					}
					//else if( c.sectionName==fieldValue ){
					//	bootbox.alert("Please use a unique section name", function() {
					//		Router.go("sections", {});
					//		return;
					//	});
					//}
					else{
						isApply=0;
					}

				}

			},
			function(msg) {
				console.log("msg="+msg);
			},
			function(values) {

				if (isApply==0){
					newId = CmsSections.insert(values, function(e)
					{
						if(e) errorAction(e); else submitAction();
						history.back();
					});
				}

			}
		);

		return false;
	},
	"click #form-cancel-button": function(e, t) {
		
		e.preventDefault();

		Router.go("cms.sections",  {_id: 1}, {query: this.params.query})
	},
	"click #form-close-button": function(e, t) {
		e.preventDefault();

		/*CLOSE_REDIRECT*/
	},
	"click #form-back-button": function(e, t) {
		e.preventDefault();

		/*BACK_REDIRECT*/
	},
	"click .close": function(e, t) {

		pageSession.set("showHelp",false)
	}


});

Template.SectionsInsertInsertForm.helpers({
	"infoMessage": function() {
		return pageSession.get("sectionsInsertInsertFormInfoMessage");
	},
	"errorMessage": function() {
		return pageSession.get("sectionsInsertInsertFormErrorMessage");
	},
	"nextCampaignName": function() {
		return this.params.query.id;
	}

});
