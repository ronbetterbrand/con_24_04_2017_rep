this.SectionsInsertController = RouteController.extend({
	template: "SectionsInsert",


	yieldTemplates: {
		/*YIELD_TEMPLATES*/
	},

	onBeforeAction: function() {
		this.next();
	},

	action: function() {
		if(this.isReady()) { this.render(); } else { this.render("loading"); }
		/*ACTION_FUNCTION*/
	},

	isReady: function() {


		var subs = [
			Meteor.subscribe("cms_customer_list"),
			Meteor.subscribe("cms_sections_empty"),
			Meteor.subscribe("cms_section_list"),
			Meteor.subscribe("cms_campaign_details", this.params.query.cid)
		];
		var ready = true;
		_.each(subs, function(sub) {
			if(!sub.ready())
				ready = false;
		});
		return ready;
	},

	data: function() {


		var data = {
			params: this.params || {},
			cms_customer_list: CmsCustomers.find({}, {}),
			cms_sections_empty: CmsSections.findOne({_id:null}, {}),
			cms_section_list: CmsSections.find({}, {sort:[["sectionName","desc"]]}),
			cms_campaign_details: CmsCampaigns.findOne({campaignName:this.params.query.id}, {})
		};




		return data;
	},

	onAfterAction: function() {

	}
});