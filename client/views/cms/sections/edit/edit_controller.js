this.SectionsEditController = RouteController.extend({
	template: "SectionsEdit",


	yieldTemplates: {
		/*YIELD_TEMPLATES*/
	},

	onBeforeAction: function() {
		this.next();
	},

	action: function() {
		if(this.isReady()) { this.render(); } else { this.render("loading"); }
		/*ACTION_FUNCTION*/
	},

	isReady: function() {


		var subs = [
			Meteor.subscribe("cms_customer_list"),
			Meteor.subscribe("cms_section_details", this.params.sectionId),
			Meteor.subscribe("cms_campaign_list")
		];
		var ready = true;
		_.each(subs, function(sub) {
			if(!sub.ready())
				ready = false;
		});
		return ready;
	},

	data: function() {


		var data = {
			params: this.params || {},
			cms_customer_list: CmsCustomers.find({}, {}),
			cms_section_details: CmsSections.findOne({_id:this.params.sectionId}, {}),
			cms_campaign_list: CmsCampaigns.find({}, {})
		};
		



		return data;
	},

	onAfterAction: function() {

	}
});