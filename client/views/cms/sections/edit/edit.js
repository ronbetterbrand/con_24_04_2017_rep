var pageSession = new ReactiveDict();

Template.SectionsEdit.rendered = function() {

};

Template.SectionsEdit.events({

});

Template.SectionsEdit.helpers({

});

Template.SectionsEditEditForm.rendered = function() {


	pageSession.set("sectionsEditEditFormInfoMessage", "");
	pageSession.set("sectionsEditEditFormErrorMessage", "");

	$(".input-group.date").each(function() {
		var format = $(this).find("input[type='text']").attr("data-format");

		if(format) {
			format = format.toLowerCase();
		}
		else {
			format = "mm/dd/yyyy";
		}

		$(this).datepicker({
			autoclose: true,
			todayHighlight: true,
			todayBtn: true,
			forceParse: false,
			keyboardNavigation: false,
			format: format
		});
	});

	$("input[type='file']").fileinput();
	$("select[data-role='tagsinput']").tagsinput();
	$(".bootstrap-tagsinput").addClass("form-control");
	$("input[autofocus]").focus();
};

Template.SectionsEditEditForm.events({
	"submit": function(e, t) {
		e.preventDefault();
		pageSession.set("sectionsEditEditFormInfoMessage", "");
		pageSession.set("sectionsEditEditFormErrorMessage", "");

		var self = this;

		function submitAction(msg) {
			var sectionsEditEditFormMode = "update";
			if(!t.find("#form-cancel-button")) {
				switch(sectionsEditEditFormMode) {
					case "insert": {
						$(e.target)[0].reset();
					}; break;

					case "update": {
						var message = msg || "Saved.";
						pageSession.set("sectionsEditEditFormInfoMessage", message);
					}; break;
				}
			}

			history.back();
		}

		function errorAction(msg) {
			msg = msg || "";
			var message = msg.message || msg || "Error.";
			pageSession.set("sectionsEditEditFormErrorMessage", message);
		}

		validateForm(
			$(e.target),
			function(fieldName, fieldValue) {

			},
			function(msg) {

			},
			function(values) {


				CmsSections.update({ _id: t.data.cms_section_details._id }, { $set: values }, function(e) { if(e) errorAction(e); else submitAction(); });
			}
		);

		return false;
	},
	"click #form-cancel-button": function(e, t) {
		e.preventDefault();



		history.back();
	},
	"click #form-close-button": function(e, t) {
		e.preventDefault();

		/*CLOSE_REDIRECT*/
	},
	"click #form-back-button": function(e, t) {
		e.preventDefault();

		/*BACK_REDIRECT*/
	},
	"click #form-sections-button": function(e, t) {
		e.preventDefault();

		history.back();
		/*BACK_REDIRECT*/
	}


});

Template.SectionsEditEditForm.helpers({
	"infoMessage": function() {
		return pageSession.get("sectionsEditEditFormInfoMessage");
	},
	"errorMessage": function() {
		return pageSession.get("sectionsEditEditFormErrorMessage");
	},
	"cms_campaign_id": function(campName) {

		return this.params.query.cid;
		
	},

});
