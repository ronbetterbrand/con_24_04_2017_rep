this.SectionsController = RouteController.extend({
	template: "Sections",
	

	yieldTemplates: {
		/*YIELD_TEMPLATES*/
	},

	onBeforeAction: function() {
		this.next();
	},

	action: function() {
		if(this.isReady()) { this.render(); } else { this.render("loading"); }
		/*ACTION_FUNCTION*/
	},

	isReady: function() {
		

		var subs = [
			Meteor.subscribe("cms_section_list")
		];
		var ready = true;
		_.each(subs, function(sub) {
			if(!sub.ready())
				ready = false;
		});
		return ready;
	},

	data: function() {
		

		var data = {
			params: this.params || {},
			cms_section_list: CmsSections.find({campaignName:this.params.query.id}, {sort:[["cardID","cardType","sectionName","desc"]]}),
			cms_campaign_details: CmsCampaigns.findOne({campaignName:this.params.query.id}, {})
		};
		

		

		return data;
	},

	onAfterAction: function() {
		
	}
});