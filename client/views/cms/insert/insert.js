var pageSession = new ReactiveDict();

Template.C_CampaignsInsert.rendered = function() {
	
};

Template.C_CampaignsInsert.events({
	
});

Template.C_CampaignsInsert.helpers({
	
});

Template.C_CampaignsInsertInsertForm.rendered = function() {
	

	pageSession.set("campaignsInsertInsertFormInfoMessage", "");
	pageSession.set("campaignsInsertInsertFormErrorMessage", "");

	$(".input-group.date").each(function() {
		var format = $(this).find("input[type='text']").attr("data-format");

		if(format) {
			format = format.toLowerCase();
		}
		else {
			format = "mm/dd/yyyy";
		}

		$(this).datepicker({
			autoclose: true,
			todayHighlight: true,
			todayBtn: true,
			forceParse: false,
			keyboardNavigation: false,
			format: format
		});
	});

	$("input[type='file']").fileinput();
	$("select[data-role='tagsinput']").tagsinput();
	$(".bootstrap-tagsinput").addClass("form-control");
	$("input[autofocus]").focus();
};

Template.C_CampaignsInsertInsertForm.events({
	"submit": function(e, t) {
		e.preventDefault();
		pageSession.set("campaignsInsertInsertFormInfoMessage", "");
		pageSession.set("campaignsInsertInsertFormErrorMessage", "");

		var self = this;
		var isApply = -1;

		function submitAction(msg) {
			var campaignsInsertInsertFormMode = "insert";
			if(!t.find("#form-cancel-button")) {
				switch(campaignsInsertInsertFormMode) {
					case "insert": {
						$(e.target)[0].reset();
					}; break;

					case "update": {
						var message = msg || "Saved.";
						pageSession.set("campaignsInsertInsertFormInfoMessage", message);
					}; break;
				}
			}

			Router.go("cms", {campaignId: newId});
		}

		function errorAction(msg) {
			msg = msg || "";
			var message = msg.message || msg || "Error.";
			pageSession.set("campaignsInsertInsertFormErrorMessage", message);
		}

		validateForm(
			$(e.target),
			function(fieldName, fieldValue) {

				if( fieldName=="campaignName") {

					var c = CmsCampaigns.findOne({"campaignName": fieldValue});

					if (c == undefined) {
						isApply = 0;
					}
					else {

						if (c.campaignName == fieldValue) {
							bootbox.alert("Please use a unique campaign name",
								function () {
									Router.go("cms", {});
									return;
								});
						}
						else {
							isApply = 0;
						}

					}

				}


			},
			function(msg) {

			},
			function(values) {

				if (isApply==0) {
					newId = CmsCampaigns.insert(values, function (e) {
						if (e) errorAction(e); else submitAction();
					});
				}
			}
		);

		return false;
	},
	"click #form-cancel-button": function(e, t) {
		e.preventDefault();

		

		Router.go("cms", {});
	},
	"click #form-close-button": function(e, t) {
		e.preventDefault();

		/*CLOSE_REDIRECT*/
	},
	"click #form-back-button": function(e, t) {
		e.preventDefault();

		/*BACK_REDIRECT*/
	}

	
});

Template.C_CampaignsInsertInsertForm.helpers({
	"infoMessage": function() {
		return pageSession.get("campaignsInsertInsertFormInfoMessage");
	},
	"errorMessage": function() {
		return pageSession.get("campaignsInsertInsertFormErrorMessage");
	}
	
});
