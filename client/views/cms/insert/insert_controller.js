this.C_CampaignsInsertController = RouteController.extend({
	template: "C_CampaignsInsert",
	

	yieldTemplates: {
		/*YIELD_TEMPLATES*/
	},

	onBeforeAction: function() {
		this.next();
	},

	action: function() {
		if(this.isReady()) { this.render(); } else { this.render("loading"); }
		/*ACTION_FUNCTION*/
	},

	isReady: function() {
		

		var subs = [
			Meteor.subscribe("cms_customer_list"),
			Meteor.subscribe("cms_campaigns_empty"),
			Meteor.subscribe("cms_campaign_list")
		];
		var ready = true;
		_.each(subs, function(sub) {
			if(!sub.ready())
				ready = false;
		});
		return ready;
	},

	data: function() {
		

		var data = {
			params: this.params || {},
			cms_customer_list: CmsCustomers.find({}, {}),
			cms_campaigns_empty: CmsCampaigns.findOne({_id:null}, {}),
			cms_campaign_list: CmsCampaigns.find({}, {sort:[["campaignName","desc"]]})
		};
		

		

		return data;
	},

	onAfterAction: function() {
		
	}
});