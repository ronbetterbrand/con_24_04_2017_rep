var pageSession = new ReactiveDict();


function performSimpleWelcomeRequest(botRelativeURL,pageID, type, msg){

	$.ajax({
		url: "https://" + botRelativeURL + ".betterbrand.im/FBServer/InitWelcomeScreen",
		jsonp: "callback",
		dataType: "jsonp",
		data: {
			type: type,
			pageID: pageID,
			msg: msg
		},
		success: function (response) {
			console.log(response);
		}
	});
}



Template.C_CampaignsEdit.rendered = function() {
	
};

Template.C_CampaignsEdit.events({
	
});

Template.C_CampaignsEdit.helpers({
	
});

Template.C_CampaignsEditEditForm.rendered = function() {

	//$('.collapse').collapse();

	var c = CmsCampaigns.findOne();

	//console.log(c.welcomeMsgTextCheckBox+" = "+ c.welcomeMsgGalleryCheckBox);

	if( c.welcomeMsgTextCheckBox ){
		//console.log("open text");
		$('.collapseText').collapse('show');
	}

	if( c.welcomeMsgGalleryCheckBox ){
		//console.log("close gallery");
		$('.collapseGallery').collapse('show');
	}




	pageSession.set("campaignsEditEditFormInfoMessage", "");
	pageSession.set("campaignsEditEditFormErrorMessage", "");

	$(".input-group.date").each(function() {
		var format = $(this).find("input[type='text']").attr("data-format");

		if(format) {
			format = format.toLowerCase();
		}
		else {
			format = "mm/dd/yyyy";
		}

		$(this).datepicker({
			autoclose: true,
			todayHighlight: true,
			todayBtn: true,
			forceParse: false,
			keyboardNavigation: false,
			format: format
		});
	});

	$("input[type='file']").fileinput();
	$("select[data-role='tagsinput']").tagsinput();
	$(".bootstrap-tagsinput").addClass("form-control");
	$("input[autofocus]").focus();
};

Template.C_CampaignsEditEditForm.events({
	"submit": function (e, t) {
		e.preventDefault();
		pageSession.set("campaignsEditEditFormInfoMessage", "");
		pageSession.set("campaignsEditEditFormErrorMessage", "");

		var self = this;

		function submitAction(msg) {
			var campaignsEditEditFormMode = "update";
			if (!t.find("#form-cancel-button")) {
				switch (campaignsEditEditFormMode) {
					case "insert":
					{
						$(e.target)[0].reset();
					}
						;
						break;

					case "update":
					{
						var message = msg || "Saved.";
						pageSession.set("campaignsEditEditFormInfoMessage", message);
					}
						;
						break;
				}
			}

			Router.go("cms", {});
		}

		function errorAction(msg) {
			msg = msg || "";
			var message = msg.message || msg || "Error.";
			pageSession.set("campaignsEditEditFormErrorMessage", message);
		}

		validateForm(
			$(e.target),
			function (fieldName, fieldValue) {

			},
			function (msg) {

			},
			function (values) {

				var oldCn=t.data.cms_campaign_details.campaignName;
				CmsCampaigns.update({_id: t.data.cms_campaign_details._id}, {$set: values}, function (e) {
					if (e) errorAction(e); else submitAction();
				});

				var secIds = CmsSections.find({campaignName: oldCn}).fetch().map(function (x) {
					return x._id;
				});

				for(var x in secIds)
				{
					console.log(x+"  "+secIds[x])
					CmsSections.update({_id: secIds[x]}, {$set: {campaignName: values.campaignName }});
				}

				//CmsSections.update({canpaignName: t.data.cms_campaign_details.canpaignName}, {$set: {canpaignName: values.canpaignName }}, function (e) {
			//		if (e) errorAction(e); else submitAction();
			//	});


			}
		);

		return false;
	},
	"click #form-cancel-button": function (e, t) {
		e.preventDefault();

		console.log("cancel");

		Router.go("cms", {});
	},
	"click #form-close-button": function (e, t) {
		e.preventDefault();

		/*CLOSE_REDIRECT*/
	},
	"click #form-back-button": function (e, t) {

		e.preventDefault();

		/*BACK_REDIRECT*/
	},
	"click #form-sections-button": function (e, t) {
		e.preventDefault();

		//console.log("section of="+ this.params.campaignId);

		Router.go("cms.sections", {campaignId: this.params.campaignId}, {_id: this.params.campaignId});


		return false;
		//Router.go("sections", {});


		/*BACK_REDIRECT*/
	},
	"click #initGallery-button": function (e, t) {

		e.preventDefault();

		var botRelativeURL = document.getElementById('botRelativeURL').value;
		var botPageID = document.getElementById('botPageID').value;
		var title = document.getElementById('welcomeMsgGalleryTitle').value;
		var itemURL = document.getElementById('welcomeMsgGalleryItemUrl').value;
		var imageURL = document.getElementById('welcomeMsgGalleryImageUrl').value;
		var subtitle = document.getElementById('welcomeMsgGallerySubtitle').value;
		var button1Title = document.getElementById('button1Title').value;
		var button1URL = document.getElementById('button1URL').value;
		var button2Title = document.getElementById('button2Title').value;
		var button2Payload = document.getElementById('button2Payload').value;

		console.log("init galery=" + title+" button1URL="+button1URL);

		$.ajax({
			url: "https://"+botRelativeURL+".betterbrand.im/FBServer/InitWelcomeScreen",
			jsonp: "callback",
			dataType: "jsonp",
			data: {
				type: "gallery",
				pageID: botPageID,
				title: title,
				itemURL: itemURL,
				imageURL: imageURL,
				subtitle: subtitle,
				button1Title: button1Title,
				button1URL: button1URL,
				button2Title: button2Title,
		        button2Payload: button2Payload
			},
			// Work with the response
			success: function (response) {
				console.log(response); // server response
			}
		});

		return false;
	},
	"click #initText-button": function (e, t) {

		console.log("deleteGallery-button");
		e.preventDefault();

			var botRelativeURL = document.getElementById('botRelativeURL').value;
			var botPageID = document.getElementById('botPageID').value;
			var welcomeMsgText = document.getElementById('welcomeMsgText').value;

			performSimpleWelcomeRequest( botRelativeURL,
										 botPageID,
				                         "text",
				                         welcomeMsgText );

		return false;
	},
	"click #deleteGallery": function (e, t) {

		console.log("deleteGallery-button");
		e.preventDefault();

		var botRelativeURL = document.getElementById('botRelativeURL').value;
		var botPageID = document.getElementById('botPageID').value;
		console.log("delete-welcome-msg text=");

		performSimpleWelcomeRequest( botRelativeURL,
									 botPageID,
			                         "delete-welcome-msg",
									 "empty");

		return false;
	}

});


Template.C_CampaignsEditEditForm.helpers({
	"infoMessage": function() {
		return pageSession.get("campaignsEditEditFormInfoMessage");
	},
	"errorMessage": function() {
		return pageSession.get("campaignsEditEditFormErrorMessage");
	},
	isGalleryChecked: function(){
		var c = CmsCampaigns.findOne();
		return c.welcomeMsgGalleryCheckBox;
	},
	isTextChecked: function(){
		var c = CmsCampaigns.findOne();
		return c.welcomeMsgTextCheckBox;
	}
	
});
