this.C_CampaignsEditController = RouteController.extend({
	template: "C_CampaignsEdit",
	

	yieldTemplates: {
		/*YIELD_TEMPLATES*/
	},

	onBeforeAction: function() {
		this.next();
	},

	action: function() {
		if(this.isReady()) { this.render(); } else { this.render("loading"); }
		/*ACTION_FUNCTION*/
	},

	isReady: function() {
		

		var subs = [
			Meteor.subscribe("cms_customer_list"),
			Meteor.subscribe("cms_campaign_details", this.params.campaignId),
			Meteor.subscribe("cms_section_list")
		];
		var ready = true;
		_.each(subs, function(sub) {
			if(!sub.ready())
				ready = false;
		});
		return ready;
	},

	data: function() {
		

		var data = {
			params: this.params || {},
			cms_customer_list: CmsCustomers.find({}, {}),
			cms_campaign_details: CmsCampaigns.findOne({_id:this.params.campaignId}, {}),
			cms_section_list: CmsSections.find({}, {sort:[["cardID","cardType","sectionName","desc"]]})
		};
		

		

		return data;
	},

	onAfterAction: function() {
		
	}
});