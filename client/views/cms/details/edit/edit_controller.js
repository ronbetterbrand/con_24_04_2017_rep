this.C_CampaignsDetailsEditController = RouteController.extend({
	template: "C_CampaignsDetails",
	

	yieldTemplates: {
		'C_CampaignsDetailsEdit': { to: 'C_CampaignsDetailsSubcontent'}
		
	},

	onBeforeAction: function() {
		this.next();
	},

	action: function() {
		if(this.isReady()) { this.render(); } else { this.render("C_CampaignsDetails"); this.render("loading", { to: "C_CampaignsDetailsSubcontent" });}
		/*ACTION_FUNCTION*/
	},

	isReady: function() {
		

		var subs = [
			Meteor.subscribe("cms_section_list"),
			Meteor.subscribe("cms_campaign_item", this.params.itemId),
			Meteor.subscribe("cms_campaign_details", this.params.campaignId)
		];
		var ready = true;
		_.each(subs, function(sub) {
			if(!sub.ready())
				ready = false;
		});
		return ready;
	},

	data: function() {
		

		var data = {
			params: this.params || {},
			cms_campaign_item: CmsCampaignItems.findOne({_id:this.params.itemId}, {}),
			cms_campaign_details: CmsCampaigns.findOne({_id:this.params.campaignId}, {}),

			cms_section_list: CmsSections.find({}, {sort:[["cardID","cardType","sectionName","desc"]]})
		};
		

		

		return data;
	},

	onAfterAction: function() {
		
	}
});