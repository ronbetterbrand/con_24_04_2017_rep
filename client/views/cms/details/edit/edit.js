var pageSession = new ReactiveDict();

UPLOADCARE_PUBLIC_KEY = '5663fa664431c0362e6e';
UPLOADCARE_IMAGES_ONLY = true;
UPLOADCARE_AUTOSTORE = true;

var choosenType = "Default";
var MAX_BUTTONS=10; // Limit the amount of  buttons a form can have

Template.C_CampaignsDetailsEdit.rendered = function () {

};



Template.C_CampaignsDetailsEdit.events({});

Template.C_CampaignsDetailsEdit.helpers({});

Template.C_CampaignsDetailsEditEditForm.onCreated(function(){
  this.nrOfButtons=new ReactiveVar(0);
});

Template.C_CampaignsDetailsEditEditForm.onRendered(function(){
  this.$('#Counter').textCounter({
    target: '#mesField', // required: string
    count: 640, // optional: if string, specifies attribute of target to use as value
    //           if integer, specifies value. [defaults 140]
    alertAt: 20, // optional: integer [defaults 20]
    warnAt: 10, // optional: integer [defaults 0]
    stopAtLimit: false // optional: defaults to false
  });
  this.$('#Counter1').textCounter({
    target: '#subField', // required: string
    count: 80, // optional: if string, specifies attribute of target to use as value
    //           if integer, specifies value. [defaults 140]
    alertAt: 20, // optional: integer [defaults 20]
    warnAt: 10, // optional: integer [defaults 0]
    stopAtLimit: false // optional: defaults to false
  });
  this.$('#Counter2').textCounter({
    target: '#cardTitle', // required: string
    count: 80, // optional: if string, specifies attribute of target to use as value
    //           if integer, specifies value. [defaults 140]
    alertAt: 20, // optional: integer [defaults 20]
    warnAt: 10, // optional: integer [defaults 0]
    stopAtLimit: false // optional: defaults to false
  });
  this.$('#Counter_A').textCounter({
    target: '#mesField_A', // required: string
    count: 640, // optional: if string, specifies attribute of target to use as value
    //           if integer, specifies value. [defaults 140]
    alertAt: 20, // optional: integer [defaults 20]
    warnAt: 10, // optional: integer [defaults 0]
    stopAtLimit: false // optional: defaults to false
  });

  this.$('#Counter_B').textCounter({
    target: '#mesField_B', // required: string
    count: 640, // optional: if string, specifies attribute of target to use as value
    //           if integer, specifies value. [defaults 140]
    alertAt: 20, // optional: integer [defaults 20]
    warnAt: 10, // optional: integer [defaults 0]
    stopAtLimit: false // optional: defaults to false
  });
});




Template.C_CampaignsDetailsEditEditForm.rendered = function () {

  var widget = uploadcare.Widget('[role=uploadcare-uploader]');
  widget.onUploadComplete(function(fileInfo) {
    document.getElementById("imageField").value = fileInfo.originalUrl;
  });


  pageSession.set("C_CampaignsDetailsEditEditFormInfoMessage", "");
  pageSession.set("C_CampaignsDetailsEditEditFormErrorMessage", "");

  $(".input-group.date").each(function () {
    var format = $(this).find("input[type='text']").attr("data-format");

    if (format) {
      format = format.toLowerCase();
    }
    else {
      format = "mm/dd/yyyy";
    }

    $(this).datepicker({
      autoclose: true,
      todayHighlight: true,
      todayBtn: true,
      forceParse: false,
      keyboardNavigation: false,
      format: format
    });
  });

  $("input[type='file']").fileinput();
  $("select[data-role='tagsinput']").tagsinput();
  $(".bootstrap-tagsinput").addClass("form-control");
  $("input[autofocus]").focus();

  //Check if is it the "push-notification" type

  var e = document.getElementById("sectionID");
  var strUser = e.options[e.selectedIndex].value;

  console.log("change " + strUser);

  var d = CmsSections.findOne({sectionName: strUser});

  console.log("cardType=" + d.cardType);

  var element = document.getElementById("typeID");
  element.innerHTML = d.cardType;

  choosenType = d.cardType;

  document.getElementById("cardTitle").style.display = "block";
  document.getElementById("imageDIV").style.display = "block";
  //document.getElementById("imageField").value="";
  document.getElementById("subtitleDIV").style.display = "none";
  //document.getElementById("subField").value="empty";
  document.getElementById("messageID").style.display = "block";
  //document.getElementById("mesField").value="";
  document.getElementById("positionDiv").style.display = "block";
  document.getElementById("buttonsID").style.display = "block";
  document.getElementById("buttonsMenu").style.display = "none";

  if (d.cardType == "Push Notification Message" || d.cardType == "Text") {
    document.getElementById("imageDIV").style.display = "none";
    //document.getElementById("imageField").value="empty";
    document.getElementById("subtitleDIV").style.display = "none";
    //document.getElementById("subField").value="empty";
    document.getElementById("buttonsID").style.display = "none";
    document.getElementById("messageID").style.display = "block";
    //document.getElementById("mesField").value="";
    document.getElementById("positionDiv").style.display = "block";
    document.getElementById("cardTitle").style.display = "block";
    document.getElementById("buttonsMenu").style.display = "none";
  }
	else
	if(d.cardType == "FB Image + Buttons"){
    document.getElementById("cardTitle").style.display = "block";
    document.getElementById("imageDIV").style.display = "block";
    //document.getElementById("imageField").value="";
    document.getElementById("subtitleDIV").style.display = "none";
    //document.getElementById("subField").value="empty";
    document.getElementById("messageID").style.display = "block";
    //document.getElementById("mesField").value="";
    document.getElementById("positionDiv").style.display = "block";
    document.getElementById("buttonsID").style.display = "block";
    document.getElementById("buttonsMenu").style.display = "none";
  }
	else
	if (d.cardType == "Gallery") {
    document.getElementById("cardTitle").style.display = "block";
    document.getElementById("subtitleDIV").style.display = "block";
    //document.getElementById("subField").value="";
    document.getElementById("messageID").style.display = "none";
    //document.getElementById("mesField").value="empty";
    document.getElementById("positionDiv").style.display = "block";
    document.getElementById("buttonsID").style.display = "block";
    document.getElementById("imageDIV").style.display = "block";
    //document.getElementById("imageField").value="";
    document.getElementById("buttonsMenu").style.display = "none";
  }
	else
	if (d.cardType == "Menu") {
    document.getElementById("cardTitle").style.display = "block";
    document.getElementById("subtitleDIV").style.display = "none";
    //document.getElementById("subField").value="empty";
    document.getElementById("messageID").style.display = "none";
    //document.getElementById("mesField").value="empty";
    document.getElementById("positionDiv").style.display = "none";
    document.getElementById("imageDIV").style.display = "none";
    //document.getElementById("imageField").value="empty";
    document.getElementById("buttonsMenu").style.display = "block";
    document.getElementById("buttonsID").style.display = "block";
  }

  for (var chosenIndex = 0; chosenIndex < this.nrOfButtons; chosenIndex++) {
    var theitem = $(".typeSelect")[chosenIndex].value;
    if (theitem == "postback") {
      $(".destInput")[chosenIndex].placeholder = "please type the item title destination";
      $(".textInput")[chosenIndex].placeholder = "please type the button text";
    }
    if (theitem == "web_url") {
      $(".destInput")[chosenIndex].placeholder = "please type external link destination";
      $(".textInput")[chosenIndex].placeholder = "please type the button text";
    }
  }

  //var d = CmsCampaignItems.findOne({sectionName:strUser});
  //
  //console.log("P="+ currentData);
  //var element = document.getElementById('position');
  //element.value = this.position;

};

Template.C_CampaignsDetailsEditEditForm.events({
  "submit": function (e, t) {

    console.log("Preform save...");

    e.preventDefault();
    pageSession.set("C_CampaignsDetailsEditEditFormInfoMessage", "");
    pageSession.set("C_CampaignsDetailsEditEditFormErrorMessage", "");

    var self = this;

    function submitAction(msg) {

      console.log("Preform submitAction..." + msg);

      var C_CampaignsDetailsEditEditFormMode = "update";
      if (!t.find("#form-cancel-button")) {
        switch (C_CampaignsDetailsEditEditFormMode) {
					case "insert": {
            $(e.target)[0].reset();
					}; break;

					case "update": {
            var message = msg || "Saved.";
            pageSession.set("C_CampaignsDetailsEditEditFormInfoMessage", message);
					}; break;
        }
      }

      var url = window.location.href;
      var cid = url.split('/')[5] + "/items";
      var query = "sname=" + t.data.params.query.sname;
      e.preventDefault();
      Router.go("cms.details", {campaignId: cid}, {query: query});
      //Router.go("cms_campaigns.details", {campaignId: self.params.campaignId});
    }

    function errorAction(msg) {
      msg = msg || "";
      var message = msg.message || msg || "Error.";
      pageSession.set("C_CampaignsDetailsEditEditFormErrorMessage", message);
    }

    validateForm(
      $(e.target),
      function (fieldName, fieldValue) {

      },
      function (msg) {

      },
      function (values) {


        try {
// TODO: should we do this for every button?? Rafa
          if (values.button1Data.indexOf("Rm_Tell_me_more") > -1) {
            values.button1Data = "Rm_Tell_me_more" + Math.floor(Math.random() * 1000);
            //console.log("button1Data="+values.button1Data);
          }
          if (values.button2Data.indexOf("Rm_Tell_me_more") > -1) {
            values.button2Data = "Rm_Tell_me_more" + Math.floor(Math.random() * 1000);
            //console.log("button1Data="+values.button1Data);
          }
          if (values.button3Data.indexOf("Rm_Tell_me_more") > -1) {
            values.button3Data = "Rm_Tell_me_more" + Math.floor(Math.random() * 1000);
            //console.log("button1Data="+values.button1Data);
          }
        }
        catch (e) {
          console.log(e);
        }

        values.type = choosenType;

        CmsCampaignItems.update(
          {_id: t.data.cms_campaign_item._id},
          {$set: values},
          function (e) {
						if(e){ errorAction(e);}
						else{ submitAction(); }
          }
        );

      }
    );

    return false;
  },
  "click #form-cancel-button": function (e, t) {
    e.preventDefault();
    var url = window.location.href;
    var cid = url.split('/')[5] + "/items";
    var query = "sname=" + t.data.params.query.sname;
    //	e.preventDefault();
    Router.go("cms.details", {campaignId: cid}, {query: query});


    //Router.go("cms.details", {campaignId: this.params.campaignId});
  },
  "click #form-close-button": function (e, t) {
    e.preventDefault();

    /*CLOSE_REDIRECT*/
  },
  "click #form-back-button": function (e, t) {
    e.preventDefault();

    console.log("back");
    /*BACK_REDIRECT*/
  },
  "click #form-duplicate-button": function (e, t) {
    event.preventDefault();
    var itemName = document.getElementById('name').value;

    console.log("dup this item=" + itemName);

    //var wrapperKey = {};
    //var key = tkey;
    //wrapperKey['keyword.' +key] = tvalue;

    /*var wrapperCamp = {};
     var key = "name";
     wrapperCamp[key] = campaignName;

     var wrapperObj = {};
     var key = "campaign";
     wrapperObj[key] = t.data.campaign.campaign;*/

    //var tkey = document.getElementById('keyword_key').value;
    //var tvalue = document.getElementById('keyword_value').value;

    //var wrapperKey = {};
    //var key = 'campaign';
    //wrapperKey['campaign.' +key] = t.data.campaign.campaign;

    //var ncid = new Meteor.Collection.ObjectID();



    //var cc = Campaigns.find( { _id: t.data.campaign._id  } );
    //console.log("cc = "+cc.campaign+" obj="+cc.campaign.obj);
    //console.log("cc2 = "+t.data.campaign.campaign+" obj="+t.data.campaign.obj);

    /*Campaigns.insert( {$set: wrapperObj }, {$set: wrapperCamp }, function (e) {
     console.log("Success duplicate ");
     });*/



    //console.log(t.data.campaign._id+" ****************************** ncid="+ncid);
    /*Campaigns.insert({ _id: ncid }, { $set: wrapperCamp },{ upsert: true }, function(e) {console.log("Success duplicate "+campaignName);  });*/

    //Campaigns.insert({  _id: ncid}, { $set: wrapperKey },function(e) {  console.log("Success duplicate "); });
    //Campaigns.insert( {  _id: ncid}, { $set: wrapperCamp },function(e) {  console.log("Success save wrapperCamp "); });


    validateForm(
      $(e.target),
      function (fieldName, fieldValue) {

      },
      function (msg) {

      },
      function (values) {
        values = t.data.cms_campaign_item;
        values.name = "Copy of " + itemName;
        var ncid = new Meteor.Collection.ObjectID().valueOf();
        values._id = ncid;
        CmsCampaignItems.insert(values, function (e) {
          console.log("Success save dup item ");
          //Router.go("cms_campaigns");
          Router.go("cms.details", {campaignId: t.data.cms_campaign_item._id});
        });


      }
    );

  },
  "change #sectionID": function (e, t) {
    e.preventDefault();

    var e = document.getElementById("sectionID");
    var strUser = e.options[e.selectedIndex].value;

    console.log("change " + strUser);

    var d = CmsSections.findOne({sectionName: strUser});

    console.log("cardType=" + d.cardType + " id=" + t.data.cms_campaign_item._id);
    choosenType = d.cardType;


    var element = document.getElementById("typeID");
    element.innerHTML = d.cardType;

    if (d.cardType == "Push Notification Message" || d.cardType == "Text") {
      document.getElementById("imageDIV").style.display = "none";
      document.getElementById("subtitleDIV").style.display = "none";
      document.getElementById("buttonsID").style.display = "none";
      document.getElementById("messageID").style.display = "block";
      document.getElementById("positionDiv").style.display = "block";
      document.getElementById("cardTitle").style.display = "block";
      document.getElementById("buttonsMenu").style.display = "none";
    }
		else
		if(d.cardType == "FB Image + Buttons"){
      document.getElementById("cardTitle").style.display = "block";
      document.getElementById("imageDIV").style.display = "block";
      document.getElementById("subtitleDIV").style.display = "none";
      document.getElementById("messageID").style.display = "block";
      document.getElementById("positionDiv").style.display = "block";
      document.getElementById("buttonsID").style.display = "block";
      document.getElementById("buttonsMenu").style.display = "none";
    }
		else
		if (d.cardType == "Gallery") {
      document.getElementById("cardTitle").style.display = "block";
      document.getElementById("subtitleDIV").style.display = "block";
      document.getElementById("messageID").style.display = "none";
      document.getElementById("positionDiv").style.display = "block";
      document.getElementById("buttonsID").style.display = "block";
      document.getElementById("imageDIV").style.display = "block";
      document.getElementById("buttonsMenu").style.display = "none";
    }
		else
		if (d.cardType == "Menu") {
      document.getElementById("cardTitle").style.display = "block";
      document.getElementById("subtitleDIV").style.display = "none";
      document.getElementById("messageID").style.display = "none";
      document.getElementById("positionDiv").style.display = "none";
      document.getElementById("imageDIV").style.display = "none";
      document.getElementById("buttonsMenu").style.display = "block";
      document.getElementById("buttonsID").style.display = "block";
    }
    values = t.data.cms_campaign_item;
    values.type = d.cardType;

    /*CmsCampaignItems.update(
     { _id: t.data.cms_campaign_item._id },
     { $set: values }, function(e)
     {
     ///???var message = msg || "Saved.";
     ///???pageSession.set("campaignsDetailsEditEditFormInfoMessage", message);
     ///???if(e) errorAction(e); else submitAction();
     }
     );*/

    CmsCampaignItems.update({_id: t.data.cms_campaign_item._id}, {$set: values});





  },
  "change .typeSelect": function (e, t) {
    e.preventDefault();
    var chosenIndex = parseInt(e.currentTarget.name[6]);
    if (e.currentTarget.value == "postback") {
      $(".destInput")[chosenIndex - 1].placeholder = "please type the item title destination";
      $(".textInput")[chosenIndex - 1].placeholder = "please type the button text";
    }
    if (e.currentTarget.value == "web_url") {
      $(".destInput")[chosenIndex - 1].placeholder = "please type external link destination";
      $(".textInput")[chosenIndex - 1].placeholder = "please type the button text";
    }



  },

  'click #addButton' : function(e, t){
    var currAmnt=Template.instance().nrOfButtons.get();
    if (currAmnt < MAX_BUTTONS){
      Template.instance().nrOfButtons.set(currAmnt+1);
    } else {
      $('#addButton').addClass("disabled");
      $('#addButton').prop('disabled', true);
    }
  }
});

Template.C_CampaignsDetailsEditEditForm.helpers({
  "infoMessage": function () {
    return pageSession.get("C_CampaignsDetailsEditEditFormInfoMessage");
  },
  "errorMessage": function () {
    return pageSession.get("C_CampaignsDetailsEditEditFormErrorMessage");
  },
  'sections': function () {
    //returns reference to Document for all sections

    return CmsSections.find().fetch();
  },
  'isTellMeMore': function (passedString) {

    //console.log("............Rm_Tell_me_more"+" "+passedString);

    if (passedString.indexOf("Rm_Tell_me_more") > -1) {

      //console.log("............Rm_Tell_me_more");

      return "Rm_Tell_me_more";
    }

    return passedString;
  },
  'CardOrItem': function () {

    var strUser = this.params.query.sname;

    console.log("SECTYPE " + strUser);

    var d = CmsSections.findOne({sectionName: strUser});

    console.log("cardType=" + d.cardType);
    //choosenType = d.cardType;


    if (d.cardType == "Gallery") {
      return "Card";
    }
    else
      return "Item";
  },
  'loopNrOfButtons': function(cms_campaign_item){
    var getNrOfButtons= function(cms_campaign_item){
      for (var i=MAX_BUTTONS; i> 0; i--){
        var btnName='button'+i+'Data';
        if ((!!cms_campaign_item[btnName]) && (cms_campaign_item[btnName] != "")){
          return i;
        }
      }
      return 1;
    };
    var nrOfButtons= Template.instance().nrOfButtons;
    if (nrOfButtons.get() == 0){
      var crnt= getNrOfButtons(cms_campaign_item);
      nrOfButtons.set(Math.max(crnt,3));
    }

    var count= nrOfButtons.get();
    var countArr = [];
    for (var i=0; i<count; i++){
      countArr.push(i+1);
    }
    return countArr;
  },
  "isFBimgAndButton": function() {
    var strUser = this.params.query.sname;

    var d = CmsSections.findOne({sectionName: strUser});
    console.log("type= " + d.cardType);
    if (d.cardType == "FB Image + Buttons") {
      console.log("its True you are fbimgandbutton...")
      return true;
    }else{
      console.log("its False you are fbimgandbutton...")
      return false;
    }
  }
});

Template.C_CampaignsDetailsEditEditForm_button.helpers({
  'isSelected': function (theitem, thisoption, chosenIndex) {
    if (theitem == thisoption) {
      /*	if(theitem=="postback"){
       $(".destInput")[chosenIndex-1].placeholder="please type the item title destination"
       $(".textInput")[chosenIndex-1].placeholder="please type the button text"
       }
       if(theitem=="web_url"){
       $(".destInput")[chosenIndex-1].placeholder="please type the item title destination"
       $(".textInput")[chosenIndex-1].placeholder="please type the button text"
       }*/
      return "selected";
    }
  },
  'getButton' : function(nr, str){ // str='label'/'Data'/'type'
    var btn='button'+nr+str;
    return this.cms_campaign_item[btn];
  }

});
