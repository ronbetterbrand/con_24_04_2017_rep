this.C_CampaignsDetailsController = RouteController.extend({
	template: "C_CampaignsDetails",
	

	yieldTemplates: {
		/*YIELD_TEMPLATES*/
	},

	onBeforeAction: function() {
		this.next();
	},

	action: function() {
		this.redirect('cms.details.items', this.params || {}, { replaceState: true });
		/*ACTION_FUNCTION*/
	},

	isReady: function() {
		

		var subs = [
			Meteor.subscribe("cms_campaign_details", this.params.campaignId)
		];
		var ready = true;
		_.each(subs, function(sub) {
			if(!sub.ready())
				ready = false;
		});
		return ready;
	},

	data: function() {
		

		var data = {
			params: this.params || {},
			cms_campaign_details: CmsCampaigns.findOne({_id:this.params.campaignId}, {})
		};
		

		

		return data;
	},

	onAfterAction: function() {
		
	}
});