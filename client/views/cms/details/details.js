var pageSession = new ReactiveDict();

Template.C_CampaignsDetails.rendered = function() {
	
};

Template.C_CampaignsDetails.events({
	
});

Template.C_CampaignsDetails.helpers({
	
});

Template.C_CampaignsDetailsDetailsForm.rendered = function() {
	

	pageSession.set("campaignsDetailsDetailsFormInfoMessage", "");
	pageSession.set("campaignsDetailsDetailsFormErrorMessage", "");

	$(".input-group.date").each(function() {
		var format = $(this).find("input[type='text']").attr("data-format");

		if(format) {
			format = format.toLowerCase();
		}
		else {
			format = "mm/dd/yyyy";
		}

		$(this).datepicker({
			autoclose: true,
			todayHighlight: true,
			todayBtn: true,
			forceParse: false,
			keyboardNavigation: false,
			format: format
		});
	});

	$("input[type='file']").fileinput();
	$("select[data-role='tagsinput']").tagsinput();
	$(".bootstrap-tagsinput").addClass("form-control");
	$("input[autofocus]").focus();
};

Template.C_CampaignsDetailsDetailsForm.events({
	"submit": function(e, t) {
		e.preventDefault();
		pageSession.set("campaignsDetailsDetailsFormInfoMessage", "");
		pageSession.set("campaignsDetailsDetailsFormErrorMessage", "");

		var self = this;

		function submitAction(msg) {
			var campaignsDetailsDetailsFormMode = "read_only";
			if(!t.find("#form-cancel-button")) {
				switch(campaignsDetailsDetailsFormMode) {
					case "insert": {
						$(e.target)[0].reset();
					}; break;

					case "update": {
						var message = msg || "Saved.";
						pageSession.set("campaignsDetailsDetailsFormInfoMessage", message);
					}; break;
				}
			}

			/*SUBMIT_REDIRECT*/
		}

		function errorAction(msg) {
			msg = msg || "";
			var message = msg.message || msg || "Error.";
			pageSession.set("campaignsDetailsDetailsFormErrorMessage", message);
		}

		validateForm(
			$(e.target),
			function(fieldName, fieldValue) {

			},
			function(msg) {

			},
			function(values) {
				

				
			}
		);

		return false;
	},
	"click #form-cancel-button": function(e, t) {
		e.preventDefault();

		

		/*CANCEL_REDIRECT*/
	},
	"click #form-close-button": function(e, t) {
		e.preventDefault();

		/*CLOSE_REDIRECT*/
	},
	"click #form-back-button": function(e, t) {
		e.preventDefault();
		Router.go("cms");

		//Router.go("cms_campaigns", {});
	}

	
});

Template.C_CampaignsDetailsDetailsForm.helpers({
	"infoMessage": function() {
		return pageSession.get("campaignsDetailsDetailsFormInfoMessage");
	},
	"errorMessage": function() {
		return pageSession.get("campaignsDetailsDetailsFormErrorMessage");
	}
	
});
