var pageSession = new ReactiveDict();

UPLOADCARE_PUBLIC_KEY = '5663fa664431c0362e6e';
UPLOADCARE_IMAGES_ONLY = true;
UPLOADCARE_AUTOSTORE = true;


var choosenType = "Default";
var MAX_BUTTONS=10; // Limit the amount of  buttons a form can have

Template.C_CampaignsDetailsInsert.rendered = function() {

};

Template.C_CampaignsDetailsInsert.events({

});

Template.C_CampaignsDetailsInsert.helpers({

});

Template.C_CampaignsDetailsInsertInsertForm.onCreated(function(){
	this.nrOfButtons=new ReactiveVar(3);
});

Template.C_CampaignsDetailsInsertInsertForm.onRendered(function(){
	this.$('#Counter').textCounter({
		target: '#mesField', // required: string
		count: 640, // optional: if string, specifies attribute of target to use as value
				   //           if integer, specifies value. [defaults 140]
		alertAt: 20, // optional: integer [defaults 20]
		warnAt: 10, // optional: integer [defaults 0]
		stopAtLimit: false // optional: defaults to false
	});
	this.$('#Counter1').textCounter({
		target: '#subField', // required: string
		count: 80, // optional: if string, specifies attribute of target to use as value
		//           if integer, specifies value. [defaults 140]
		alertAt: 20, // optional: integer [defaults 20]
		warnAt: 10, // optional: integer [defaults 0]
		stopAtLimit: false // optional: defaults to false
	});
	this.$('#Counter2').textCounter({
		target: '#cardTitle', // required: string
		count: 80, // optional: if string, specifies attribute of target to use as value
		//           if integer, specifies value. [defaults 140]
		alertAt: 20, // optional: integer [defaults 20]
		warnAt: 10, // optional: integer [defaults 0]
		stopAtLimit: false // optional: defaults to false
	});

	this.$('#Counter_A').textCounter({
		target: '#mesField_A', // required: string
		count: 640, // optional: if string, specifies attribute of target to use as value
		//           if integer, specifies value. [defaults 140]
		alertAt: 20, // optional: integer [defaults 20]
		warnAt: 10, // optional: integer [defaults 0]
		stopAtLimit: false // optional: defaults to false
	});

	this.$('#Counter_B').textCounter({
		target: '#mesField_B', // required: string
		count: 640, // optional: if string, specifies attribute of target to use as value
		//           if integer, specifies value. [defaults 140]
		alertAt: 20, // optional: integer [defaults 20]
		warnAt: 10, // optional: integer [defaults 0]
		stopAtLimit: false // optional: defaults to false
	});
});



Template.C_CampaignsDetailsInsertInsertForm.rendered = function() {

	var widget = uploadcare.Widget('[role=uploadcare-uploader]');
	widget.onUploadComplete(function(fileInfo) {
		document.getElementById("imageField").value = fileInfo.originalUrl;
	});

	var e = document.getElementById("sectionID");
	var strUser = this.data.params.query.sname;

	console.log("change " + strUser);

	var d = CmsSections.findOne({sectionName: strUser});

	console.log("cardType=" + d.cardType);
	choosenType = d.cardType;

	if(choosenType == "Menu"){
		this.nrOfButtons.set(5);
	}

	var element = document.getElementById("typeID");
	element.innerHTML = d.cardType;

	document.getElementById("cardTitle").style.display = "block";
	document.getElementById("imageDIV").style.display = "block";
	document.getElementById("imageField").value="";
	document.getElementById("subtitleDIV").style.display = "none";
	document.getElementById("subField").value="empty";
	document.getElementById("messageID").style.display= "block";
	document.getElementById("mesField").value="";
	document.getElementById("positionDiv").style.display = "block";
	document.getElementById("buttonsID").style.display = "block";
	document.getElementById("buttonsMenu").style.display = "none";


	if(d.cardType == "Push Notification Message"  || d.cardType == "Text"){
		document.getElementById("imageDIV").style.display = "none";
		document.getElementById("imageField").value="empty";
		document.getElementById("subtitleDIV").style.display = "none";
		document.getElementById("subField").value="empty";
		document.getElementById("buttonsID").style.display = "none";
		document.getElementById("messageID").style.display= "block";
		document.getElementById("mesField").value="";
		document.getElementById("positionDiv").style.display = "block";
		document.getElementById("cardTitle").style.display = "block";
		document.getElementById("buttonsMenu").style.display = "none";

	}
	else
	if(d.cardType == "FB Image + Buttons") {
		document.getElementById("cardTitle").style.display = "block";
		document.getElementById("imageDIV").style.display = "block";
		document.getElementById("imageField").value = "";
		document.getElementById("subtitleDIV").style.display = "none";
		document.getElementById("subField").value = "empty";
		document.getElementById("messageID").style.display = "block";
		document.getElementById("mesField").value = "";
		document.getElementById("positionDiv").style.display = "block";
		document.getElementById("buttonsID").style.display = "block";
		document.getElementById("buttonsMenu").style.display = "none";
	}
	else
	if (d.cardType == "Gallery") {
		document.getElementById("cardTitle").style.display = "block";
		document.getElementById("subtitleDIV").style.display = "block";
		document.getElementById("subField").value="";
		document.getElementById("messageID").style.display = "none";
		document.getElementById("mesField").value="empty";
		document.getElementById("positionDiv").style.display = "block";
		document.getElementById("buttonsID").style.display = "block";
		document.getElementById("imageDIV").style.display = "block";
		document.getElementById("imageField").value="";
		document.getElementById("buttonsMenu").style.display = "none";

	}
	else
	if (d.cardType == "Menu") {
		document.getElementById("cardTitle").style.display = "block";
		document.getElementById("subtitleDIV").style.display = "none";
		document.getElementById("subField").value="empty";
		document.getElementById("messageID").style.display = "none";
		document.getElementById("mesField").value="empty";
		document.getElementById("positionDiv").style.display = "none";
		document.getElementById("imageDIV").style.display = "none";
		document.getElementById("imageField").value="empty";
		document.getElementById("buttonsMenu").style.display = "block";
		document.getElementById("buttonsID").style.display = "block";
	}


	pageSession.set("C_CampaignsDetailsInsertInsertFormInfoMessage", "");
	pageSession.set("C_CampaignsDetailsInsertInsertFormErrorMessage", "");

	$(".input-group.date").each(function() {
		var format = $(this).find("input[type='text']").attr("data-format");

		if(format) {
			format = format.toLowerCase();
		}
		else {
			format = "mm/dd/yyyy";
		}

		$(this).datepicker({
			autoclose: true,
			todayHighlight: true,
			todayBtn: true,
			forceParse: false,
			keyboardNavigation: false,
			format: format
		});
	});

	$("input[type='file']").fileinput();
	$("select[data-role='tagsinput']").tagsinput();
	$(".bootstrap-tagsinput").addClass("form-control");
	$("input[autofocus]").focus();


};

Template.C_CampaignsDetailsInsertInsertForm.events({
	"submit": function(e, t) {
		e.preventDefault();
		pageSession.set("C_CampaignsDetailsInsertInsertFormInfoMessage", "");
		pageSession.set("C_CampaignsDetailsInsertInsertFormErrorMessage", "");

		var self = this;

		function submitAction(msg) {
			var C_CampaignsDetailsInsertInsertFormMode = "insert";
			if(!t.find("#form-cancel-button")) {
				switch(C_CampaignsDetailsInsertInsertFormMode) {
					case "insert": {
						$(e.target)[0].reset();
					}; break;

					case "update": {
						var message = msg || "Saved.";
						pageSession.set("C_CampaignsDetailsInsertInsertFormInfoMessage", message);
					}; break;
				}
			}
			var url = window.location.href;
			var cid = url.split('/')[5] +"/items";
			var query = "sname=" + t.data.params.query.sname;
			e.preventDefault();
			Router.go("cms.details", {campaignId: cid}, {query: query});
			//Router.go("cms_campaigns.details", {campaignId: self.params.campaignId});
		}

		function errorAction(msg) {
			msg = msg || "";
			var message = msg.message || msg || "Error.";
			pageSession.set("C_CampaignsDetailsInsertInsertFormErrorMessage", message);
		}

		validateForm(
			$(e.target),
			function(fieldName, fieldValue) {

			},
			function(msg) {

			},
			function(values) {
				values.campaignId = self.params.campaignId;

// TODO: should we do this for every button?
				try
				{
					if (values.button1Data.indexOf("Rm_Tell_me_more") > -1) {
						values.button1Data = "Rm_Tell_me_more" + Math.floor(Math.random() * 1000);
						//console.log("button1Data="+values.button1Data);
					}
					if (values.button2Data.indexOf("Rm_Tell_me_more") > -1) {
						values.button2Data = "Rm_Tell_me_more" + Math.floor(Math.random() * 1000);
						//console.log("button1Data="+values.button1Data);
					}
					if (values.button3Data.indexOf("Rm_Tell_me_more") > -1) {
						values.button3Data = "Rm_Tell_me_more" + Math.floor(Math.random() * 1000);
						//console.log("button1Data="+values.button1Data);
					}
				}
				catch(e){}


				values.type = choosenType;

				newId = CmsCampaignItems.insert(values, function(e) { if(e) errorAction(e); else submitAction(); });
			}
		);

		return false;
	},
	"click #form-cancel-button": function(e, t) {

		//var a="http://localhost:3000/cms/details/FwzDYEAsB4hFS7PJb/items?sname=newsec"
		//var query="sname="+this.params.query.sname;
		e.preventDefault();

		var url = window.location.href;
		var cid = url.split('/')[5] +"/items";
		var query = "sname=" + t.data.params.query.sname;
		e.preventDefault();
		Router.go("cms.details", {campaignId: cid}, {query: query});


		//Router.go("cms.details", {campaignId: this.params.campaignId},{query:this.params.query});
	},
	"click #form-close-button": function(e, t) {
		e.preventDefault();

		/*CLOSE_REDIRECT*/
	},
	"click #form-back-button": function(e, t) {
		e.preventDefault();

		/*BACK_REDIRECT*/
	},
  // TODO: Should we do this for every button
	"click #b1Info": function (e,t){
		e.preventDefault();
		$( "#b1Data" ).toggle();
	},
	"click #b2Info": function (e,t){
		e.preventDefault();
		$( "#b2Data" ).toggle();
	},
	"click #b3Info": function (e,t){
		e.preventDefault();
		$( "#b3Data" ).toggle();
	},
	"click #b4Info": function (e,t){
		e.preventDefault();
		$( "#b4Data" ).toggle();
	},
	"click #b5Info": function (e,t){
		e.preventDefault();
		$( "#b5Data" ).toggle();
	},
	"change #sectionID": function (e,t) {
		e.preventDefault();

		var e = document.getElementById("sectionID");
		var strUser = e.options[e.selectedIndex].value;

		console.log("change " + strUser);

		var d = CmsSections.findOne({sectionName: strUser});

		console.log("cardType=" + d.cardType);
		choosenType = d.cardType;

		var element = document.getElementById("typeID");
		element.innerHTML = d.cardType;


		document.getElementById("buttonsMenu").style.display = "none";

		if(d.cardType == "Push Notification Message"  || d.cardType == "Text"){
			document.getElementById("imageDIV").style.display = "none";
			document.getElementById("subtitleDIV").style.display = "none";
			document.getElementById("buttonsID").style.display = "none";
			document.getElementById("messageID").style.display= "block";
			document.getElementById("positionDiv").style.display = "block";
			document.getElementById("cardTitle").style.display = "block";
			document.getElementById("buttonsMenu").style.display = "none";
		}
		else
		if(d.cardType == "FB Image + Buttons"){
			document.getElementById("cardTitle").style.display = "block";
			document.getElementById("imageDIV").style.display = "block";
			document.getElementById("subtitleDIV").style.display = "none";
			document.getElementById("messageID").style.display= "block";
			document.getElementById("positionDiv").style.display = "block";
			document.getElementById("buttonsID").style.display = "block";
			document.getElementById("buttonsMenu").style.display = "none";
		}
		else
		if (d.cardType == "Gallery") {
			document.getElementById("cardTitle").style.display = "block";
			document.getElementById("subtitleDIV").style.display = "block";
			document.getElementById("messageID").style.display = "none";
			document.getElementById("positionDiv").style.display = "block";
			document.getElementById("buttonsID").style.display = "block";
			document.getElementById("imageDIV").style.display = "block";
			document.getElementById("buttonsMenu").style.display = "none";
		}
		else
		if (d.cardType == "Menu") {
			document.getElementById("cardTitle").style.display = "block";
			document.getElementById("subtitleDIV").style.display = "none";
			document.getElementById("messageID").style.display = "none";
			document.getElementById("positionDiv").style.display = "none";
			document.getElementById("imageDIV").style.display = "none";
			document.getElementById("buttonsMenu").style.display = "block";
			document.getElementById("buttonsID").style.display = "block";
		}

	},
	"change .typeSelect": function (e, t) {
		e.preventDefault();
		var chosenIndex=parseInt(e.currentTarget.name[6]);
		if(e.currentTarget.value=="postback"){
			$(".destInput")[chosenIndex-1].placeholder="please type the item title destination";
			$(".textInput")[chosenIndex-1].placeholder="please type the button text";
		}
		if(e.currentTarget.value=="web_url"){
			$(".destInput")[chosenIndex-1].placeholder="please type external link destination";
			$(".textInput")[chosenIndex-1].placeholder="please type the button text";
		}
	},
	'click #addButton' : function(e, t){
		var currAmnt=Template.instance().nrOfButtons.get();
		if (currAmnt < MAX_BUTTONS){
			Template.instance().nrOfButtons.set(currAmnt+1);
		} else {
			$('#addButton').addClass("disabled");
			$('#addButton').prop('disabled', true);
		}
	}
});

Template.C_CampaignsDetailsInsertInsertForm.helpers({
	"infoMessage": function() {
		return pageSession.get("C_CampaignsDetailsInsertInsertFormInfoMessage");
	},
	"errorMessage": function() {
		return pageSession.get("C_CampaignsDetailsInsertInsertFormErrorMessage");
	},
	"isFBimgAndButton": function() {
		var strUser = this.params.query.sname;



		var d = CmsSections.findOne({sectionName: strUser});
		console.log("type= " + d.cardType);
		if (d.cardType == "FB Image + Buttons") {
			console.log("its True you are fbimgandbutton...")
			return true;
		}else{
			console.log("its False you are fbimgandbutton...")
			return false;
		}
	},

	'sections' : function () {
		//returns reference to Document for all sections

		return CmsSections.find().fetch();
	},
	'isTellMeMore': function (passedString){
		if( passedString.indexOf("Rm_Tell_me_more") > -1){
			return "Rm_Tell_me_more";
		}

		return passedString;
	},
	'sectionName': function (){

		return this.params.query.sname;
	},
	'CardOrItem': function (){

		var strUser = this.params.query.sname;

		console.log("SECTYPE " + strUser);

		var d = CmsSections.findOne({sectionName: strUser});

		console.log("cardType=" + d.cardType);
		//choosenType = d.cardType;


		if (d.cardType == "Gallery") {
			return "Card";
		}
		else
			return "Item";
	},

	'loopNrOfButtons': function(cms_campaign_item){
		var getNrOfButtons= function(cms_campaign_item){
			for (var i=MAX_BUTTONS; i> 0; i--){
				var btnName='button'+i+'Data';
				if ((!!cms_campaign_item[btnName]) && (cms_campaign_item[btnName] != "")){
					return i;
				}
			}
			return 1;
		};
		var nrOfButtons= Template.instance().nrOfButtons;
		if (nrOfButtons.get() == 0){
			nrOfButtons.set(getNrOfButtons(cms_campaign_item));
		}

		var count= nrOfButtons.get();
		var countArr = [];
		for (var i=0; i<count; i++){
			countArr.push(i+1);
		}
		return countArr;
	}
});

