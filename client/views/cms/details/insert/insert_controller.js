this.C_CampaignsDetailsInsertController = RouteController.extend({
	template: "C_CampaignsDetails",
	

	yieldTemplates: {
		'C_CampaignsDetailsInsert': { to: 'C_CampaignsDetailsSubcontent'}
		
	},

	onBeforeAction: function() {
		this.next();
	},

	action: function() {
		if(this.isReady()) { this.render(); } else { this.render("C_CampaignsDetails"); this.render("loading", { to: "C_CampaignsDetailsSubcontent" });}
		/*ACTION_FUNCTION*/
	},

	isReady: function() {
		

		var subs = [
			Meteor.subscribe("cms_section_list"),
			Meteor.subscribe("cms_campaign_items_empty"),
			Meteor.subscribe("cms_campaign_details", this.params.campaignId)
		];
		var ready = true;
		_.each(subs, function(sub) {
			if(!sub.ready())
				ready = false;
		});
		return ready;
	},

	data: function() {
		

		var data = {
			params: this.params || {},
			cms_campaign_items_empty: CmsCampaignItems.findOne({_id:null}, {}),
			cms_campaign_details: CmsCampaigns.findOne({_id:this.params.campaignId}, {}),

			cms_section_list: CmsSections.find({}, {sort:[["cardID","cardType","sectionName","desc"]]})
		};
		

		

		return data;
	},

	onAfterAction: function() {
		
	}
});