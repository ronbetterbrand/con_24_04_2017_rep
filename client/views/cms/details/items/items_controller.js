this.C_CampaignsDetailsItemsController = RouteController.extend({
	template: "C_CampaignsDetails",


	yieldTemplates: {
		'C_CampaignsDetailsItems': { to: 'C_CampaignsDetailsSubcontent'}

	},

	onBeforeAction: function() {
		this.next();
	},

	action: function() {
		if(this.isReady()) { this.render(); } else { this.render("C_CampaignsDetails"); this.render("loading", { to: "C_CampaignsDetailsSubcontent" });}
		/*ACTION_FUNCTION*/
	},

	isReady: function() {


		var subs = [
			Meteor.subscribe("cms_campaign_items", this.params.campaignId),
			Meteor.subscribe("cms_campaign_details", this.params.campaignId)
		];
		var ready = true;
		_.each(subs, function(sub) {
			if(!sub.ready())
				ready = false;
		});
		return ready;
	},

	data: function() {
		var url = window.location.href;
		var sectionName = url.substring(url.indexOf('?') + 7);
		//var res = sectionName.replace(, " ");
		var a=sectionName.split("%20");
		var sectionName=a.join(" ");
		var data = {
			params: this.params || {},
			cms_campaign_item: CmsCampaignItems.findOne({_id: this.params.itemId}, {}),
			cms_campaign_items: CmsCampaignItems.find({sectionName: sectionName}, {}),
			cms_campaign_details: CmsCampaigns.findOne({_id: this.params.campaignId}, {}),
		};




		return data;
	},

	onAfterAction: function() {

	}
});