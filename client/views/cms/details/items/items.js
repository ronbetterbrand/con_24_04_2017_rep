var pageSession = new ReactiveDict();

Template.C_CampaignsDetailsItems.rendered = function() {

};

Template.C_CampaignsDetailsItems.events({

	"click #dataview-insert-button": function(e, t) {
		var query = "sname=" + t.data.params.query.sname;
		e.preventDefault();
		Router.go("cms.details.insert", {campaignId: this.params.campaignId}, {query: query});
		//e.preventDefault();
		//Router.go("cms_campaigns.details.insert", {campaignId: this.params.campaignId});
	}

});

Template.C_CampaignsDetailsItems.helpers({

	'sectionName': function (){

		return this.params.query.sname;
	}

});

var C_CampaignsDetailsItemsViewItems = function(cursor) {
	if(!cursor) {
		return [];
	}

	var searchString = pageSession.get("C_CampaignsDetailsItemsViewSearchString");
	var sortBy = pageSession.get("C_CampaignsDetailsItemsViewSortBy");
	var sortAscending = pageSession.get("C_CampaignsDetailsItemsViewSortAscending");
	if(typeof(sortAscending) == "undefined") sortAscending = true;

	var raw = cursor.fetch();

	// filter
	var filtered = [];
	if(!searchString || searchString == "") {
		filtered = raw;
	} else {
		searchString = searchString.replace(".", "\\.");
		var regEx = new RegExp(searchString, "i");
		var searchFields = ["description", "key", "type", "title"];
		filtered = _.filter(raw, function(item) {
			var match = false;
			_.each(searchFields, function(field) {
				var value = (getPropertyValue(field, item) || "") + "";

				match = match || (value && value.match(regEx));
				if(match) {
					return false;
				}
			})
			return match;
		});
	}

	// sort
	if(sortBy) {
		filtered = _.sortBy(filtered, sortBy);

		// descending?
		if(!sortAscending) {
			filtered = filtered.reverse();
		}
	}

	return filtered;
};

var C_CampaignsDetailsItemsViewExport = function(cursor, fileType) {
	var data = C_CampaignsDetailsItemsViewItems(cursor);
	var exportFields = ["description", "key", "type", "title"];

	var str = convertArrayOfObjects(data, exportFields, fileType);

	var filename = "export." + fileType;

	downloadLocalResource(str, filename, "application/octet-stream");
}


Template.C_CampaignsDetailsItemsView.rendered = function() {
	pageSession.set("C_CampaignsDetailsItemsViewStyle", "table");

};

Template.C_CampaignsDetailsItemsView.events({
	"submit #dataview-controls": function(e, t) {
		return false;
	},

	"click #dataview-search-button": function(e, t) {
		e.preventDefault();
		var form = $(e.currentTarget).parent();
		if(form) {
			var searchInput = form.find("#dataview-search-input");
			if(searchInput) {
				searchInput.focus();
				var searchString = searchInput.val();
				pageSession.set("C_CampaignsDetailsItemsViewSearchString", searchString);
			}

		}
		return false;
	},

	"keydown #dataview-search-input": function(e, t) {
		if(e.which === 13)
		{
			e.preventDefault();
			var form = $(e.currentTarget).parent();
			if(form) {
				var searchInput = form.find("#dataview-search-input");
				if(searchInput) {
					var searchString = searchInput.val();
					pageSession.set("C_CampaignsDetailsItemsViewSearchString", searchString);
				}

			}
			return false;
		}

		if(e.which === 27)
		{
			e.preventDefault();
			var form = $(e.currentTarget).parent();
			if(form) {
				var searchInput = form.find("#dataview-search-input");
				if(searchInput) {
					searchInput.val("");
					pageSession.set("C_CampaignsDetailsItemsViewSearchString", "");
				}

			}
			return false;
		}

		return true;
	},


	"click #dataview-export-default": function(e, t) {
		e.preventDefault();
		CampaignsDetailsItemsViewExport(this.cms_campaign_items, "csv");
	},

	"click #dataview-export-csv": function(e, t) {
		e.preventDefault();
		CampaignsDetailsItemsViewExport(this.cms_campaign_items, "csv");
	},

	"click #dataview-export-tsv": function(e, t) {
		e.preventDefault();
		CampaignsDetailsItemsViewExport(this.cms_campaign_items, "tsv");
	},

	"click #dataview-export-json": function(e, t) {
		e.preventDefault();
		CampaignsDetailsItemsViewExport(this.cms_campaign_items, "json");
	}


});

Template.C_CampaignsDetailsItemsView.helpers({

	"insertButtonClass": function() {
		return CmsCampaignItems.userCanInsert(Meteor.userId(), {}) ? "" : "hidden";
	},

	"isEmpty": function() {
		return !this.cms_campaign_items || this.cms_campaign_items.count() == 0;
	},
	"isNotEmpty": function() {
		return this.cms_campaign_items && this.cms_campaign_items.count() > 0;
	},
	"isNotFound": function() {
		return this.cms_campaign_items && pageSession.get("C_CampaignsDetailsItemsViewSearchString") && C_CampaignsDetailsItemsViewItems(this.cms_campaign_items).length == 0;
	},
	"searchString": function() {
		return pageSession.get("C_CampaignsDetailsItemsViewSearchString");
	},
	"viewAsTable": function() {
		return pageSession.get("C_CampaignsDetailsItemsViewStyle") == "table";
	},
	"viewAsList": function() {
		return pageSession.get("C_CampaignsDetailsItemsViewStyle") == "list";
	},
	"viewAsGallery": function() {
		return pageSession.get("C_CampaignsDetailsItemsViewStyle") == "gallery";
	},
	"sectionName": function() {
		return window.location.search.split("=")[1];
	}


});


Template.C_CampaignsDetailsItemsViewTable.rendered = function() {
	var table = document.getElementsByTagName('table')[0],
		rows = table.getElementsByTagName('tr'),
		text = 'textContent' in document ? 'textContent' : 'innerText';
	console.log(text);

	for (var i = 1, len = rows.length; i < len; i++){
		rows[i].children[0][text] = i + ' ' + rows[i].children[0][text];
	}
};

Template.C_CampaignsDetailsItemsViewTable.events({
	"click .th-sortable": function(e, t) {
		e.preventDefault();
		var oldSortBy = pageSession.get("C_CampaignsDetailsItemsViewSortBy");
		var newSortBy = $(e.target).attr("data-sort");

		pageSession.set("C_CampaignsDetailsItemsViewSortBy", newSortBy);
		if(oldSortBy == newSortBy) {
			var sortAscending = pageSession.get("C_CampaignsDetailsItemsViewSortAscending") || false;
			pageSession.set("C_CampaignsDetailsItemsViewSortAscending", !sortAscending);
		} else {
			pageSession.set("C_CampaignsDetailsItemsViewSortAscending", true);
		}
	}
});

Template.C_CampaignsDetailsItemsViewTable.helpers({
	"tableItems": function() {
		return C_CampaignsDetailsItemsViewItems(this.cms_campaign_items);
	}
});


Template.C_CampaignsDetailsItemsViewTableItems.rendered = function() {

};

Template.C_CampaignsDetailsItemsViewTableItems.events({
	"click td": function(e, t) {
		e.preventDefault();

		/**/
		return false;
	},

	"click .inline-checkbox": function(e, t) {
		e.preventDefault();

		if(!this || !this._id) return false;

		var fieldName = $(e.currentTarget).attr("data-field");
		if(!fieldName) return false;

		var values = {};
		values[fieldName] = !this[fieldName];

		CmsCampaignItems.update({ _id: this._id }, { $set: values });

		return false;
	},

	"click #delete-button": function(e, t) {
		e.preventDefault();
		var me = this;
		bootbox.dialog({
			message: "Delete? Are you sure?",
			title: "Delete",
			animate: false,
			buttons: {
				success: {
					label: "Yes",
					className: "btn-success",
					callback: function() {
						CmsCampaignItems.remove({ _id: me._id });
					}
				},
				danger: {
					label: "No",
					className: "btn-default"
				}
			}
		});
		return false;
	},
	"click #edit-button": function(e, t) {
		e.preventDefault();

		var query = "sname=" + t.data.sectionName;

		// Router.go("cms_campaigns.details.insert", {campaignId: this.params.campaignId}, {query: query});

		Router.go("cms.details.edit", {campaignId: UI._parentData(1).params.campaignId, itemId: this._id}, {query: query});
		return false;
	},


	"click #clone-button": function(e, t) {
		e.preventDefault();
		var self = this;

		var cid = UI._parentData(1).params.campaignId;
		console.log("id="+this._id+" cid="+cid);

		var c = CmsCampaignItems.findOne( { _id: this._id });//.fetch();

		console.log("tit1="+ c._id);

		//values = t.data.cms_campaign_item;
		//values.name = "Copy of "+itemName;
		var ncid = new Meteor.Collection.ObjectID().valueOf();
		c._id = ncid;
		c.title = "Copy of "+ c.title;

		if( c.button1Data.indexOf("Rm_Tell_me_more") > -1){
			c.button1Data = "Rm_Tell_me_more"+Math.floor(Math.random() * 1000);
			//console.log("button1Data="+values.button1Data);
		}
		if( c.button2Data.indexOf("Rm_Tell_me_more") > -1){
			c.button2Data = "Rm_Tell_me_more"+Math.floor(Math.random() * 1000);
			//console.log("button1Data="+values.button1Data);
		}
		if( c.button3Data.indexOf("Rm_Tell_me_more") > -1){
			c.button3Data = "Rm_Tell_me_more"+Math.floor(Math.random() * 1000);
			//console.log("button1Data="+values.button1Data);
		}


		CmsCampaignItems.insert(c, function (e) {
			console.log("Success save dup item ");
			//Router.go("cms_campaigns");
			//Router.go("cms_campaigns.details", {campaignId: cid});

			setTimeout(function(){
				console.log("refresh page");
				document.location.reload(true);
				//history.back();
			}, 300);

		});


		//validateForm(
		// $(e.target),
		// function(fieldName, fieldValue) {
		//
		// },
		// function(msg) {
		//
		// },
		// function(values) {
		//
		//    values = t.data.cms_campaign;
		//    console.log("value="+values);
		//
		//    //values.title = "Copy of "+this.cms_campaign_item;//t.data.cms_campaigns.title;
		//    //var ncid = new Meteor.Collection.ObjectID().valueOf();
		//    /*values._id = ncid;
		//    CmsCampaignItems.insert(values, function (e) {
		//       console.log("Success save dup item ");
		//
		//    Router.go("cms_campaigns.details.edit", {campaignId: cid, itemId: this._id});
		//        return false;
		//
		//    });*/
		//
		//
		// }
		//);

	}
});

Template.C_CampaignsDetailsItemsViewTableItems.helpers({
	"checked": function(value) { return value ? "checked" : "" },
	"editButtonClass": function() {
		return CmsCampaignItems.userCanUpdate(Meteor.userId(), this) ? "" : "hidden";
	},

	"deleteButtonClass": function() {
		return CmsCampaignItems.userCanRemove(Meteor.userId(), this) ? "" : "hidden";
	},
	"actvie1": function() {
		//c = CmsCampaignItems.userCanUpdate(Meteor.userId(), this);
		//console.log("sdate="+c.sdate);
		return false;
	},
	"scheduled1": function() {
		return false;
	},
	"inactvie1": function() {
		return true;
	},
	"statusData": function() {
		var today = new Date();
		var c = CmsCampaignItems.findOne( { _id: this._id });
		console.log(c+" sdate="+c.sdate);

		if(today > c.edate){
			//return "inactive";
			return Spacebars.SafeString('<span class="badge bgm-gray">inactive</span>');

		}else
		if(today < c.sdate){
			//return "scheduled";
			return Spacebars.SafeString('<span class="badge bgm-orange">scheduled</span>');

		}else{
			//return "active";
			return Spacebars.SafeString('<span class="badge bgm-green">active</span>');
		}

	}
});