this.C_CampaignsController = RouteController.extend({
	template: "C_Campaigns",
	

	yieldTemplates: {
		/*YIELD_TEMPLATES*/
	},

	onBeforeAction: function() {
		this.next();
	},

	action: function() {
		if(this.isReady()) { this.render(); } else { this.render("loading"); }
		/*ACTION_FUNCTION*/
	},

	isReady: function() {
		

		var subs = [
			Meteor.subscribe("cms_campaign_list")
		];
		var ready = true;
		_.each(subs, function(sub) {
			if(!sub.ready())
				ready = false;
		});
		return ready;
	},

	data: function() {
		

		var data = {
			params: this.params || {},
			cms_campaign_list: CmsCampaigns.find({}, {sort:[["campaignName","desc"]]})
		};
		

		

		return data;
	},

	onAfterAction: function() {
		
	}
});