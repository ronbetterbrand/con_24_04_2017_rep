var pageSession = new ReactiveDict();
pageSession.set("showHelp", true);

Template.C_Campaigns.onRendered(function(){
	if(!pageSession.get("showHelp")) {
		$("#chatsuite-introdution").addClass("perhide");
	}
});
Template.C_Campaigns.rendered = function() {
	
};

Template.C_Campaigns.events({
	"click #dataview-insert-button": function(e, t) {
		e.preventDefault();
		Router.go("cms.insert", {});
	},
	"click .close": function(e, t) {

		pageSession.set("showHelp",false)
	}
	
});

Template.C_Campaigns.helpers({
	"isNotEmpty": function() {
		return this.cms_campaign_list && this.cms_campaign_list.count() > 0;
	}
	
});

var C_CampaignsViewItems = function(cursor) {
	if(!cursor) {
		return [];
	}

	var searchString = pageSession.get("C_CampaignsViewSearchString");
	var sortBy = pageSession.get("C_CampaignsViewSortBy");
	var sortAscending = pageSession.get("C_CampaignsViewSortAscending");
	if(typeof(sortAscending) == "undefined") sortAscending = true;

	var raw = cursor.fetch();

	// filter
	var filtered = [];
	if(!searchString || searchString == "") {
		filtered = raw;
	} else {
		searchString = searchString.replace(".", "\\.");
		var regEx = new RegExp(searchString, "i");
		var searchFields = ["campaignName", "date"];
		filtered = _.filter(raw, function(item) {
			var match = false;
			_.each(searchFields, function(field) {
				var value = (getPropertyValue(field, item) || "") + "";

				match = match || (value && value.match(regEx));
				if(match) {
					return false;
				}
			})
			return match;
		});
	}

	// sort
	if(sortBy) {
		filtered = _.sortBy(filtered, sortBy);

		// descending?
		if(!sortAscending) {
			filtered = filtered.reverse();
		}
	}

	return filtered;
};

var C_CampaignsViewExport = function(cursor, fileType) {
	var data = C_CampaignsViewItems(cursor);
	var exportFields = ["campaignName", "date", "customer.name"];

	var str = convertArrayOfObjects(data, exportFields, fileType);

	var filename = "export." + fileType;

	downloadLocalResource(str, filename, "application/octet-stream");
}


Template.C_CampaignsView.rendered = function() {
	pageSession.set("C_CampaignsViewStyle", "table");
	
};

Template.C_Campaigns.events({
	"submit #dataview-controls": function(e, t) {
		return false;
	},

	"click #dataview-search-button": function(e, t) {
		e.preventDefault();
		var form = $(e.currentTarget).parent();
		if(form) {
			var searchInput = form.find("#dataview-search-input");
			if(searchInput) {
				searchInput.focus();
				var searchString = searchInput.val();
				pageSession.set("C_CampaignsViewSearchString", searchString);
			}

		}
		return false;
	},

	"keydown #dataview-search-input": function(e, t) {
		if(e.which === 13)
		{
			e.preventDefault();
			var form = $(e.currentTarget).parent();
			if(form) {
				var searchInput = form.find("#dataview-search-input");
				if(searchInput) {
					var searchString = searchInput.val();
					pageSession.set("C_CampaignsViewSearchString", searchString);
				}

			}
			return false;
		}

		if(e.which === 27)
		{
			e.preventDefault();
			var form = $(e.currentTarget).parent();
			if(form) {
				var searchInput = form.find("#dataview-search-input");
				if(searchInput) {
					searchInput.val("");
					pageSession.set("C_CampaignsViewSearchString", "");
				}

			}
			return false;
		}

		return true;
	},



	"click #dataview-export-default": function(e, t) {
		e.preventDefault();
		C_CampaignsViewExport(this.cms_campaign_list, "csv");
	},

	"click #dataview-export-csv": function(e, t) {
		e.preventDefault();
		C_CampaignsViewExport(this.cms_campaign_list, "csv");
	},

	"click #dataview-export-tsv": function(e, t) {
		e.preventDefault();
		C_CampaignsViewExport(this.cms_campaign_list, "tsv");
	},

	"click #dataview-export-json": function(e, t) {
		e.preventDefault();
		C_CampaignsViewExport(this.cms_campaign_list, "json");
	}
});

Template.C_CampaignsView.helpers({

	"insertButtonClass": function() {
		return CmsCampaigns.userCanInsert(Meteor.userId(), {}) ? "" : "hidden";
	},

	"isEmpty": function() {
		return !this.cms_campaign_list || this.cms_campaign_list.count() == 0;
	},
	"isNotEmpty": function() {
		return this.cms_campaign_list && this.cms_campaign_list.count() > 0;
	},
	"isNotFound": function() {
		return this.cms_campaign_list && pageSession.get("C_CampaignsViewSearchString") && C_CampaignsViewItems(this.cms_campaign_list).length == 0;
	},
	"searchString": function() {
		return pageSession.get("C_CampaignsViewSearchString");
	},
	"viewAsTable": function() {
		return pageSession.get("C_CampaignsViewStyle") == "table";
	},
	"viewAsList": function() {
		return pageSession.get("C_CampaignsViewStyle") == "list";
	},
	"viewAsGallery": function() {
		return pageSession.get("C_CampaignsViewStyle") == "gallery";
	}
});


Template.C_CampaignsViewTable.rendered = function() {
	
};

Template.C_CampaignsViewTable.events({
	"click .th-sortable": function(e, t) {
		e.preventDefault();
		var oldSortBy = pageSession.get("C_CampaignsViewSortBy");
		var newSortBy = $(e.target).attr("data-sort");

		pageSession.set("C_CampaignsViewSortBy", newSortBy);
		if(oldSortBy == newSortBy) {
			var sortAscending = pageSession.get("C_CampaignsViewSortAscending") || false;
			pageSession.set("C_CampaignsViewSortAscending", !sortAscending);
		} else {
			pageSession.set("C_CampaignsViewSortAscending", true);
		}
	}
});

Template.C_CampaignsViewTable.helpers({
	"tableItems": function() {
		return C_CampaignsViewItems(this.cms_campaign_list);
	}
});


Template.C_CampaignsViewTableItems.rendered = function() {
	
};

Template.C_CampaignsViewTableItems.events({
	//"click td": function(e, t) {
	//	e.preventDefault();

		//Router.go("cms_campaigns.details", {campaignId: this._id});
	//	return false;
	//},

	"click .inline-checkbox": function(e, t) {
		e.preventDefault();

		if(!this || !this._id) return false;

		var fieldName = $(e.currentTarget).attr("data-field");
		if(!fieldName) return false;

		var values = {};
		values[fieldName] = !this[fieldName];

		CmsCampaigns.update({ _id: this._id }, { $set: values });

		return false;
	},

	"click #delete-button": function(e, t) {
		e.preventDefault();
		var me = this;
		bootbox.dialog({
			message: "Delete? Are you sure?",
			title: "Delete",
			animate: false,
			buttons: {
				success: {
					label: "Yes",
					className: "btn-success",
					callback: function() {
						CmsCampaigns.remove({ _id: me._id });
					}
				},
				danger: {
					label: "No",
					className: "btn-default"
				}
			}
		});
		return false;
	},
	"click #edit-button": function(e, t) {
		e.preventDefault();
		Router.go("cms.edit", {campaignId: this._id});
		return false;
	}
});

Template.C_CampaignsViewTableItems.helpers({
	"checked": function(value) { return value ? "checked" : "" }, 
	"editButtonClass": function() {
		return CmsCampaigns.userCanUpdate(Meteor.userId(), this) ? "" : "hidden";
	},
	"deleteButtonClass": function() {
		return CmsCampaigns.userCanRemove(Meteor.userId(), this) ? "" : "hidden";
	}
});

Template.C_CampaignsViewTableItems.events({
	"click td": function(e, t) {
		var query = "id=" + t.data.campaignName+"&cid="+t.data._id;
		e.preventDefault();
		Router.go("cms.sections", {}, {query: query});
		return false;
	}
});
