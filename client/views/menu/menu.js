var pageSession = new ReactiveDict();

function getParameterByName(name) {
	name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
		results = regex.exec(location.search);
	return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

Template.Menu.rendered = function() {
	
};

Template.Menu.events({
	
});

Template.Menu.helpers({
	
});

var MenuViewItems = function(cursor) {
	if(!cursor) {
		return [];
	}

	var searchString = pageSession.get("MenuViewSearchString");
	var sortBy = pageSession.get("MenuViewSortBy");
	var sortAscending = pageSession.get("MenuViewSortAscending");
	if(typeof(sortAscending) == "undefined") sortAscending = true;

	var raw = cursor.fetch();

	// filter
	var filtered = [];
	if(!searchString || searchString == "") {
		filtered = raw;
	} else {
		searchString = searchString.replace(".", "\\.");
		var regEx = new RegExp(searchString, "i");
		var searchFields = ["fbPageID","campaignName","careatedAt","modifiedAt"];
		filtered = _.filter(raw, function(item) {
			var match = false;
			_.each(searchFields, function(field) {
				var value = (getPropertyValue(field, item) || "") + "";

				match = match || (value && value.match(regEx));
				if(match) {
					return false;
				}
			})
			return match;
		});
	}

	// sort
	if(sortBy) {
		filtered = _.sortBy(filtered, sortBy);

		// descending?
		if(!sortAscending) {
			filtered = filtered.reverse();
		}
	}

	return filtered;
};

var MenuViewExport = function(cursor, fileType) {
	var data = MenuViewItems(cursor);
	var exportFields = ["cardID","cardType","menuName","campaignName", "date"];

	var str = convertArrayOfObjects(data, exportFields, fileType);

	var filename = "export." + fileType;

	downloadLocalResource(str, filename, "application/octet-stream");
}


Template.MenuView.rendered = function() {
	pageSession.set("MenuViewStyle", "table");
	
};

Template.MenuView.events({
	"submit #dataview-controls": function(e, t) {
		return false;
	},

	"click #dataview-search-button": function(e, t) {
		e.preventDefault();
		var form = $(e.currentTarget).parent();
		if(form) {
			var searchInput = form.find("#dataview-search-input");
			if(searchInput) {
				searchInput.focus();
				var searchString = searchInput.val();
				pageSession.set("MenuViewSearchString", searchString);
			}

		}
		return false;
	},

	"keydown #dataview-search-input": function(e, t) {
		if(e.which === 13)
		{
			e.preventDefault();
			var form = $(e.currentTarget).parent();
			if(form) {
				var searchInput = form.find("#dataview-search-input");
				if(searchInput) {
					var searchString = searchInput.val();
					pageSession.set("MenuViewSearchString", searchString);
				}

			}
			return false;
		}

		if(e.which === 27)
		{
			e.preventDefault();
			var form = $(e.currentTarget).parent();
			if(form) {
				var searchInput = form.find("#dataview-search-input");
				if(searchInput) {
					searchInput.val("");
					pageSession.set("MenuViewSearchString", "");
				}

			}
			return false;
		}

		return true;
	},

	"click #dataview-insert-button": function(e, t) {
		e.preventDefault();
		//var id = getParameterByName('id');
		//var query = "cn=" + this.params.query.id;
		Router.go("menu.insert", {_id: 1}, {query: this.params.query});
	},

	"click #dataview-export-default": function(e, t) {
		e.preventDefault();
		MenuViewExport(this.cms_menu_list, "csv");
	},

	"click #dataview-export-csv": function(e, t) {
		e.preventDefault();
		MenuViewExport(this.cms_menu_list, "csv");
	},

	"click #dataview-export-tsv": function(e, t) {
		e.preventDefault();
		MenuViewExport(this.cms_menu_list, "tsv");
	},

	"click #dataview-export-json": function(e, t) {
		e.preventDefault();
		MenuViewExport(this.cms_menu_list, "json");
	},
	"campaignName": function () {
		return getParameterByName('id');

	}

	
});

Template.MenuView.helpers({

	"getID":function(){
		return getParameterByName('id');
	},
	"insertButtonClass": function() {
		return CmsMenu.userCanInsert(Meteor.userId(), {}) ? "" : "hidden";
	},

	"isEmpty": function() {
		return !this.cms_menu_list || this.cms_menu_list.count() == 0;
	},
	"isNotEmpty": function() {
		return this.cms_menu_list && this.cms_menu_list.count() > 0;
	},
	"isNotFound": function() {
		return this.cms_menu_list && pageSession.get("MenuViewSearchString") && MenuViewItems(this.cms_menu_list).length == 0;
	},
	"searchString": function() {
		return pageSession.get("MenuViewSearchString");
	},
	"viewAsTable": function() {
		return pageSession.get("MenuViewStyle") == "table";
	},
	"viewAsList": function() {
		return pageSession.get("MenuViewStyle") == "list";
	},
	"viewAsGallery": function() {
		return pageSession.get("MenuViewStyle") == "gallery";
	},
	"campaignName": function () {
		return getParameterByName('id');

	},
	"campaignId": function () {
		return getParameterByName('cid');

	}

	
});


Template.MenuViewTable.rendered = function() {
	
};

Template.MenuViewTable.events({
	"click .th-sortable": function(e, t) {
		e.preventDefault();
		var oldSortBy = pageSession.get("MenuViewSortBy");
		var newSortBy = $(e.target).attr("data-sort");

		pageSession.set("MenuViewSortBy", newSortBy);
		if(oldSortBy == newSortBy) {
			var sortAscending = pageSession.get("MenuViewSortAscending") || false;
			pageSession.set("MenuViewSortAscending", !sortAscending);
		} else {
			pageSession.set("MenuViewSortAscending", true);
		}
	}
});



Template.MenuViewTable.helpers({
	"tableItems": function()
	{


		var id = getParameterByName('id');
		console.log("campaigName="+id);
return MenuViewItems(CmsMenu.find({ campaignName: id  }));
//this.cms_section_list);
	}
});


Template.MenuViewTableItems.rendered = function() {
	
};

Template.MenuViewTableItems.events({
	"click td": function(e, t) {
		e.preventDefault();
		
		Router.go("menu.details", {menuId: this._id});
		return false;
	},

	"click .inline-checkbox": function(e, t) {
		e.preventDefault();

		if(!this || !this._id) return false;

		var fieldName = $(e.currentTarget).attr("data-field");
		if(!fieldName) return false;

		var values = {};
		values[fieldName] = !this[fieldName];

		CmsMenu.update({ _id: this._id }, { $set: values });

		return false;
	},

	"click #delete-button": function(e, t) {
		e.preventDefault();
		var me = this;
		bootbox.dialog({
			message: "Delete? Are you sure?",
			title: "Delete",
			animate: false,
			buttons: {
				success: {
					label: "Yes",
					className: "btn-success",
					callback: function() {
						CmsMenu.remove({ _id: me._id });
					}
				},
				danger: {
					label: "No",
					className: "btn-default"
				}
			}
		});
		return false;
	},
	"click #edit-button": function(e, t) {
		e.preventDefault();
		Router.go("menu.edit", {menuId: this._id},{ query: UI._parentData(1).params.query});
		return false;
	}
});

Template.MenuViewTableItems.helpers({
	"checked": function(value) { return value ? "checked" : "" }, 
	"editButtonClass": function() {
		return CmsMenu.userCanUpdate(Meteor.userId(), this) ? "" : "hidden";
	},

	"deleteButtonClass": function() {
		return CmsMenu.userCanRemove(Meteor.userId(), this) ? "" : "hidden";
	}
});
