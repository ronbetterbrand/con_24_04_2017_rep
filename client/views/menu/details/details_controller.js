this.MenuDetailsController = RouteController.extend({
	template: "MenuDetails",


	yieldTemplates: {
		/*YIELD_TEMPLATES*/
	},

	onBeforeAction: function() {
		this.next();
	},

	action: function() {
		Router.go("menu", {});
		///console.log("we don't have - sections.details.items");
		//this.redirect('sections.details', this.params || {}, { replaceState: true });
		//this.next();
		///???this.redirect('sections.details.items', this.params || {}, { replaceState: true });
		/*ACTION_FUNCTION*/
	},

	isReady: function() {


		var subs = [
			Meteor.subscribe("cms_menu_details", this.params.menuId)
		];
		var ready = true;
		_.each(subs, function(sub) {
			if(!sub.ready())
				ready = false;
		});
		return ready;
	},

	data: function() {


		var data = {
			params: this.params || {},
			cms_menu_details: CmsMenu.findOne({_id:this.params.menuId}, {})
		};




		return data;
	},

	onAfterAction: function() {

	}
});