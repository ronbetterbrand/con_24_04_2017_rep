this.MenuController = RouteController.extend({
	template: "Menu",
	

	yieldTemplates: {
		/*YIELD_TEMPLATES*/
	},

	onBeforeAction: function() {
		this.next();
	},

	action: function() {
		if(this.isReady()) { this.render(); } else { this.render("loading"); }
		/*ACTION_FUNCTION*/
	},

	isReady: function() {
		

		var subs = [
			Meteor.subscribe("cms_menu_list"),
		];
		var ready = true;
		_.each(subs, function(sub) {
			if(!sub.ready())
				ready = false;
		});
		return ready;
	},

	data: function() {
		

		var data = {
			params: this.params || {},
			cms_menu_list: CmsMenu.find({campaignName:this.params.query.id}, {sort:[["cardID","cardType","menuName","desc"]]})

		};
		

		

		return data;
	},

	onAfterAction: function() {
		
	}
});