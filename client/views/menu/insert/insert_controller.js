this.MenuInsertController = RouteController.extend({
	template: "MenuInsert",


	yieldTemplates: {
		/*YIELD_TEMPLATES*/
	},

	onBeforeAction: function() {
		this.next();
	},

	action: function() {
		if(this.isReady()) { this.render(); } else { this.render("loading"); }
		/*ACTION_FUNCTION*/
	},

	isReady: function() {


		var subs = [
			Meteor.subscribe("cms_customer_list"),
			Meteor.subscribe("cms_menu_empty"),
			Meteor.subscribe("cms_menu_list")
		];
		var ready = true;
		_.each(subs, function(sub) {
			if(!sub.ready())
				ready = false;
		});
		return ready;
	},

	data: function() {


		var data = {
			params: this.params || {},
			cms_customer_list: CmsCustomers.find({}, {}),
			cms_menu_empty: CmsMenu.findOne({_id:null}, {}),
			cms_menu_list: CmsMenu.find({}, {sort:[["menuName","desc"]]})

		};




		return data;
	},

	onAfterAction: function() {

	}
});