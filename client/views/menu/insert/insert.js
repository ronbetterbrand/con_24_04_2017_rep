var pageSession = new ReactiveDict();

function getParameterByName(name) {
	name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
		results = regex.exec(location.search);
	return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

Template.MenuInsert.rendered = function() {

};

Template.MenuInsert.events({

});

Template.MenuInsert.helpers({

});

Template.MenuInsertInsertForm.rendered = function() {
	$('[data-toggle="tooltip"]').tooltip();

	//console.log("here...");

	pageSession.set("menuInsertInsertFormInfoMessage", "");
	pageSession.set("menuInsertInsertFormErrorMessage", "");

	$("input[type='file']").fileinput();
	$("select[data-role='tagsinput']").tagsinput();
	$(".bootstrap-tagsinput").addClass("form-control");
	$("input[autofocus]").focus();


};

Template.MenuInsertInsertForm.events({
	"submit": function(e, t) {
		e.preventDefault();
		pageSession.set("menuInsertInsertFormInfoMessage", "");
		pageSession.set("menuInsertInsertFormErrorMessage", "");

		var self = this;
		var isApply = 0;//-1;

		function submitAction(msg) {
			var menuInsertInsertFormMode = "insert";
			if(!t.find("#form-cancel-button")) {
				switch(menuInsertInsertFormMode) {
					case "insert": {
						$(e.target)[0].reset();
					}; break;

					case "update": {
						var message = msg || "Saved.";
						pageSession.set("menuInsertInsertFormInfoMessage", message);
					}; break;
				}
			}

			//Router.go("menu.details", {menuId: newId});
		}

		function errorAction(msg) {
			msg = msg || "";
			var message = msg.message || msg || "Error.";
			pageSession.set("menuInsertInsertFormErrorMessage", message);
		}

		validateForm(
			$(e.target),
			function(fieldName, fieldValue) {
				//console.log("f="+fieldName+" v="+fieldValue);

				//var c = CmsMenu.findOne({"sectionName": fieldValue});


				//if( fieldName=="menuName"){
                //
				//	if(fieldValue.toString().length < 3){
				//		bootbox.alert("Menu name length >=3 ", function() {
				//			//Router.go("/menu?id="+"c2", {});
				//			return false;
				//		});
				//	}
				//	//else if( c.sectionName==fieldValue ){
				//	//	bootbox.alert("Please use a unique section name", function() {
				//	//		Router.go("menu", {});
				//	//		return;
				//	//	});
				//	//}
				//	else{
				//		isApply=0;
				//	}
                //
				//}

			},
			function(msg) {
				console.log("msg="+msg);
			},
			function(values) {

				if (isApply==0){

					var c = getParameterByName('id');
					console.log ("c="+ c);
					values.campaignName = c;

					newId = CmsMenu.insert(values, function(e)
					{
						if(e) errorAction(e); else submitAction();
						history.back();
					});
				}

			}
		);

		return false;
	},
	"click #form-cancel-button": function(e, t) {
		e.preventDefault();

		Router.go("menu",  {_id: 1}, {query: this.params.query});

		
	},
	"click #form-close-button": function(e, t) {
		e.preventDefault();

		/*CLOSE_REDIRECT*/
	},
	"click #form-back-button": function(e, t) {
		e.preventDefault();

		/*BACK_REDIRECT*/
	}


});

Template.MenuInsertInsertForm.helpers({
	"infoMessage": function() {
		return pageSession.get("menuInsertInsertFormInfoMessage");
	},
	"errorMessage": function() {
		return pageSession.get("menuInsertInsertFormErrorMessage");
	},
	"campaignName": function () {
		return getParameterByName('id');

	},
	"campaignId": function () {
		return getParameterByName('cid');

	}

});
