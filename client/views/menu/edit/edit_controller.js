this.MenuEditController = RouteController.extend({
	template: "MenuEdit",


	yieldTemplates: {
		/*YIELD_TEMPLATES*/
	},

	onBeforeAction: function() {
		this.next();
	},

	action: function() {
		if(this.isReady()) { this.render(); } else { this.render("loading"); }
		/*ACTION_FUNCTION*/
	},

	isReady: function() {


		var subs = [
			Meteor.subscribe("cms_customer_list"),
			Meteor.subscribe("cms_menu_details", this.params.menuId)
		];
		var ready = true;
		_.each(subs, function(sub) {
			if(!sub.ready())
				ready = false;
		});
		return ready;
	},

	data: function() {


		var data = {
			params: this.params || {},
			cms_customer_list: CmsCustomers.find({}, {}),
			cms_menu_details: CmsMenu.findOne({_id:this.params.menuId}, {})
		};




		return data;
	},

	onAfterAction: function() {

	}
});