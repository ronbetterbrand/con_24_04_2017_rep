var pageSession = new ReactiveDict();

Template.MenuEdit.rendered = function() {

};

Template.MenuEdit.events({

});

Template.MenuEdit.helpers({

});

Template.MenuEditEditForm.rendered = function() {


	pageSession.set("menuEditEditFormInfoMessage", "");
	pageSession.set("menuEditEditFormErrorMessage", "");

	$(".input-group.date").each(function() {
		var format = $(this).find("input[type='text']").attr("data-format");

		if(format) {
			format = format.toLowerCase();
		}
		else {
			format = "mm/dd/yyyy";
		}

		$(this).datepicker({
			autoclose: true,
			todayHighlight: true,
			todayBtn: true,
			forceParse: false,
			keyboardNavigation: false,
			format: format
		});
	});

	$("input[type='file']").fileinput();
	$("select[data-role='tagsinput']").tagsinput();
	$(".bootstrap-tagsinput").addClass("form-control");
	$("input[autofocus]").focus();
};

Template.MenuEditEditForm.events({
	"submit": function(e, t) {
		e.preventDefault();
		pageSession.set("menuEditEditFormInfoMessage", "");
		pageSession.set("menuEditEditFormErrorMessage", "");

		var self = this;

		function submitAction(msg) {
			var menuEditEditFormMode = "update";
			if(!t.find("#form-cancel-button")) {
				switch(menuEditEditFormMode) {
					case "insert": {
						$(e.target)[0].reset();
					}; break;

					case "update": {
						var message = msg || "Saved.";
						pageSession.set("menuEditEditFormInfoMessage", message);
					}; break;
				}
			}


			history.back();


		}

		function errorAction(msg) {
			msg = msg || "";
			var message = msg.message || msg || "Error.";
			pageSession.set("menuEditEditFormErrorMessage", message);
		}

		validateForm(
			$(e.target),
			function(fieldName, fieldValue) {

			},
			function(msg) {

			},
			function(values) {


				CmsMenu.update(
					{ _id: t.data.cms_menu_details._id },
					{ $set: values },
					function(e)
					{
						if(e) errorAction(e);
						else submitAction();
					}
				);


				console.log("button1Type="+values.button1Type);
				console.log("button1Data="+values.button1Data);
				console.log("button1Label="+values.button1Label);



				Meteor.call("setMenu",
					values.fbPageID,

					values.button1Type,
					values.button1Data,
					values.button1Label,

					values.button2Type,
					values.button2Data,
					values.button2Label,

					values.button3Type,
					values.button3Data,
					values.button3Label,

					values.button4Type,
					values.button4Data,
					values.button3Label,

					values.button5Type,
					values.button5Data,
					values.button5Label,

					values.fbServer);

			}
		);

		return false;
	},
	"click #form-cancel-button": function(e, t) {
		e.preventDefault();



		history.back();
	},
	"click #form-close-button": function(e, t) {
		e.preventDefault();

		/*CLOSE_REDIRECT*/
	},
	"click #form-back-button": function(e, t) {
		e.preventDefault();

		/*BACK_REDIRECT*/
	},
	"click #form-menu-button": function(e, t) {
		e.preventDefault();

		history.back();
		/*BACK_REDIRECT*/
	}


});

Template.MenuEditEditForm.helpers({
	"infoMessage": function() {
		return pageSession.get("menuEditEditFormInfoMessage");
	},
	"errorMessage": function() {
		return pageSession.get("menuEditEditFormErrorMessage");
	},
	"campaignName": function () {
		var a=CmsMenu.findOne({_id:this.params.menuId}, {}).campaignName
		return a;

	},
	"campaignId": function () {
		return this.params.query.cid;

	}

});

