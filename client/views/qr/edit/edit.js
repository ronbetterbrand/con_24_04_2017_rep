var pageSession = new ReactiveDict();

Template.QREdit.rendered = function() {

};

Template.QREdit.events({

});

Template.QREdit.helpers({

});

Template.QREditEditForm.rendered = function() {


	pageSession.set("qrEditEditFormInfoMessage", "");
	pageSession.set("qrEditEditFormErrorMessage", "");

	$(".input-group.date").each(function() {
		var format = $(this).find("input[type='text']").attr("data-format");

		if(format) {
			format = format.toLowerCase();
		}
		else {
			format = "mm/dd/yyyy";
		}

		$(this).datepicker({
			autoclose: true,
			todayHighlight: true,
			todayBtn: true,
			forceParse: false,
			keyboardNavigation: false,
			format: format
		});
	});

	$("input[type='file']").fileinput();
	$("select[data-role='tagsinput']").tagsinput();
	$(".bootstrap-tagsinput").addClass("form-control");
	$("input[autofocus]").focus();
};

Template.QREditEditForm.events({
	"submit": function(e, t) {
		e.preventDefault();
		pageSession.set("qrEditEditFormInfoMessage", "");
		pageSession.set("qrEditEditFormErrorMessage", "");

		var self = this;

		function submitAction(msg) {
			var qrEditEditFormMode = "update";
			if(!t.find("#form-cancel-button")) {
				switch(qrEditEditFormMode) {
					case "insert": {
						$(e.target)[0].reset();
					}; break;

					case "update": {
						var message = msg || "Saved.";
						pageSession.set("qrEditEditFormInfoMessage", message);
					}; break;
				}
			}


			history.back();


		}

		function errorAction(msg) {
			msg = msg || "";
			var message = msg.message || msg || "Error.";
			pageSession.set("qrEditEditFormErrorMessage", message);
		}

		validateForm(
			$(e.target),
			function(fieldName, fieldValue) {

			},
			function(msg) {

			},
			function(values) {


				CmsQR.update(
					{ _id: t.data.cms_qr_details._id },
					{ $set: values },
					function(e)
					{
						if(e) errorAction(e);
						else submitAction();
					}
				);


				console.log("button1Type="+values.button1Type);
				console.log("button1Data="+values.button1Data);
				console.log("button1Label="+values.button1Label);



				Meteor.call("setQR",
					values.fbPageID,

					values.button1Type,
					values.button1Data,
					values.button1Label,

					values.button2Type,
					values.button2Data,
					values.button2Label,

					values.button3Type,
					values.button3Data,
					values.button3Label,

					values.button4Type,
					values.button4Data,
					values.button3Label,

					values.button5Type,
					values.button5Data,
					values.button5Label );

			}
		);

		return false;
	},
	"click #form-cancel-button": function(e, t) {
		e.preventDefault();



		history.back();
	},
	"click #form-close-button": function(e, t) {
		e.preventDefault();

		/*CLOSE_REDIRECT*/
	},
	"click #form-back-button": function(e, t) {
		e.preventDefault();

		/*BACK_REDIRECT*/
	},
	"click #form-qr-button": function(e, t) {
		e.preventDefault();

		history.back();
		/*BACK_REDIRECT*/
	}


});

Template.QREditEditForm.helpers({
	"infoMessage": function() {
		return pageSession.get("qrEditEditFormInfoMessage");
	},
	"errorMessage": function() {
		return pageSession.get("qrEditEditFormErrorMessage");
	},
	"campaignName": function() {
		var a=CmsQR.findOne({_id:this.params.qrId}, {}).campaignName
		return a;
	},
	"campaignId": function () {
		return this.params.query.cid;

	}

});

