this.QREditController = RouteController.extend({
	template: "QREdit",


	yieldTemplates: {
		/*YIELD_TEMPLATES*/
	},

	onBeforeAction: function() {
		this.next();
	},

	action: function() {
		if(this.isReady()) { this.render(); } else { this.render("loading"); }
		/*ACTION_FUNCTION*/
	},

	isReady: function() {


		var subs = [
			Meteor.subscribe("cms_customer_list"),
			Meteor.subscribe("cms_qr_details", this.params.qrId)
		];
		var ready = true;
		_.each(subs, function(sub) {
			if(!sub.ready())
				ready = false;
		});
		return ready;
	},

	data: function() {


		var data = {
			params: this.params || {},
			cms_customer_list: CmsCustomers.find({}, {}),
			cms_qr_details: CmsQR.findOne({_id:this.params.qrId}, {})
		};




		return data;
	},

	onAfterAction: function() {

	}
});