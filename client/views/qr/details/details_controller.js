this.QRDetailsController = RouteController.extend({
	template: "QRDetails",


	yieldTemplates: {
		/*YIELD_TEMPLATES*/
	},

	onBeforeAction: function() {
		this.next();
	},

	action: function() {
		Router.go("qr", {});
		///console.log("we don't have - sections.details.items");
		//this.redirect('sections.details', this.params || {}, { replaceState: true });
		//this.next();
		///???this.redirect('sections.details.items', this.params || {}, { replaceState: true });
		/*ACTION_FUNCTION*/
	},

	isReady: function() {


		var subs = [
			Meteor.subscribe("cms_qr_details", this.params.qrId)
		];
		var ready = true;
		_.each(subs, function(sub) {
			if(!sub.ready())
				ready = false;
		});
		return ready;
	},

	data: function() {


		var data = {
			params: this.params || {},
			cms_qr_details: CmsQR.findOne({_id:this.params.qrId}, {})
		};




		return data;
	},

	onAfterAction: function() {

	}
});