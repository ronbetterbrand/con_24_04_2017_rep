var pageSession = new ReactiveDict();

Template.QRDetails.rendered = function() {

};

Template.QRDetails.events({

});

Template.QRDetails.helpers({

});

Template.QRDetailsDetailsForm.rendered = function() {


	pageSession.set("QRDetailsDetailsFormInfoMessage", "");
	pageSession.set("QRDetailsDetailsFormErrorMessage", "");

	$(".input-group.date").each(function() {
		var format = $(this).find("input[type='text']").attr("data-format");

		if(format) {
			format = format.toLowerCase();
		}
		else {
			format = "mm/dd/yyyy";
		}

		$(this).datepicker({
			autoclose: true,
			todayHighlight: true,
			todayBtn: true,
			forceParse: false,
			keyboardNavigation: false,
			format: format
		});
	});

	$("input[type='file']").fileinput();
	$("select[data-role='tagsinput']").tagsinput();
	$(".bootstrap-tagsinput").addClass("form-control");
	$("input[autofocus]").focus();
};

Template.QRDetailsDetailsForm.events({
	"submit": function(e, t) {
		e.preventDefault();
		pageSession.set("QRDetailsDetailsFormInfoMessage", "");
		pageSession.set("QRDetailsDetailsFormErrorMessage", "");

		var self = this;

		function submitAction(msg) {
			var QRDetailsDetailsFormMode = "read_only";
			if(!t.find("#form-cancel-button")) {
				switch(QRDetailsDetailsFormMode) {
					case "insert": {
						$(e.target)[0].reset();
					}; break;

					case "update": {
						var message = msg || "Saved.";
						pageSession.set("QRDetailsDetailsFormInfoMessage", message);
					}; break;
				}
			}

			/*SUBMIT_REDIRECT*/
		}

		function errorAction(msg) {
			msg = msg || "";
			var message = msg.message || msg || "Error.";
			pageSession.set("QRDetailsDetailsFormErrorMessage", message);
		}

		validateForm(
			$(e.target),
			function(fieldName, fieldValue) {

			},
			function(msg) {

			},
			function(values) {



			}
		);

		return false;
	},
	"click #form-cancel-button": function(e, t) {
		e.preventDefault();



		/*CANCEL_REDIRECT*/
	},
	"click #form-close-button": function(e, t) {
		e.preventDefault();

		/*CLOSE_REDIRECT*/
	},
	"click #form-back-button": function(e, t) {
		e.preventDefault();

		Router.go("qr", {});
	}


});

Template.QRDetailsDetailsForm.helpers({
	"infoMessage": function() {
		return pageSession.get("QRDetailsDetailsFormInfoMessage");
	},
	"errorMessage": function() {
		return pageSession.get("QRDetailsDetailsFormErrorMessage");
	}

});
