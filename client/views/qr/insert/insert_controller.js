this.QRInsertController = RouteController.extend({
	template: "QRInsert",


	yieldTemplates: {
		/*YIELD_TEMPLATES*/
	},

	onBeforeAction: function() {
		this.next();
	},

	action: function() {
		if(this.isReady()) { this.render(); } else { this.render("loading"); }
		/*ACTION_FUNCTION*/
	},

	isReady: function() {


		var subs = [
			Meteor.subscribe("cms_customer_list"),
			Meteor.subscribe("cms_qr_empty"),
			Meteor.subscribe("cms_qr_list")
		];
		var ready = true;
		_.each(subs, function(sub) {
			if(!sub.ready())
				ready = false;
		});
		return ready;
	},

	data: function() {


		var data = {
			params: this.params || {},
			cms_customer_list: CmsCustomers.find({}, {}),
			cms_qr_empty: CmsQR.findOne({_id:null}, {}),
			cms_qr_list: CmsQR.find({}, {sort:[["qrName","desc"]]})

		};




		return data;
	},

	onAfterAction: function() {

	}
});