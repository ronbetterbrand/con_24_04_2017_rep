var pageSession = new ReactiveDict();

function getParameterByName(name) {
	name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
		results = regex.exec(location.search);
	return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

Template.QRInsert.rendered = function() {

};

Template.QRInsert.events({

});

Template.QRInsert.helpers({

});

Template.QRInsertInsertForm.rendered = function() {
	$('[data-toggle="tooltip"]').tooltip();

	//console.log("here...");

	pageSession.set("qrInsertInsertFormInfoMessage", "");
	pageSession.set("qrInsertInsertFormErrorMessage", "");

	$("input[type='file']").fileinput();
	$("select[data-role='tagsinput']").tagsinput();
	$(".bootstrap-tagsinput").addClass("form-control");
	$("input[autofocus]").focus();


};

Template.QRInsertInsertForm.events({
	"submit": function(e, t) {
		e.preventDefault();
		pageSession.set("qrInsertInsertFormInfoMessage", "");
		pageSession.set("qrInsertInsertFormErrorMessage", "");

		var self = this;
		var isApply = 0;//-1;

		function submitAction(msg) {
			var qrInsertInsertFormMode = "insert";
			if(!t.find("#form-cancel-button")) {
				switch(qrInsertInsertFormMode) {
					case "insert": {
						$(e.target)[0].reset();
					}; break;

					case "update": {
						var message = msg || "Saved.";
						pageSession.set("qrInsertInsertFormInfoMessage", message);
					}; break;
				}
			}

			//Router.go("menu.details", {menuId: newId});
		}

		function errorAction(msg) {
			msg = msg || "";
			var message = msg.message || msg || "Error.";
			pageSession.set("qrInsertInsertFormErrorMessage", message);
		}

		validateForm(
			$(e.target),
			function(fieldName, fieldValue) {
				//console.log("f="+fieldName+" v="+fieldValue);

				//var c = CmsMenu.findOne({"sectionName": fieldValue});


				//if( fieldName=="menuName"){
                //
				//	if(fieldValue.toString().length < 3){
				//		bootbox.alert("Menu name length >=3 ", function() {
				//			//Router.go("/menu?id="+"c2", {});
				//			return false;
				//		});
				//	}
				//	//else if( c.sectionName==fieldValue ){
				//	//	bootbox.alert("Please use a unique section name", function() {
				//	//		Router.go("menu", {});
				//	//		return;
				//	//	});
				//	//}
				//	else{
				//		isApply=0;
				//	}
                //
				//}

			},
			function(msg) {
				console.log("msg="+msg);
			},
			function(values) {

				if (isApply==0){

					var c = getParameterByName('id');
					console.log ("c="+ c);
					values.campaignName = c;

					newId = CmsQR.insert(values, function(e)
					{
						if(e) errorAction(e); else submitAction();
						history.back();
					});
				}

			}
		);

		return false;
	},
	"click #form-cancel-button": function(e, t) {
		e.preventDefault();



		Router.go("qr",  {_id: 1}, {query: this.params.query});
	},
	"click #form-close-button": function(e, t) {
		e.preventDefault();

		/*CLOSE_REDIRECT*/
	},
	"click #form-back-button": function(e, t) {
		e.preventDefault();

		/*BACK_REDIRECT*/
	}


});

Template.QRInsertInsertForm.helpers({
	"infoMessage": function() {
		return pageSession.get("qrInsertInsertFormInfoMessage");
	},
	"errorMessage": function() {
		return pageSession.get("qrInsertInsertFormErrorMessage");
	},
	"campaignName": function() {
		return getParameterByName('id');
	},
	"campaignId": function () {
		return getParameterByName('cid');

	}

});
