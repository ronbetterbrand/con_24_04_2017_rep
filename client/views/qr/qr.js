var pageSession = new ReactiveDict();
pageSession.set("showHelp", true);

Template.QRView.onRendered(function(){
	if(!pageSession.get("showHelp")) {
		$("#chatsuite-introdution").addClass("perhide");
	}
});
function getParameterByName(name) {
	name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
		results = regex.exec(location.search);
	return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

Template.QR.rendered = function() {
	
};

Template.QR.events({
	
});

Template.QR.helpers({
	
});

var QRViewItems = function(cursor) {
	if(!cursor) {
		return [];
	}

	var searchString = pageSession.get("QRViewSearchString");
	var sortBy = pageSession.get("QRViewSortBy");
	var sortAscending = pageSession.get("QRViewSortAscending");
	if(typeof(sortAscending) == "undefined") sortAscending = true;

	var raw = cursor.fetch();

	// filter
	var filtered = [];
	if(!searchString || searchString == "") {
		filtered = raw;
	} else {
		searchString = searchString.replace(".", "\\.");
		var regEx = new RegExp(searchString, "i");
		var searchFields = ["fbPageID","campaignName","careatedAt","modifiedAt"];
		filtered = _.filter(raw, function(item) {
			var match = false;
			_.each(searchFields, function(field) {
				var value = (getPropertyValue(field, item) || "") + "";

				match = match || (value && value.match(regEx));
				if(match) {
					return false;
				}
			})
			return match;
		});
	}

	// sort
	if(sortBy) {
		filtered = _.sortBy(filtered, sortBy);

		// descending?
		if(!sortAscending) {
			filtered = filtered.reverse();
		}
	}

	return filtered;
};

var QRViewExport = function(cursor, fileType) {
	var data = QRViewItems(cursor);
	var exportFields = ["cardID","cardType","qrName","campaignName", "date"];

	var str = convertArrayOfObjects(data, exportFields, fileType);

	var filename = "export." + fileType;

	downloadLocalResource(str, filename, "application/octet-stream");
}


Template.QRView.rendered = function() {
	pageSession.set("QRViewStyle", "table");
	
};

Template.QRView.events({
	"submit #dataview-controls": function(e, t) {
		return false;
	},

	"click #dataview-search-button": function(e, t) {
		e.preventDefault();
		var form = $(e.currentTarget).parent();
		if(form) {
			var searchInput = form.find("#dataview-search-input");
			if(searchInput) {
				searchInput.focus();
				var searchString = searchInput.val();
				pageSession.set("QRViewSearchString", searchString);
			}

		}
		return false;
	},

	"keydown #dataview-search-input": function(e, t) {
		if(e.which === 13)
		{
			e.preventDefault();
			var form = $(e.currentTarget).parent();
			if(form) {
				var searchInput = form.find("#dataview-search-input");
				if(searchInput) {
					var searchString = searchInput.val();
					pageSession.set("QRViewSearchString", searchString);
				}

			}
			return false;
		}

		if(e.which === 27)
		{
			e.preventDefault();
			var form = $(e.currentTarget).parent();
			if(form) {
				var searchInput = form.find("#dataview-search-input");
				if(searchInput) {
					searchInput.val("");
					pageSession.set("QRViewSearchString", "");
				}

			}
			return false;
		}

		return true;
	},

	"click #dataview-insert-button": function(e, t) {
		e.preventDefault();
		//var id = getParameterByName('id');
		//var query = "cn=" + this.params.query.id;
		//e.preventDefault();
		Router.go("qr.insert",  {_id: 1}, {query: this.params.query});
	},

	"click #dataview-export-default": function(e, t) {
		e.preventDefault();
		QRViewExport(this.cms_qr_list, "csv");
	},

	"click #dataview-export-csv": function(e, t) {
		e.preventDefault();
		QRViewExport(this.cms_qr_list, "csv");
	},

	"click #dataview-export-tsv": function(e, t) {
		e.preventDefault();
		QRViewExport(this.cms_qr_list, "tsv");
	},

	"click #dataview-export-json": function(e, t) {
		e.preventDefault();
		QRViewExport(this.cms_qr_list, "json");
	},
	"click .close": function(e, t) {

	pageSession.set("showHelp",false)
}

	
});

Template.QRView.helpers({

	"getID":function(){
		return getParameterByName('id');
	},
	"insertButtonClass": function() {
		return CmsQR.userCanInsert(Meteor.userId(), {}) ? "" : "hidden";
	},

	"isEmpty": function() {
		return !this.cms_qr_list || this.cms_qr_list.count() == 0;
	},
	"isNotEmpty": function() {
		return this.cms_qr_list && this.cms_qr_list.count() > 0;
	},
	"isNotFound": function() {
		return this.cms_qr_list && pageSession.get("QRViewSearchString") && QRViewItems(this.cms_qr_list).length == 0;
	},
	"searchString": function() {
		return pageSession.get("QRViewSearchString");
	},
	"viewAsTable": function() {
		return pageSession.get("QRViewStyle") == "table";
	},
	"viewAsList": function() {
		return pageSession.get("QRViewStyle") == "list";
	},
	"viewAsGallery": function() {
		return pageSession.get("QRViewStyle") == "gallery";
	},
	"campaignName": function () {
		return getParameterByName('id');

	},
	"campaignId": function () {
		return getParameterByName('cid');

	}



});


Template.QRViewTable.rendered = function() {
	
};

Template.QRViewTable.events({
	"click .th-sortable": function(e, t) {
		e.preventDefault();
		var oldSortBy = pageSession.get("QRViewSortBy");
		var newSortBy = $(e.target).attr("data-sort");

		pageSession.set("QRViewSortBy", newSortBy);
		if(oldSortBy == newSortBy) {
			var sortAscending = pageSession.get("QRViewSortAscending") || false;
			pageSession.set("QRViewSortAscending", !sortAscending);
		} else {
			pageSession.set("QRViewSortAscending", true);
		}
	}
});



Template.QRViewTable.helpers({
	"tableItems": function()
	{


		var id = getParameterByName('id');
		console.log("campaignName="+id);
return QRViewItems(CmsQR.find({ campaignName: id  }));
//this.cms_section_list);
	}
});


Template.QRViewTableItems.rendered = function() {
	
};

Template.QRViewTableItems.events({
	"click td": function(e, t) {
		e.preventDefault();
		
		Router.go("qr.details", {qrId: this._id});
		return false;
	},

	"click .inline-checkbox": function(e, t) {
		e.preventDefault();

		if(!this || !this._id) return false;

		var fieldName = $(e.currentTarget).attr("data-field");
		if(!fieldName) return false;

		var values = {};
		values[fieldName] = !this[fieldName];

		CmsQR.update({ _id: this._id }, { $set: values });

		return false;
	},

	"click #delete-button": function(e, t) {
		e.preventDefault();
		var me = this;
		bootbox.dialog({
			message: "Delete? Are you sure?",
			title: "Delete",
			animate: false,
			buttons: {
				success: {
					label: "Yes",
					className: "btn-success",
					callback: function() {
						CmsQR.remove({ _id: me._id });
					}
				},
				danger: {
					label: "No",
					className: "btn-default"
				}
			}
		});
		return false;
	},
	"click #edit-button": function(e, t) {
		e.preventDefault();
		Router.go("qr.edit", {qrId: this._id},{ query: UI._parentData(1).params.query});
		return false;
	}
});

Template.QRViewTableItems.helpers({
	"checked": function(value) { return value ? "checked" : "" }, 
	"editButtonClass": function() {
		return CmsQR.userCanUpdate(Meteor.userId(), this) ? "" : "hidden";
	},

	"deleteButtonClass": function() {
		return CmsQR.userCanRemove(Meteor.userId(), this) ? "" : "hidden";
	}
});
