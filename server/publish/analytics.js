Meteor.publish("analytics", function() {
    return Analytics.find({});
});

Meteor.publish("analytics_empty", function() {
    return Analytics.find({_id:null});
});

Meteor.publish("analytics", function(analyticsId) {
    return Analytics.find({_id:analyticsId});
});

Meteor.publish("analytics_base", function() {
    return Analytics.find();
});

Meteor.publish("analytics_single", function() {
    return Analytics.find();
});

