Meteor.publish("cms_customer_list", function() {
	return CmsCustomers.find({ownerId:this.userId}, {});
});

Meteor.publish("cms_customers_empty", function() {
	return CmsCustomers.find({_id:null,ownerId:this.userId}, {});
});

Meteor.publish("cms_customer_details", function(customerId) {
	return CmsCustomers.find({_id:customerId,ownerId:this.userId}, {});
});

