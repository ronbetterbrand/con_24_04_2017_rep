Meteor.publish("temp_keywords", function() {
    return Keywords.find({});
});

Meteor.publish("temp_keywords_empty", function() {
    return Keywords.find({_id:null});
});

Meteor.publish("temp_keywords", function(keywordId) {
    return Keywords.find({_id:keywordId});
});

Meteor.publish("temp_keywords_base", function() {
    return Keywords.find();
});

Meteor.publish("temp_keywords_single", function() {
    return Keywords.find();
});

