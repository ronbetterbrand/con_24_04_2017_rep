Meteor.publish("imuser", function() {
    return Imuser.find({});
});

Meteor.publish("imuser_empty", function() {
    return Imuser.find({_id:null});
});

Meteor.publish("imuser", function(imuserId) {
    return Imuser.find({_id:imuserId});
});

Meteor.publish("imuser_base", function() {
    return Imuser.find();
});

Meteor.publish("imuser_single", function() {
    return Imuser.find();
});

