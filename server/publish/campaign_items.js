Meteor.publish("cms_campaign_items", function(campaignId) {
	return CmsCampaignItems.find({campaignId:campaignId,ownerId:this.userId}, {});
});

Meteor.publish("cms_campaign_items_empty", function() {
	return CmsCampaignItems.find({_id:null,ownerId:this.userId}, {});
});

Meteor.publish("cms_campaign_item", function(itemId) {
	return CmsCampaignItems.find({_id:itemId,ownerId:this.userId}, {});
});

