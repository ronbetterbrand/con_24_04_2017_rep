Meteor.publish("cms_menu_list", function() {
    return CmsMenu.publishJoinedCursors(CmsMenu.find({ownerId:this.userId}, {sort:[["menuName","desc"]]}));
});

Meteor.publish("cms_menu_empty", function() {
    return CmsMenu.publishJoinedCursors(CmsMenu.find({_id:null,ownerId:this.userId}, {}));
});

Meteor.publish("cms_menu_details", function(menuId) {
    return CmsMenu.publishJoinedCursors(CmsMenu.find({_id:menuId,ownerId:this.userId}, {}));
});

