Meteor.publish("cms_campaign_list", function() {
	return CmsCampaigns.publishJoinedCursors(CmsCampaigns.find({ownerId:this.userId}, {sort:[["campaignName","desc"]]}));
});

Meteor.publish("cms_campaigns_empty", function() {
	return CmsCampaigns.publishJoinedCursors(CmsCampaigns.find({_id:null,ownerId:this.userId}, {}));
});

Meteor.publish("cms_campaign_details", function(campaignId) {
	return CmsCampaigns.publishJoinedCursors(CmsCampaigns.find({_id:campaignId,ownerId:this.userId}, {}));
});

