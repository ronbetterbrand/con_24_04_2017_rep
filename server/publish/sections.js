Meteor.publish("cms_section_list", function() {
    return CmsSections.publishJoinedCursors(CmsSections.find({ownerId:this.userId}, {sort:[["sectionName","desc"]]}));
});

Meteor.publish("cms_sections_empty", function() {
    return CmsSections.publishJoinedCursors(CmsSections.find({_id:null,ownerId:this.userId}, {}));
});

Meteor.publish("cms_section_details", function(sectionId) {
    return CmsSections.publishJoinedCursors(CmsSections.find({_id:sectionId,ownerId:this.userId}, {}));
});

