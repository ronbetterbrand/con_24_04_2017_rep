Meteor.publish("cms_qr_list", function() {
    return CmsQR.publishJoinedCursors(CmsQR.find({ownerId:this.userId}, {sort:[["qrName","desc"]]}));
});

Meteor.publish("cms_qr_empty", function() {
    return CmsQR.publishJoinedCursors(CmsQR.find({_id:null,ownerId:this.userId}, {}));
});

Meteor.publish("cms_qr_details", function(qrId) {
    return CmsQR.publishJoinedCursors(CmsQR.find({_id:qrId,ownerId:this.userId}, {}));
});

