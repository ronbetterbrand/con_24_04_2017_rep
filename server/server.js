var verifyEmail = false;

Accounts.config({ sendVerificationEmail: verifyEmail });

Meteor.startup(function() {
   activateAnalytics();

	// read environment variables from Meteor.settings
	if(Meteor.settings && Meteor.settings.env && _.isObject(Meteor.settings.env)) {
		for(var variableName in Meteor.settings.env) {
			process.env[variableName] = Meteor.settings.env[variableName];
		}
	}

	//Campaings = new Meteor.Collection('campaigns');
    //
	//Router.route('/campaign',{where: 'server'})
	//	.get(function(){
	//		var response = Messages.find().fetch();
	//		this.response.setHeader('Content-Type','application/json');
	//		this.response.end(JSON.stringify(response));
	//	})

	//Router.route( "campaigns/:name", function() {
	//	var name   = this.params.name,
	//	//	query  = this.request.query,
	//	//	fields = {};
     //   //
	//	//fields[ query.field ] = query.field;
    //
	//	var getUser = Meteor.campaigns.findOne( { "campaign.name": name }, { fields: name } );
    //
	//	this.response.statusCode = 200;
	//	this.response.end( campaign.name );
    //
	//}, { where: "server" });


	//Router.route( "/campaigns/:name/", { where: "server" } )
	//	.get( function() {
	//		// If a GET request is made, return the user's profile.
	//	})
	//	.post( function() {
	//		// If a POST request is made, create the user's profile.
	//	});

	//Router.route('/campaign/:id',{where: 'server'})
    //
	//	// GET /message/:id - returns specific records
    //
	//	.get(function(){
	//		var response;
	//		if(this.params.id !== undefined) {
	//			var data = Campaings.find({"_id": this.params.id}).fetch();
	//			//var c = Campaigns.findOne( {"_id": t.data.campaign._id } );
    //
	//			//if(data.length &gt; 0) {
	//				response = data;
	//			//} else {
	//			//	response = {
	//			//		"error" : true,
	//			//		"message" : "Record not found."
	//			//	}
	//			//}
	//		}
	//		this.response.setHeader('Content-Type','application/json');
	//		this.response.end(JSON.stringify(response));
	//	})


	//UploadServer.init({
	//	tmpDir: process.env.PWD + '/.uploads/tmp',
	//	uploadDir: process.env.PWD + '/.uploads/'
    //
    //
	//})

	//UploadServer.init({
	//	tmpDir: process.env.PWD + '../public/uploads/tmp',
	//	uploadDir: process.env.PWD + '../public/uploads/'
    //
    //
	//})


	//console.log("Dir... "+process.env.PWD);

	/*UploadServer.init({
	 tmpDir:   './Documents/uploads/tmp',
	 uploadDir:   './Documents/uploads/'
	})*/



	//
	// Setup OAuth login service configuration (read from Meteor.settings)
	//
	// Your settings file should look like this:
	//
	// {
	//     "oauth": {
	//         "google": {
	//             "clientId": "yourClientId",
	//             "secret": "yourSecret"
	//         },
	//         "github": {
	//             "clientId": "yourClientId",
	//             "secret": "yourSecret"
	//         }
	//     }
	// }
	//
	if(Accounts && Accounts.loginServiceConfiguration && Meteor.settings && Meteor.settings.oauth && _.isObject(Meteor.settings.oauth)) {
		// google
		if(Meteor.settings.oauth.google && _.isObject(Meteor.settings.oauth.google)) {
			// remove old configuration
			Accounts.loginServiceConfiguration.remove({
				service: "google"
			});

			var settingsObject = Meteor.settings.oauth.google;
			settingsObject.service = "google";

			// add new configuration
			Accounts.loginServiceConfiguration.insert(settingsObject);
		}
		// github
		if(Meteor.settings.oauth.github && _.isObject(Meteor.settings.oauth.github)) {
			// remove old configuration
			Accounts.loginServiceConfiguration.remove({
				service: "github"
			});

			var settingsObject = Meteor.settings.oauth.github;
			settingsObject.service = "github";

			// add new configuration
			Accounts.loginServiceConfiguration.insert(settingsObject);
		}
		// linkedin
		if(Meteor.settings.oauth.linkedin && _.isObject(Meteor.settings.oauth.linkedin)) {
			// remove old configuration
			Accounts.loginServiceConfiguration.remove({
				service: "linkedin"
			});

			var settingsObject = Meteor.settings.oauth.linkedin;
			settingsObject.service = "linkedin";

			// add new configuration
			Accounts.loginServiceConfiguration.insert(settingsObject);
		}
		// facebook
		if(Meteor.settings.oauth.facebook && _.isObject(Meteor.settings.oauth.facebook)) {
			// remove old configuration
			Accounts.loginServiceConfiguration.remove({
				service: "facebook"
			});

			var settingsObject = Meteor.settings.oauth.facebook;
			settingsObject.service = "facebook";

			// add new configuration
			Accounts.loginServiceConfiguration.insert(settingsObject);
		}
		// twitter
		if(Meteor.settings.oauth.twitter && _.isObject(Meteor.settings.oauth.twitter)) {
			// remove old configuration
			Accounts.loginServiceConfiguration.remove({
				service: "twitter"
			});

			var settingsObject = Meteor.settings.oauth.twitter;
			settingsObject.service = "twitter";

			// add new configuration
			Accounts.loginServiceConfiguration.insert(settingsObject);
		}
		// meteor
		if(Meteor.settings.oauth.meteor && _.isObject(Meteor.settings.oauth.meteor)) {
			// remove old configuration
			Accounts.loginServiceConfiguration.remove({
				service: "meteor-developer"
			});

			var settingsObject = Meteor.settings.oauth.meteor;
			settingsObject.service = "meteor-developer";

			// add new configuration
			Accounts.loginServiceConfiguration.insert(settingsObject);
		}
	}

	
});

Meteor.methods({
	"createUserAccount": function(options) {
		if(!Users.isAdmin(Meteor.userId())) {
			throw new Meteor.Error(403, "Access denied.");
		}

		var userOptions = {};
		if(options.username) userOptions.username = options.username;
		if(options.email) userOptions.email = options.email;
		if(options.password) userOptions.password = options.password;
		if(options.profile) userOptions.profile = options.profile;
		if(options.profile && options.profile.email) userOptions.email = options.profile.email;

		Accounts.createUser(userOptions);
	},
	"updateUserAccount": function(userId, options) {
		// only admin or users own profile
		if(!(Users.isAdmin(Meteor.userId()) || userId == Meteor.userId())) {
			throw new Meteor.Error(403, "Access denied.");
		}

		// non-admin user can change only profile
		if(!Users.isAdmin(Meteor.userId())) {
			var keys = Object.keys(options);
			if(keys.length !== 1 || !options.profile) {
				throw new Meteor.Error(403, "Access denied.");
			}
		}

		var userOptions = {};
		if(options.username) userOptions.username = options.username;
		if(options.email) userOptions.email = options.email;
		if(options.password) userOptions.password = options.password;
		if(options.profile) userOptions.profile = options.profile;

		if(options.profile && options.profile.email) userOptions.email = options.profile.email;
		if(options.roles) userOptions.roles = options.roles;

		if(userOptions.email) {
			var email = userOptions.email;
			delete userOptions.email;
			userOptions.emails = [{ address: email }];
		}

		var password = "";
		if(userOptions.password) {
			password = userOptions.password;
			delete userOptions.password;
		}

		if(userOptions) {
			Users.update(userId, { $set: userOptions });
		}

		if(password) {
			Accounts.setPassword(userId, password);
		}
	},

	"sendMail": function(options) {
		this.unblock();

		Email.send(options);
	},
	download: function() {
		var collection = Tracks.find().fetch();
		var heading = true; // Optional, defaults to true
		var delimiter = ";" // Optional, defaults to ",";
		return exportcsv.exportToCSV(collection, heading, delimiter);
	}
});

Accounts.onCreateUser(function (options, user) {
	user.roles = [];

	if(options.profile) {
		user.profile = options.profile;
	}

	
	return user;
});

Accounts.validateLoginAttempt(function(info) {

	// reject users with role "blocked"
	if(info.user && Users.isInRole(info.user._id, "blocked")) {
		throw new Meteor.Error(403, "Your account is blocked.");
	}

  if(verifyEmail && info.user && info.user.emails && info.user.emails.length && !info.user.emails[0].verified ) {
			throw new Meteor.Error(499, "E-mail not verified.");
  }

	return true;
});


Users.before.insert(function(userId, doc) {
	if(doc.emails && doc.emails[0] && doc.emails[0].address) {
		doc.profile = doc.profile || {};
		doc.profile.email = doc.emails[0].address;
	} else {
		// oauth
		if(doc.services) {
			// google e-mail
			if(doc.services.google && doc.services.google.email) {
				doc.profile = doc.profile || {};
				doc.profile.email = doc.services.google.email;
			} else {
				// github e-mail
				if(doc.services.github && doc.services.github.accessToken) {
					var github = new GitHub({
						version: "3.0.0",
						timeout: 5000
					});

					github.authenticate({
						type: "oauth",
						token: doc.services.github.accessToken
					});

					try {
						var result = github.user.getEmails({});
						var email = _.findWhere(result, { primary: true });
						if(!email && result.length && _.isString(result[0])) {
							email = { email: result[0] };
						}

						if(email) {
							doc.profile = doc.profile || {};
							doc.profile.email = email.email;
						}
					} catch(e) {
						console.log(e);
					}
				} else {
					// linkedin email
					if(doc.services.linkedin && doc.services.linkedin.emailAddress) {
						doc.profile = doc.profile || {};
						doc.profile.name = doc.services.linkedin.firstName + " " + doc.services.linkedin.lastName;
						doc.profile.email = doc.services.linkedin.emailAddress;
					} else {
						if(doc.services.facebook && doc.services.facebook.email) {
							doc.profile = doc.profile || {};
							doc.profile.email = doc.services.facebook.email;
						} else {
							if(doc.services.twitter && doc.services.twitter.email) {
								doc.profile = doc.profile || {};
								doc.profile.email = doc.services.twitter.email;
							} else {
								if(doc.services["meteor-developer"] && doc.services["meteor-developer"].emails && doc.services["meteor-developer"].emails.length) {
									doc.profile = doc.profile || {};
									doc.profile.email = doc.services["meteor-developer"].emails[0].address;
								}
							}
						}
					}
				}
			}
		}
	}
});

Users.before.update(function(userId, doc, fieldNames, modifier, options) {
	if(modifier.$set && modifier.$set.emails && modifier.$set.emails.length && modifier.$set.emails[0].address) {
		modifier.$set.profile.email = modifier.$set.emails[0].address;
	}
});

Accounts.onLogin(function (info) {

});

Accounts.urls.resetPassword = function (token) {
	return Meteor.absoluteUrl('reset_password/' + token);
};

Accounts.urls.verifyEmail = function (token) {
	return Meteor.absoluteUrl('verify_email/' + token);
};

function activateAnalytics() {
    updateAnalytics();
    var everyMinute = new Cron(function() {
        updateAnalytics();
    }, {minute: 30});
}

function updateAnalytics() {
    var campaignNames = Campaigns.find({}, {}).fetch().map(function (x) {
        return x.name;
    });
    campaignNames = removeDuplicates(campaignNames);
    for(var i = 0; i < campaignNames.length; i++) {
        var arr = Analytics.find({campaignName: campaignNames[i]}, {limit: 1}).fetch().map(function (x) {
            return x.uniqueUsersViber;
        });
        var previousUniqueUsersViber = arr[0];
        if (previousUniqueUsersViber == undefined)
            previousUniqueUsersViber = 0;
        Analytics.remove({'campaignName': campaignNames[i]});
        Analytics.insert({'campaignName': campaignNames[i]});
        updateAnalyticsForCampaign(campaignNames[i], previousUniqueUsersViber);
    }
}

function removeDuplicates(arr){
    var resultArr = [];
    for(var i = 0; i < arr.length; i++){
        if(resultArr.indexOf(arr[i]) == -1){
            resultArr.push(arr[i]);
        }
    }
    return resultArr;
}


function updateAnalyticsForCampaign(campaignName, previousUniqueUsersViber) {
    var times = setTime("Today");
    var viewsDay = Views(times, campaignName);
    times = setTime("Yesterday");
    var viewsPreviousDay = Views(times, campaignName);
    times = setTime("ThisWeek");
    var viewsWeek = Views(times, campaignName);
    times = setTime("LastWeek");
    var viewsPreviousWeek = Views(times, campaignName);
    times = setTime("ThisMonth");
    var viewsMonth = Views(times, campaignName);
    times = setTime("LastMonth");
    var viewsPreviousMonth = Views(times, campaignName);
    times = setTime("All");
    var views = Views(times, campaignName);
    times = setTime("Today");
    var interactionsDay = Interactions(times, campaignName);
    times = setTime("Yesterday");
    var interactionsPreviousDay = Interactions(times, campaignName);
    times = setTime("ThisWeek");
    var interactionsWeek = Interactions(times, campaignName);
    times = setTime("LastWeek");
    var interactionsPreviousWeek = Interactions(times, campaignName);
    times = setTime("ThisMonth");
    var interactionsMonth = Interactions(times, campaignName);
    times = setTime("LastMonth");
    var interactionsPreviousMonth = Interactions(times, campaignName);
    times = setTime("All");
    var interactions = Interactions(times, campaignName);
    times = setTime("Today");
    var uniqueUsersDay = UniqueUsers(times, campaignName);
    times = setTime("Yesterday");
    var uniqueUsersPreviousDay = UniqueUsers(times, campaignName);
    times = setTime("ThisWeek");
    var uniqueUsersWeek = UniqueUsers(times, campaignName);
    times = setTime("LastWeek");
    var uniqueUsersPreviousWeek = UniqueUsers(times, campaignName);
    times = setTime("ThisMonth");
    var uniqueUsersMonth = UniqueUsers(times, campaignName);
    times = setTime("LastMonth");
    var uniqueUsersPreviousMonth = UniqueUsers(times, campaignName);
    times = setTime("All");
    var uniqueUsers = UniqueUsers(times, campaignName);
    times = setTime("Today");
    var repeatedUsersDay = RepeatedUsers(times, campaignName);
    times = setTime("Yesterday");
    var repeatedUsersPreviousDay = RepeatedUsers(times, campaignName);
    times = setTime("ThisWeek");
    var repeatedUsersWeek = RepeatedUsers(times, campaignName);
    times = setTime("LastWeek");
    var repeatedUsersPreviousWeek = RepeatedUsers(times, campaignName);
    times = setTime("ThisMonth");
    var repeatedUsersMonth = RepeatedUsers(times, campaignName);
    times = setTime("LastMonth");
    var repeatedUsersPreviousMonth = RepeatedUsers(times, campaignName);
    times = setTime("All");
    var repeatedUsers = RepeatedUsers(times, campaignName);
    times = setTime("Today");
    var timeSpentPerUserDay = TimeSpentPerUser(times, campaignName);
    times = setTime("Yesterday");
    var timeSpentPerUserPreviousDay = TimeSpentPerUser(times, campaignName);
    times = setTime("ThisWeek");
    var timeSpentPerUserWeek = TimeSpentPerUser(times, campaignName);
    times = setTime("LastWeek");
    var timeSpentPerUserPreviousWeek = TimeSpentPerUser(times, campaignName);
    times = setTime("ThisMonth");
    var timeSpentPerUserMonth = TimeSpentPerUser(times, campaignName);
    times = setTime("LastMonth");
    var timeSpentPerUserPreviousMonth = TimeSpentPerUser(times, campaignName);
    times = setTime("All");
    var timeSpentPerUser = TimeSpentPerUser(times, campaignName);
    times = setTime("Today");
    var repeatUsePerUserDay = RepeatUsePerUser(times, campaignName);
    times = setTime("Yesterday");
    var repeatUsePerUserPreviousDay = RepeatUsePerUser(times, campaignName);
    times = setTime("ThisWeek");
    var repeatUsePerUserWeek = RepeatUsePerUser(times, campaignName);
    times = setTime("LastWeek");
    var repeatUsePerUserPreviousWeek = RepeatUsePerUser(times, campaignName);
    times = setTime("ThisMonth");
    var repeatUsePerUserMonth = RepeatUsePerUser(times, campaignName);
    times = setTime("LastMonth");
    var repeatUsePerUserPreviousMonth = RepeatUsePerUser(times, campaignName);
    times = setTime("All");
    var repeatUsePerUser = RepeatUsePerUser(times, campaignName);
    times = setTime("Today");
    var malesDay = Males(times, campaignName);
    times = setTime("ThisWeek");
    var malesWeek = Males(times, campaignName);
    times = setTime("ThisMonth");
    var malesMonth = Males(times, campaignName);
    times = setTime("Today");
    var femalesDay = Females(times, campaignName);
    times = setTime("ThisWeek");
    var femalesWeek = Females(times, campaignName);
    times = setTime("ThisMonth");
    var femalesMonth = Females(times, campaignName);
    times = setTime("Today");
    var countriesArrayDay = Countries(times, campaignName);
    times = setTime("ThisWeek");
    var countriesArrayWeek = Countries(times, campaignName);
    times = setTime("ThisMonth");
    var countriesArrayMonth = Countries(times, campaignName);
    var viewsArrayDay = DataArray("Views","Day", campaignName);
    var viewsArrayWeek = DataArray("Views","Week", campaignName);
    var viewsArrayMonth = DataArray("Views","Month", campaignName);
    var interactionsArrayDay = DataArray("Interactions","Day", campaignName);
    var interactionsArrayWeek = DataArray("Interactions","Week", campaignName);
    var interactionsArrayMonth = DataArray("Interactions","Month", campaignName);
    var uniqueUsersArrayDay = DataArray("UniqueUsers","Day", campaignName);
    var uniqueUsersArrayWeek = DataArray("UniqueUsers","Week", campaignName);
    var uniqueUsersArrayMonth = DataArray("UniqueUsers","Month", campaignName);
    var repeatedUsersArrayDay = DataArray("RepeatedUsers","Day", campaignName);
    var repeatedUsersArrayWeek = DataArray("RepeatedUsers","Week", campaignName);
    var repeatedUsersArrayMonth = DataArray("RepeatedUsers","Month", campaignName);
    var timeSpentPerUserArrayDay = DataArray("TimeSpentPerUser","Day", campaignName);
    var timeSpentPerUserArrayWeek = DataArray("TimeSpentPerUser","Week", campaignName);
    var timeSpentPerUserArrayMonth = DataArray("TimeSpentPerUser","Month", campaignName);
    var repeatUsePerUserArrayDay = DataArray("RepeatUsePerUser","Day", campaignName);
    var repeatUsePerUserArrayWeek = DataArray("RepeatUsePerUser","Week", campaignName);
    var repeatUsePerUserArrayMonth = DataArray("RepeatUsePerUser","Month", campaignName);

    var times = setTime("Today");
    var viewsViberDay = ViewsViber(times, campaignName);
    times = setTime("Yesterday");
    var viewsViberPreviousDay = ViewsViber(times, campaignName);
    times = setTime("ThisWeek");
    var viewsViberWeek = ViewsViber(times, campaignName);
    times = setTime("LastWeek");
    var viewsViberPreviousWeek = ViewsViber(times, campaignName);
    times = setTime("ThisMonth");
    var viewsViberMonth = ViewsViber(times, campaignName);
    times = setTime("LastMonth");
    var viewsViberPreviousMonth = ViewsViber(times, campaignName);
    times = setTime("All");
    var viewsViber = Views(times, campaignName);
    times = setTime("Today");
    var interactionsViberDay = InteractionsViber(times, campaignName);
    times = setTime("Yesterday");
    var interactionsViberPreviousDay = InteractionsViber(times, campaignName);
    times = setTime("ThisWeek");
    var interactionsViberWeek = InteractionsViber(times, campaignName);
    times = setTime("LastWeek");
    var interactionsViberPreviousWeek = InteractionsViber(times, campaignName);
    times = setTime("ThisMonth");
    var interactionsViberMonth = InteractionsViber(times, campaignName);
    times = setTime("LastMonth");
    var interactionsViberPreviousMonth = InteractionsViber(times, campaignName);
    times = setTime("All");
    var interactionsViber = InteractionsViber(times, campaignName);
    times = setTime("Today");
    var uniqueUsersViberDay = UniqueUsersViber(times, campaignName);
    times = setTime("Yesterday");
    var uniqueUsersViberPreviousDay = UniqueUsersViber(times, campaignName);
    times = setTime("ThisWeek");
    var uniqueUsersViberWeek = UniqueUsersViber(times, campaignName);
    times = setTime("LastWeek");
    var uniqueUsersViberPreviousWeek = UniqueUsersViber(times, campaignName);
    times = setTime("ThisMonth");
    var uniqueUsersViberMonth = UniqueUsersViber(times, campaignName);
    times = setTime("LastMonth");
    var uniqueUsersViberPreviousMonth = UniqueUsersViber(times, campaignName);
    times = setTime("All");
    var uniqueUsersViber = previousUniqueUsersViber;
    times = setTime("Today");
    var repeatedUsersViberDay = RepeatedUsersViber(times, campaignName);
    times = setTime("Yesterday");
    var repeatedUsersViberPreviousDay = RepeatedUsersViber(times, campaignName);
    times = setTime("ThisWeek");
    var repeatedUsersViberWeek = RepeatedUsersViber(times, campaignName);
    times = setTime("LastWeek");
    var repeatedUsersViberPreviousWeek = RepeatedUsersViber(times, campaignName);
    times = setTime("ThisMonth");
    var repeatedUsersViberMonth = RepeatedUsersViber(times, campaignName);
    times = setTime("LastMonth");
    var repeatedUsersViberPreviousMonth = RepeatedUsersViber(times, campaignName);
    times = setTime("All");
    var repeatedUsersViber = RepeatedUsersViber(times, campaignName);
    times = setTime("Today");
    var timeSpentPerUserViberDay = TimeSpentPerUserViber(times, campaignName);
    times = setTime("Yesterday");
    var timeSpentPerUserViberPreviousDay = TimeSpentPerUserViber(times, campaignName);
    times = setTime("ThisWeek");
    var timeSpentPerUserViberWeek = TimeSpentPerUserViber(times, campaignName);
    times = setTime("LastWeek");
    var timeSpentPerUserViberPreviousWeek = TimeSpentPerUserViber(times, campaignName);
    times = setTime("ThisMonth");
    var timeSpentPerUserViberMonth = TimeSpentPerUserViber(times, campaignName);
    times = setTime("LastMonth");
    var timeSpentPerUserViberPreviousMonth = TimeSpentPerUserViber(times, campaignName);
    times = setTime("All");
    var timeSpentPerUserViber = TimeSpentPerUserViber(times, campaignName);
    times = setTime("Today");
    var repeatUsePerUserViberDay = RepeatUsePerUserViber(times, campaignName);
    times = setTime("Yesterday");
    var repeatUsePerUserViberPreviousDay = RepeatUsePerUserViber(times, campaignName);
    times = setTime("ThisWeek");
    var repeatUsePerUserViberWeek = RepeatUsePerUserViber(times, campaignName);
    times = setTime("LastWeek");
    var repeatUsePerUserViberPreviousWeek = RepeatUsePerUserViber(times, campaignName);
    times = setTime("ThisMonth");
    var repeatUsePerUserViberMonth = RepeatUsePerUserViber(times, campaignName);
    times = setTime("LastMonth");
    var repeatUsePerUserViberPreviousMonth = RepeatUsePerUserViber(times, campaignName);
    times = setTime("All");
    var repeatUsePerUserViber = RepeatUsePerUserViber(times, campaignName);
    var viewsArrayViberDay = DataArray("ViewsViber","Day", campaignName);
    var viewsArrayViberWeek = DataArray("ViewsViber","Week", campaignName);
    var viewsArrayViberMonth = DataArray("ViewsViber","Month", campaignName);
    var interactionsArrayViberDay = DataArray("InteractionsViber","Day", campaignName);
    var interactionsArrayViberWeek = DataArray("InteractionsViber","Week", campaignName);
    var interactionsArrayViberMonth = DataArray("InteractionsViber","Month", campaignName);
    var uniqueUsersArrayViberDay = DataArray("UniqueUsersViber","Day", campaignName);
    var uniqueUsersArrayViberWeek = DataArray("UniqueUsersViber","Week", campaignName);
    var uniqueUsersArrayViberMonth = DataArray("UniqueUsersViber","Month", campaignName);
    var repeatedUsersArrayViberDay = DataArray("RepeatedUsersViber","Day", campaignName);
    var repeatedUsersArrayViberWeek = DataArray("RepeatedUsersViber","Week", campaignName);
    var repeatedUsersArrayViberMonth = DataArray("RepeatedUsersViber","Month", campaignName);
    var timeSpentPerUserArrayViberDay = DataArray("TimeSpentPerUserViber","Day", campaignName);
    var timeSpentPerUserArrayViberWeek = DataArray("TimeSpentPerUserViber","Week", campaignName);
    var timeSpentPerUserArrayViberMonth = DataArray("TimeSpentPerUserViber","Month", campaignName);
    var repeatUsePerUserArrayViberDay = DataArray("RepeatUsePerUserViber","Day", campaignName);
    var repeatUsePerUserArrayViberWeek = DataArray("RepeatUsePerUserViber","Week", campaignName);
    var repeatUsePerUserArrayViberMonth = DataArray("RepeatUsePerUserViber","Month", campaignName);

    Analytics.update({'campaignName':campaignName}, {$set:{'viewsDay':viewsDay,'viewsWeek':viewsWeek,'viewsMonth':viewsMonth,'views':views,
        'viewsPreviousDay':viewsPreviousDay,'viewsPreviousWeek':viewsPreviousWeek,'viewsPreviousMonth':viewsPreviousMonth,
        'interactionsDay':interactionsDay,'interactionsWeek':interactionsWeek,'interactionsMonth':interactionsMonth,'interactions':interactions,
        'interactionsPreviousDay':interactionsPreviousDay,'interactionsPreviousWeek':interactionsPreviousWeek,'interactionsPreviousMonth':interactionsPreviousMonth,
        'uniqueUsersDay':uniqueUsersDay,'uniqueUsersWeek':uniqueUsersWeek,'uniqueUsersMonth':uniqueUsersMonth,'uniqueUsers':uniqueUsers,
        'uniqueUsersPreviousDay':uniqueUsersPreviousDay,'uniqueUsersPreviousWeek':uniqueUsersPreviousWeek,'uniqueUsersPreviousMonth':uniqueUsersPreviousMonth,
        'repeatedUsersDay':repeatedUsersDay,'repeatedUsersWeek':repeatedUsersWeek,'repeatedUsersMonth':repeatedUsersMonth,'repeatedUsers':repeatedUsers,
        'repeatedUsersPreviousDay':repeatedUsersPreviousDay,'repeatedUsersPreviousWeek':repeatedUsersPreviousWeek,'repeatedUsersPreviousMonth':repeatedUsersPreviousMonth,
        'timeSpentPerUserDay':timeSpentPerUserDay,'timeSpentPerUserWeek':timeSpentPerUserWeek,'timeSpentPerUserMonth':timeSpentPerUserMonth,'timeSpentPerUser':timeSpentPerUser,
        'timeSpentPerUserPreviousDay':timeSpentPerUserPreviousDay,'timeSpentPerUserPreviousWeek':timeSpentPerUserPreviousWeek,'timeSpentPerUserPreviousMonth':timeSpentPerUserPreviousMonth,
        'repeatUsePerUserDay':repeatUsePerUserDay,'repeatUsePerUserWeek':repeatUsePerUserWeek,'repeatUsePerUserMonth':repeatUsePerUserMonth,'repeatUsePerUser':repeatUsePerUser,
        'repeatUsePerUserPreviousDay':repeatUsePerUserPreviousDay,'repeatUsePerUserPreviousWeek':repeatUsePerUserPreviousWeek,'repeatUsePerUserPreviousMonth':repeatUsePerUserPreviousMonth,
        'viewsArrayDay':viewsArrayDay,'viewsArrayWeek':viewsArrayWeek,'viewsArrayMonth':viewsArrayMonth,
        'interactionsArrayDay':interactionsArrayDay,'interactionsArrayWeek':interactionsArrayWeek,'interactionsArrayMonth':interactionsArrayMonth,
        'uniqueUsersArrayDay':uniqueUsersArrayDay,'uniqueUsersArrayWeek':uniqueUsersArrayWeek,'uniqueUsersArrayMonth':uniqueUsersArrayMonth,
        'repeatedUsersArrayDay':repeatedUsersArrayDay,'repeatedUsersArrayWeek':repeatedUsersArrayWeek,'repeatedUsersArrayMonth':repeatedUsersArrayMonth,
        'timeSpentPerUserArrayDay':timeSpentPerUserArrayDay,'timeSpentPerUserArrayWeek':timeSpentPerUserArrayWeek,'timeSpentPerUserArrayMonth':timeSpentPerUserArrayMonth,
        'repeatUsePerUserArrayDay':repeatUsePerUserArrayDay,'repeatUsePerUserArrayWeek':repeatUsePerUserArrayWeek,'repeatUsePerUserArrayMonth':repeatUsePerUserArrayMonth,
        'malesDay':malesDay,'malesWeek':malesWeek,'malesMonth':malesMonth,'femalesDay':femalesDay,'femalesWeek':femalesWeek,'femalesMonth':femalesMonth,
        'countriesArrayDay':countriesArrayDay,'countriesArrayWeek':countriesArrayWeek,'countriesArrayMonth':countriesArrayMonth,
        'viewsViberDay':viewsViberDay,'viewsViberWeek':viewsViberWeek,'viewsViberMonth':viewsViberMonth,'viewsViber':viewsViber,
        'viewsViberPreviousDay':viewsViberPreviousDay,'viewsViberPreviousWeek':viewsViberPreviousWeek,'viewsViberPreviousMonth':viewsViberPreviousMonth,
        'interactionsViberDay':interactionsViberDay,'interactionsViberWeek':interactionsViberWeek,'interactionsViberMonth':interactionsViberMonth,'interactionsViber':interactionsViber,
        'interactionsViberPreviousDay':interactionsViberPreviousDay,'interactionsViberPreviousWeek':interactionsViberPreviousWeek,'interactionsViberPreviousMonth':interactionsViberPreviousMonth,
        'uniqueUsersViberDay':uniqueUsersViberDay,'uniqueUsersViberWeek':uniqueUsersViberWeek,'uniqueUsersViberMonth':uniqueUsersViberMonth,'uniqueUsersViber':uniqueUsersViber,
        'uniqueUsersViberPreviousDay':uniqueUsersViberPreviousDay,'uniqueUsersViberPreviousWeek':uniqueUsersViberPreviousWeek,'uniqueUsersViberPreviousMonth':uniqueUsersViberPreviousMonth,
        'repeatedUsersViberDay':repeatedUsersViberDay,'repeatedUsersViberWeek':repeatedUsersViberWeek,'repeatedUsersViberMonth':repeatedUsersViberMonth,'repeatedUsersViber':repeatedUsersViber,
        'repeatedUsersViberPreviousDay':repeatedUsersViberPreviousDay,'repeatedUsersViberPreviousWeek':repeatedUsersViberPreviousWeek,'repeatedUsersViberPreviousMonth':repeatedUsersViberPreviousMonth,
        'timeSpentPerUserViberDay':timeSpentPerUserViberDay,'timeSpentPerUserViberWeek':timeSpentPerUserViberWeek,'timeSpentPerUserViberMonth':timeSpentPerUserViberMonth,'timeSpentPerUserViber':timeSpentPerUserViber,
        'timeSpentPerUserViberPreviousDay':timeSpentPerUserViberPreviousDay,'timeSpentPerUserViberPreviousWeek':timeSpentPerUserViberPreviousWeek,'timeSpentPerUserViberPreviousMonth':timeSpentPerUserViberPreviousMonth,
        'repeatUsePerUserViberDay':repeatUsePerUserViberDay,'repeatUsePerUserViberWeek':repeatUsePerUserViberWeek,'repeatUsePerUserViberMonth':repeatUsePerUserViberMonth,'repeatUsePerUserViber':repeatUsePerUserViber,
        'repeatUsePerUserViberPreviousDay':repeatUsePerUserViberPreviousDay,'repeatUsePerUserViberPreviousWeek':repeatUsePerUserViberPreviousWeek,'repeatUsePerUserViberPreviousMonth':repeatUsePerUserViberPreviousMonth,
        'viewsArrayViberDay':viewsArrayViberDay,'viewsArrayViberWeek':viewsArrayViberWeek,'viewsArrayViberMonth':viewsArrayViberMonth,
        'interactionsArrayViberDay':interactionsArrayViberDay,'interactionsArrayViberWeek':interactionsArrayViberWeek,'interactionsArrayViberMonth':interactionsArrayViberMonth,
        'uniqueUsersArrayViberDay':uniqueUsersArrayViberDay,'uniqueUsersArrayViberWeek':uniqueUsersArrayViberWeek,'uniqueUsersArrayViberMonth':uniqueUsersArrayViberMonth,
        'repeatedUsersArrayViberDay':repeatedUsersArrayViberDay,'repeatedUsersArrayViberWeek':repeatedUsersArrayViberWeek,'repeatedUsersArrayViberMonth':repeatedUsersArrayViberMonth,
        'timeSpentPerUserArrayViberDay':timeSpentPerUserArrayViberDay,'timeSpentPerUserArrayViberWeek':timeSpentPerUserArrayViberWeek,'timeSpentPerUserArrayViberMonth':timeSpentPerUserArrayViberMonth,
        'repeatUsePerUserArrayViberDay':repeatUsePerUserArrayViberDay,'repeatUsePerUserArrayViberWeek':repeatUsePerUserArrayViberWeek,'repeatUsePerUserArrayViberMonth':repeatUsePerUserArrayViberMonth}},{upsert:true});
};

var Views = function(times, thisCampaignName){
    var usersViews= Tracks.find({$and:[{modifiedAt: {$exists: true, $gte: times.start, $lt: times.end}}, {'uid': {$not: /==/}},
        {campaignName: thisCampaignName }]}).fetch().map(function (x) {
        return x.repeatView;
    });
    var usersViews2= Tracks.find({$and:[{modifiedAt: {$exists: false}},{createdAt:{ $gte: times.start, $lt: times.end}}, {'uid': {$not: /==/}},
        {campaignName: thisCampaignName }]}).fetch().map(function (x) {
        return x.repeatView;
    });
    var sum=0;
    for(var i=0; i<usersViews.length; i++){
        sum+=parseInt(usersViews[i])+1;
    }
    for(var i=0; i<usersViews2.length; i++){
        sum+=parseInt(usersViews2[i])+1;
    }

    return sum;
};

var ViewsViber = function(times, thisCampaignName){
    var usersViews= Tracks.find({$and:[{modifiedAt: {$exists: true, $gte: times.start, $lt: times.end}}, {'uid': /==/},
        {campaignName: thisCampaignName }]}).fetch().map(function (x) {
        return x.repeatView;
    });
    var usersViews2= Tracks.find({$and:[{modifiedAt: {$exists: false}},{createdAt:{ $gte: times.start, $lt: times.end}}, {'uid': /==/},
        {campaignName: thisCampaignName }]}).fetch().map(function (x) {
        return x.repeatView;
    });
    var sum=0;
    for(var i=0; i<usersViews.length; i++){
        sum+=parseInt(usersViews[i])+1;
    }
    for(var i=0; i<usersViews2.length; i++){
        sum+=parseInt(usersViews2[i])+1;
    }

    return sum;
};

var Interactions = function(times, thisCampaignName) {
    var messagesArr = Tracks.find({
        $and: [{modifiedAt: {$exists: true, $gte: times.start, $lt: times.end}}, {'uid': {$not: /==/}},
            {campaignName: thisCampaignName}]
    }).fetch().map(function (x) {
        return x.message;
    });
    var messagesArr2 = Tracks.find({
        $and: [{modifiedAt: {$exists: false}},{createdAt:{ $gte: times.start, $lt: times.end}}, {'uid': {$not: /==/}},
            {campaignName: thisCampaignName}]
    }).fetch().map(function (x) {
        return x.message;
    });
    var total=0;
    for (var i = 0; i < messagesArr.length; i++) {
        if( messagesArr[i] )
        total += messagesArr[i].split(",").length;
    }
    for (var i = 0; i < messagesArr2.length; i++) {
        if( messagesArr2[i] )
            total += messagesArr2[i].split(",").length;
    }

    return total;
};

var InteractionsViber = function(times, thisCampaignName) {
    var messagesArr = Tracks.find({
        $and: [{modifiedAt: {$exists: true, $gte: times.start, $lt: times.end}}, {'uid': /==/},
            {campaignName: thisCampaignName}]
    }).fetch().map(function (x) {
        return x.message;
    });
    var messagesArr2 = Tracks.find({
        $and: [{modifiedAt: {$exists: false}},{createdAt:{ $gte: times.start, $lt: times.end}}, {'uid': /==/},
            {campaignName: thisCampaignName}]
    }).fetch().map(function (x) {
        return x.message;
    });
    var total=0;
    for (var i = 0; i < messagesArr.length; i++) {
        if( messagesArr[i] )
            total += messagesArr[i].split(",").length;
    }
    for (var i = 0; i < messagesArr2.length; i++) {
        if( messagesArr2[i] )
            total += messagesArr2[i].split(",").length;
    }

    return total;
};

var UniqueUsers = function(times, thisCampaignName ) {
    var a=Tracks.find({$and:[{modifiedAt: {$exists: true, $gte: times.start, $lt: times.end}}, {'uid': {$not: /==/}},
        {campaignName: thisCampaignName }]}).count();
    var b=Tracks.find({$and:[{modifiedAt: {$exists: false}},{createdAt:{ $gte: times.start, $lt: times.end}}, {'uid': {$not: /==/}},
        {campaignName: thisCampaignName }]}).count();

    return a + b;
}

var UniqueUsersViber = function(times, thisCampaignName ) {
    var a=Tracks.find({$and:[{modifiedAt: {$exists: true, $gte: times.start, $lt: times.end}}, {'uid': /==/},
        {campaignName: thisCampaignName }]}).count();
    var b=Tracks.find({$and:[{modifiedAt: {$exists: false}},{createdAt:{ $gte: times.start, $lt: times.end}}, {'uid': /==/},
        {campaignName: thisCampaignName }]}).count();

    return a + b;
}

var RepeatedUsers = function(times, thisCampaignName){
    var a= Tracks.find({$and:[{modifiedAt: {$exists: true, $gte: times.start, $lt: times.end}}, {'uid': {$not: /==/}},
        {repeatView: {$gt: "0"}},
        {campaignName: thisCampaignName }]}).count();
    var b= Tracks.find({$and:[{modifiedAt: {$exists: false}},{createdAt:{ $gte: times.start, $lt: times.end}}, {'uid': {$not: /==/}},
        {repeatView: {$gt: "0"}},
        {campaignName: thisCampaignName }]}).count();

    return a + b;
};

var RepeatedUsersViber = function(times, thisCampaignName){
    var a= Tracks.find({$and:[{modifiedAt: {$exists: true, $gte: times.start, $lt: times.end}}, {'uid': /==/},
        {repeatView: {$gt: "0"}},
        {campaignName: thisCampaignName }]}).count();
    var b= Tracks.find({$and:[{modifiedAt: {$exists: false}},{createdAt:{ $gte: times.start, $lt: times.end}}, {'uid': /==/},
        {repeatView: {$gt: "0"}},
        {campaignName: thisCampaignName }]}).count();

    return a + b;
};

var TimeSpentPerUser = function(times, thisCampaignName) {
    var timeArray = Tracks.find({
        $and: [{createdAt: {$gte: times.start, $lt: times.end}},  {'uid': {$not: /==/}},
            {createdAt: {$type: 9}},
            {modifiedAt: {$type: 9}},
            {campaignName: thisCampaignName}]
    }).fetch().map(function (x) {
        return ({"createdAt":x.createdAt, "modifiedAt":x.modifiedAt});
    });
    var counter = 0;
    var aggregator = 0;
    for (var i = 0; i < timeArray.length; i++) {
        if (timeArray[i]!=undefined && timeArray[i].createdAt != undefined && timeArray[i].modifiedAt != undefined) {
            if (timeArray[i].modifiedAt.getHours() == timeArray[i].createdAt.getHours()) {
                var mintues = timeArray[i].modifiedAt.getMinutes() - timeArray[i].createdAt.getMinutes();
                aggregator += mintues;
                counter++;
            }
        }
    }
    if(aggregator == 0 && counter == 0)
        return 0;

    var result = aggregator / counter;
    result = result.toFixed(2);

    return result;
};

var TimeSpentPerUserViber = function(times, thisCampaignName) {
    var timeArray = Tracks.find({
        $and: [{createdAt: {$gte: times.start, $lt: times.end}},  {'uid': /==/},
            {createdAt: {$type: 9}},
            {modifiedAt: {$type: 9}},
            {campaignName: thisCampaignName}]
    }).fetch().map(function (x) {
        return ({"createdAt":x.createdAt, "modifiedAt":x.modifiedAt});
    });
    var counter = 0;
    var aggregator = 0;
    for (var i = 0; i < timeArray.length; i++) {
        if (timeArray[i]!=undefined && timeArray[i].createdAt != undefined && timeArray[i].modifiedAt != undefined) {
            if (timeArray[i].modifiedAt.getHours() == timeArray[i].createdAt.getHours()) {
                var mintues = timeArray[i].modifiedAt.getMinutes() - timeArray[i].createdAt.getMinutes();
                aggregator += mintues;
                counter++;
            }
        }
    }
    if(aggregator == 0 && counter == 0)
        return 0;

    var result = aggregator / counter;
    result = result.toFixed(2);

    return result;
};

var RepeatUsePerUser = function (times, thisCampaignName) {
    var totalViews = Views(times, thisCampaignName);
    var totalUniqueUsers = UniqueUsers(times, thisCampaignName);
    if(totalViews == 0 && totalUniqueUsers == 0)
        return 0;

    var result = totalViews/totalUniqueUsers;
    result = result.toFixed(2);

    return result;
};

var RepeatUsePerUserViber = function (times, thisCampaignName) {
    var totalViews = ViewsViber(times, thisCampaignName);
    var totalUniqueUsers = UniqueUsersViber(times, thisCampaignName);
    if(totalViews == 0 && totalUniqueUsers == 0)
        return 0;

    var result = totalViews/totalUniqueUsers;
    result = result.toFixed(2);

    return result;
};

var Males = function(times, thisCampaignName ) {
    var a =Tracks.find({$and:[{modifiedAt: {$exists: true, $gte: times.start, $lt: times.end}},{gender: "male"},
        {campaignName: thisCampaignName }]}).count();
    var b =Tracks.find({$and:[{modifiedAt: {$exists: false}},{createdAt:{ $gte: times.start, $lt: times.end}}, {gender: "male"},
        {campaignName: thisCampaignName }]}).count();

    return a + b;
}

var Females = function(times, thisCampaignName ) {
    var a =Tracks.find({$and:[{modifiedAt: {$exists: true, $gte: times.start, $lt: times.end}},{gender: "female"},
        {campaignName: thisCampaignName }]}).count();
    var b =Tracks.find({$and:[{modifiedAt: {$exists: false}},{createdAt:{ $gte: times.start, $lt: times.end}}, {gender: "female"},
        {campaignName: thisCampaignName }]}).count();

    return a + b;
}

var Countries = function(times, thisCampaignName ) {
    var localeArray1 =Tracks.find({$and:[{modifiedAt: {$exists: true, $gte: times.start, $lt: times.end}},
        {campaignName: thisCampaignName }]}).fetch().map(function (x) {
        return x.locale;
    });
    var localeArray2 =Tracks.find({$and:[{modifiedAt: {$exists: false}},{createdAt:{ $gte: times.start, $lt: times.end}},
        {campaignName: thisCampaignName }]}).fetch().map(function (x) {
        return x.locale;
    });

    var countriesArray = {};
    for(var i = 0; i < localeArray1.length; i++) {
        var country = localeToCountry(localeArray1[i]);
        if (country == undefined)
            continue;
        var value = countriesArray[country];
        if(isNaN(value))
            value = 0;
        countriesArray[country] = ++value;
    }
    for(var i = 0; i < localeArray2.length; i++) {
        var country = localeToCountry(localeArray2[i]);
        if (country == undefined)
            continue;
        var value = countriesArray[country];
        if(isNaN(value))
            value = 0;
        countriesArray[country] = ++value;
    }

    countriesArray = getSortedCountries(countriesArray);

    return countriesArray;
}

var localeToCountry = function(locale) {
    if (locale == undefined)
        return locale;

    var updatedLocale = locale;
    if (updatedLocale == 'en_US' || updatedLocale == 'es_US')
        return "United States";
    else if (updatedLocale == 'en_GB')
        return "United Kingdom";
    else if(updatedLocale == 'en_AU')
        return "Australia";
    else if (updatedLocale == 'en_NZ')
        return "New Zealand";
    else if (updatedLocale == 'en_IE' || updatedLocale == 'ga_IE')
        return "Ireland";
    else if (updatedLocale == 'en_IN' || updatedLocale == 'hi_IN')
        return "India";
    else if (updatedLocale == 'en_JM')
        return "Jamaica";
    else if (updatedLocale == 'en_PH')
        return "Phillippines";
    else if (updatedLocale == 'en_ZA')
        return "Southern Africa";
    else if(updatedLocale == 'en_CA' || updatedLocale == 'fr_CA')
        return "Canada";
    else if (updatedLocale == 'en_SG')
        return "Singapore";
    else if (updatedLocale == 'en_MT' || updatedLocale == 'mt_MT')
        return "Malta";
    else if (updatedLocale == 'fr_FR')
        return "France";
    else if (updatedLocale == 'fr_BE' || updatedLocale == 'nl_BE')
        return "Belgium";
    else if (updatedLocale == 'fr_LU' || updatedLocale == 'de_LU')
        return "Luxembourg";
    else if (updatedLocale == 'fr_CH' || updatedLocale == 'de_CH' || updatedLocale == 'it_CH')
        return "Switzerland";
    else if (updatedLocale == 'de_AT')
        return "Austria";
    else if (updatedLocale == 'de_DE')
        return "Germany";
    else if (updatedLocale == 'it_IT')
        return "Italy";
    else if (updatedLocale == 'be_BY')
        return "Belarus";
    else if (updatedLocale == 'pt_BR')
        return "Brazil";
    else if (updatedLocale == 'pt_PT')
        return "Portugal";
    else if (updatedLocale =='	fi_FI')
        return "Finland";
    else if (updatedLocale == 'hr_HR')
        return "Croatia";
    else if (updatedLocale == 'hu_HU')
        return "Hungary";
    else if (updatedLocale == 'in_ID')
        return "Indonesia";
    else if (updatedLocale == 'et_EE')
        return "Estonia";
    else if (updatedLocale == '	el_GR')
        return "Greece";
    else if (updatedLocale == '	el_CY')
        return "Cyprus";
    else if (updatedLocale == 'is_IS')
        return "Iceland";
    else if (updatedLocale == 'iw_IL')
        return "Israel";
    else if (updatedLocale == 'ja_JP')
        return "Japan";
    else if (updatedLocale == 'ko_KR')
        return "South Korea";
    else if (updatedLocale == 'lt_LT')
        return "Lithuania";
    else if (updatedLocale == 'lv_LV')
        return "Latvia";
    else if (updatedLocale == 'mk_MK')
        return "Macedonia";
    else if (updatedLocale == 'ms_MY')
        return "Malaysia";
    else if (updatedLocale == 'no_NO')
        return "Norway";
    else if (updatedLocale == 'pl_PL')
        return "Poland";
    else if (updatedLocale == 'ro_RO')
        return "Romania";
    else if (updatedLocale == 'ru_RU')
        return "Russia";
    else if (updatedLocale == 'sk_SK')
        return "Slovakia";
    else if (updatedLocale == 'sl_SI')
        return "Slovenia";
    else if (updatedLocale == 'sq_AL')
        return "Albania";
    else if (updatedLocale == 'sv_SE')
        return "Sweden";
    else if (updatedLocale == 'tr_TR')
        return "Turkey";
    else if (updatedLocale == 'uk_UA')
        return "Ukraine";
    else if (updatedLocale == 'bg_BG')
        return "Bulgaria";
    else if (updatedLocale == 'vi_VN')
        return "Vietnam";
    else if (updatedLocale == 'cs_CZ')
        return "Czech Republic";
    else if (updatedLocale == 'da_DK')
        return "Denmark";
    else if (updatedLocale == 'nl_NL')
        return "Netherlands";
    else if (updatedLocale == 'sr_RS')
        return "Serbia";
    else if (updatedLocale == 'sr_BA')
        return "Bosnia and Herzegovina";
    else if (updatedLocale == 'sr_ME')
        return "Montenegro";
    else if (updatedLocale == 'zh_CN')
        return "China";
    else if (updatedLocale == 'zh_HK')
        return "Hong Kong";
    else if (updatedLocale == 'zh_SG')
        return "Singapore";
    else if (updatedLocale == 'zh_TW')
        return "Taiwan";
    else if (updatedLocale == 'th_TH')
        return "Thailand";
    else if (updatedLocale == 'ar_AE')
        return "United Arab Emirates";
    else if (updatedLocale == 'ar_BH')
        return "Bahrain";
    else if (updatedLocale == 'ar_DZ')
        return "Algeria";
    else if (updatedLocale == 'ar_EG')
        return "Egypt";
    else if (updatedLocale == 'ar_IQ')
        return "Iraq";
    else if (updatedLocale == 'ar_JO')
        return "Jordan";
    else if (updatedLocale == 'ar_KW')
        return "Kuwait";
    else if (updatedLocale == 'ar_LB')
        return "Lebanon";
    else if (updatedLocale == 'ar_LY')
        return "Libya";
    else if (updatedLocale == 'ar_MA')
        return "Morocco";
    else if (updatedLocale == 'ar_OM')
        return "Oman";
    else if (updatedLocale == 'ar_QA')
        return "Qatar";
    else if (updatedLocale == 'ar_SA')
        return "Saudi Arabia";
    else if (updatedLocale == 'ar_SD')
        return "Sudan";
    else if (updatedLocale == 'ar_SY')
        return "Syria";
    else if (updatedLocale == 'ar_TN')
        return "Tunisia";
    else if (updatedLocale == 'ar_YE')
        return "Yemen";
    else if (updatedLocale == 'es_ES' || updatedLocale == 'ca_ES')
        return "Spain";
    else if (updatedLocale == 'es_MX')
        return "Mexico";
    else if (updatedLocale == 'es_AR')
        return "Argentina";
    else if (updatedLocale == 'es_BO')
        return "Bolivia";
    else if (updatedLocale == 'es_CL')
        return "Chile";
    else if (updatedLocale == 'es_CO')
        return "Colombia";
    else if (updatedLocale == 'es_CR')
        return "Costa Rica";
    else if (updatedLocale == 'es_DO')
        return "Dominican Republic";
    else if (updatedLocale == 'es_EC')
        return "Ecuador";
    else if (updatedLocale == 'es_GT')
        return "Guatemala";
    else if (updatedLocale == 'es_HN')
        return "Honduras";
    else if (updatedLocale == 'es_NI')
        return "Nicaragua";
    else if (updatedLocale == 'es_PA')
        return "Panama";
    else if (updatedLocale == 'es_PE')
        return "Peru";
    else if (updatedLocale == 'es_PR')
        return "Puerto Rico";
    else if (updatedLocale == 'es_PY')
        return "Paraguay";
    else if (updatedLocale == 'es_SV')
        return "El Salvador";
    else if (updatedLocale == 'es_UY')
        return "Uruguay";
    else if (updatedLocale == 'es_VE')
        return "Venezuela";
}

function getSortedCountries(obj) {
    var keys = [];
    for(var key in obj)
        keys.push(key);
    var sortedKeys = keys.sort(function(a,b){return obj[b]-obj[a]});
    var sortedCountiresArray = {};
    for (var i=0; i<sortedKeys.length; i++)
        sortedCountiresArray[sortedKeys[i]] = obj[sortedKeys[i]];
    var tenTopCountiresArray = {};
    var i = 0;
    for (var key in sortedCountiresArray) {
        tenTopCountiresArray[key] = sortedCountiresArray[key];
        if (i == 9)
            break;
        i++;
    }

    return tenTopCountiresArray;
}


var setTime=function(time) {
    var start=new Date(), end=new Date();
    if(time=="Today"){
        start.setHours(0,0,0,0);
        end.setHours(23,59,59,999);
    }
    else if(time=="Yesterday"){
        start.setDate(start.getDate() - 1);
        start.setHours(0,0,0,0);
        end.setDate(end.getDate() - 1);
        end.setHours(23,59,59,999);
    }
    else if(time=="ThisWeek"){
        var daytoset=1,distance; // 1 is Monday
        var currentDay = start.getDay();
        if(currentDay==0)//today is sunday
            distance = -6;
        else
            distance= daytoset - currentDay;
        start.setDate(start.getDate() + distance);
        start.setHours(0,0,0,0);
        end.setHours(23,59,59,999);
    }
    else if(time=="LastWeek"){
        var daytoset=1,distance; // 1 is Monday
        var currentDay = start.getDay();
        if(currentDay==0)//today is sunday
            distance = -6;
        else
            distance= daytoset - currentDay;
        start.setDate(start.getDate() + distance-7);
        start.setHours(0,0,0,0);
        end.setDate(end.getDate() + distance-1);
        end.setHours(23,59,59,999);
    }
    else if(time=="ThisMonth"){
        start.setDate(1);
        start.setHours(0,0,0,0);
        end.setHours(23,59,59,999);
    }
    else if(time=="LastMonth"){
        start.setDate(1);
        start.setHours(0,0,0,0);
        start.setMonth(start.getMonth() - 1);
        end=new Date(end.getFullYear(), end.getMonth(), 0);
        end.setHours(23,59,59,999);
    }
    else if(time=="All"){
        start.setYear(2015);
        end.setHours(23,59,59,999);
    }

    return {start: start, end:end};
}

var DataArray= function(fnName,timeType, cn) {
    var data = [];
    if (timeType == 'Week') {
        var start = new Date(), end = new Date();
        var currentDay = start.getDay();
        var dateOfSunday = start.getDate() - currentDay;
        for (var i=0; i<7; i++) {
            start = new Date(), end = new Date();
            start.setDate(dateOfSunday + i);
            end.setDate(dateOfSunday + i);
            start.setHours(0, 0, 0, 0);
            end.setHours(23, 59, 59, 999);
            var element = eval(fnName)({start: start, end: end}, cn);
            data.push(element);
        }

        return data;
    }
    var labels = [];
    if (timeType == "Day")
        labels = labelsDay();
    else if (timeType == "Month")
        labels = labelsMonth();
    for(var i=0; i<labels.length; i++){
        var element = eval(fnName)(setTimeArray(labels[i],timeType),cn);
        data.push(element);
    }

    return data;
}

var labelsDay = function () {
    return [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23];
}

var labelsMonth = function () {
    var array = [];
    var currentDate = new Date();
    var numberOfDays = new Date(currentDate.getFullYear(), currentDate.getMonth()+1, 0).getDate();
    for (var i=1; i<=numberOfDays; i++)
        array.push(i);

    return array;
}

var setTimeArray=function(time, timeType) {
    if (timeType == "Day") {
        var start = new Date(), end = new Date();
        start.setHours(time, 0, 0, 0);
        end.setHours(time, 59, 59, 999);
        return {start: start, end: end};
    }
    else if (timeType == "Month") {
        var start = new Date(), end = new Date();
        start.setDate(time);
        end.setDate(time);
        start.setHours(0, 0, 0, 0);
        end.setHours(23, 59, 59, 999);
        return {start: start, end: end};
    }
}