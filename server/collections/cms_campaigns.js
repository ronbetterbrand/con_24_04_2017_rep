CmsCampaigns.allow({
	insert: function (userId, doc) {
		return CmsCampaigns.userCanInsert(userId, doc);
	},

	update: function (userId, doc, fields, modifier) {
		return CmsCampaigns.userCanUpdate(userId, doc);
	},

	remove: function (userId, doc) {
		return CmsCampaigns.userCanRemove(userId, doc);
	}
});

CmsCampaigns.before.insert(function(userId, doc) {
	doc.createdAt = new Date();
	doc.createdBy = userId;
	doc.modifiedAt = doc.createdAt;
	doc.modifiedBy = doc.createdBy;

	
	if(!doc.ownerId) doc.ownerId = userId;
});

CmsCampaigns.before.update(function(userId, doc, fieldNames, modifier, options) {
	modifier.$set = modifier.$set || {};
	modifier.$set.modifiedAt = new Date();
	modifier.$set.modifiedBy = userId;

	
});

CmsCampaigns.before.remove(function(userId, doc) {
	
});

CmsCampaigns.after.insert(function(userId, doc) {
	
});

CmsCampaigns.after.update(function(userId, doc, fieldNames, modifier, options) {
	
});

CmsCampaigns.after.remove(function(userId, doc) {
	
});
