CmsCustomers.allow({
	insert: function (userId, doc) {
		return CmsCustomers.userCanInsert(userId, doc);
	},

	update: function (userId, doc, fields, modifier) {
		return CmsCustomers.userCanUpdate(userId, doc);
	},

	remove: function (userId, doc) {
		return CmsCustomers.userCanRemove(userId, doc);
	}
});

CmsCustomers.before.insert(function(userId, doc) {
	doc.createdAt = new Date();
	doc.createdBy = userId;
	doc.modifiedAt = doc.createdAt;
	doc.modifiedBy = doc.createdBy;

	
	if(!doc.ownerId) doc.ownerId = userId;
});

CmsCustomers.before.update(function(userId, doc, fieldNames, modifier, options) {
	modifier.$set = modifier.$set || {};
	modifier.$set.modifiedAt = new Date();
	modifier.$set.modifiedBy = userId;

	
});

CmsCustomers.before.remove(function(userId, doc) {
	
});

CmsCustomers.after.insert(function(userId, doc) {
	
});

CmsCustomers.after.update(function(userId, doc, fieldNames, modifier, options) {
	
});

CmsCustomers.after.remove(function(userId, doc) {
	
});
