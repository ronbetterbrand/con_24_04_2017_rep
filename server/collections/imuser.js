Imuser.allow({
    insert: function (userId, doc) {
        return Imuser.userCanInsert(userId, doc);
    },

    update: function (userId, doc, fields, modifier) {
        return Imuser.userCanUpdate(userId, doc);
    },

    remove: function (userId, doc) {
        return Imuser.userCanRemove(userId, doc);
    }
});

Imuser.before.insert(function(userId, doc) {
    doc.createdAt = new Date();
    doc.createdBy = userId;
    doc.modifiedAt = doc.createdAt;
    doc.modifiedBy = doc.createdBy;


    if(!doc.createdBy) doc.createdBy = userId;
});

Imuser.before.update(function(userId, doc, fieldNames, modifier, options) {
    modifier.$set = modifier.$set || {};
    modifier.$set.modifiedAt = new Date();
    modifier.$set.modifiedBy = userId;


});

Imuser.before.remove(function(userId, doc) {

});

Imuser.after.insert(function(userId, doc) {

});

Imuser.after.update(function(userId, doc, fieldNames, modifier, options) {

});

Imuser.after.remove(function(userId, doc) {

});
