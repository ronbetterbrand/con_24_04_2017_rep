CmsSections.allow({
    insert: function (userId, doc) {
        return CmsSections.userCanInsert(userId, doc);
    },

    update: function (userId, doc, fields, modifier) {
        return CmsSections.userCanUpdate(userId, doc);
    },

    remove: function (userId, doc) {
        return CmsSections.userCanRemove(userId, doc);
    }
});

CmsSections.before.insert(function(userId, doc) {
    doc.createdAt = new Date();
    doc.createdBy = userId;
    doc.modifiedAt = doc.createdAt;
    doc.modifiedBy = doc.createdBy;


    if(!doc.ownerId) doc.ownerId = userId;
});

CmsSections.before.update(function(userId, doc, fieldNames, modifier, options) {
    modifier.$set = modifier.$set || {};
    modifier.$set.modifiedAt = new Date();
    modifier.$set.modifiedBy = userId;


});

CmsSections.before.remove(function(userId, doc) {

});

CmsSections.after.insert(function(userId, doc) {

});

CmsSections.after.update(function(userId, doc, fieldNames, modifier, options) {

});

CmsSections.after.remove(function(userId, doc) {

});
