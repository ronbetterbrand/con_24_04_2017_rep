CmsQR.allow({
    insert: function (userId, doc) {
        return CmsQR.userCanInsert(userId, doc);
    },

    update: function (userId, doc, fields, modifier) {
        return CmsQR.userCanUpdate(userId, doc);
    },

    remove: function (userId, doc) {
        return CmsQR.userCanRemove(userId, doc);
    }
});

CmsQR.before.insert(function(userId, doc) {
    doc.createdAt = new Date();
    doc.createdBy = userId;
    doc.modifiedAt = doc.createdAt;
    doc.modifiedBy = doc.createdBy;


    if(!doc.ownerId) doc.ownerId = userId;
});

CmsQR.before.update(function(userId, doc, fieldNames, modifier, options) {
    modifier.$set = modifier.$set || {};
    modifier.$set.modifiedAt = new Date();
    modifier.$set.modifiedBy = userId;


});

CmsQR.before.remove(function(userId, doc) {

});

CmsQR.after.insert(function(userId, doc) {

});

CmsQR.after.update(function(userId, doc, fieldNames, modifier, options) {

});

CmsQR.after.remove(function(userId, doc) {

});
