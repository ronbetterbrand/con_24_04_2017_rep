Analytics.allow({
    insert: function (userId, doc) {
        return Analytics.userCanInsert(userId, doc);
    },

    update: function (userId, doc, fields, modifier) {
        return Analytics.userCanUpdate(userId, doc);
    },

    remove: function (userId, doc) {
        return Analytics.userCanRemove(userId, doc);
    }
});

Analytics.before.insert(function(userId, doc) {
    doc.createdAt = new Date();
    doc.createdBy = userId;
    doc.modifiedAt = doc.createdAt;
    doc.modifiedBy = doc.createdBy;


    if(!doc.createdBy) doc.createdBy = userId;
});

Analytics.before.update(function(userId, doc, fieldNames, modifier, options) {
    modifier.$set = modifier.$set || {};
    modifier.$set.modifiedAt = new Date();
    modifier.$set.modifiedBy = userId;


});

Analytics.before.remove(function(userId, doc) {

});

Analytics.after.insert(function(userId, doc) {

});

Analytics.after.update(function(userId, doc, fieldNames, modifier, options) {

});

Analytics.after.remove(function(userId, doc) {

});
