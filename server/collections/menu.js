CmsMenu.allow({
    insert: function (userId, doc) {
        return CmsMenu.userCanInsert(userId, doc);
    },

    update: function (userId, doc, fields, modifier) {
        return CmsMenu.userCanUpdate(userId, doc);
    },

    remove: function (userId, doc) {
        return CmsMenu.userCanRemove(userId, doc);
    }
});

CmsMenu.before.insert(function(userId, doc) {
    doc.createdAt = new Date();
    doc.createdBy = userId;
    doc.modifiedAt = doc.createdAt;
    doc.modifiedBy = doc.createdBy;


    if(!doc.ownerId) doc.ownerId = userId;
});

CmsMenu.before.update(function(userId, doc, fieldNames, modifier, options) {
    modifier.$set = modifier.$set || {};
    modifier.$set.modifiedAt = new Date();
    modifier.$set.modifiedBy = userId;


});

CmsMenu.before.remove(function(userId, doc) {

});

CmsMenu.after.insert(function(userId, doc) {

});

CmsMenu.after.update(function(userId, doc, fieldNames, modifier, options) {

});

CmsMenu.after.remove(function(userId, doc) {

});
