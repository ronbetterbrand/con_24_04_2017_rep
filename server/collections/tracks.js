Tracks.allow({
    insert: function (userId, doc) {
        return Tracks.userCanInsert(userId, doc);
    },

    update: function (userId, doc, fields, modifier) {
        return Tracks.userCanUpdate(userId, doc);
    },

    remove: function (userId, doc) {
        return Tracks.userCanRemove(userId, doc);
    }
});

Tracks.before.insert(function(userId, doc) {
    doc.createdAt = new Date();
    doc.createdBy = userId;
    doc.modifiedAt = doc.createdAt;
    doc.modifiedBy = doc.createdBy;


    if(!doc.createdBy) doc.createdBy = userId;
});

Tracks.before.update(function(userId, doc, fieldNames, modifier, options) {
    modifier.$set = modifier.$set || {};
    modifier.$set.modifiedAt = new Date();
    modifier.$set.modifiedBy = userId;


});

Tracks.before.remove(function(userId, doc) {

});

Tracks.after.insert(function(userId, doc) {

});

Tracks.after.update(function(userId, doc, fieldNames, modifier, options) {

});

Tracks.after.remove(function(userId, doc) {

});
