Campaigns.allow({
	insert: function (userId, doc) {
		return Campaigns.userCanInsert(userId, doc);
	},

	update: function (userId, doc, fields, modifier) {
		return Campaigns.userCanUpdate(userId, doc);
	},

	remove: function (userId, doc) {
		return Campaigns.userCanRemove(userId, doc);
	}
});

Campaigns.before.insert(function(userId, doc) {
	doc.createdAt = new Date();
	doc.createdBy = userId;
	doc.modifiedAt = doc.createdAt;
	doc.modifiedBy = doc.createdBy;

	
	if(!doc.createdBy) doc.createdBy = userId;
});

Campaigns.before.update(function(userId, doc, fieldNames, modifier, options) {
	modifier.$set = modifier.$set || {};
	modifier.$set.modifiedAt = new Date();
	modifier.$set.modifiedBy = userId;

	
});

Campaigns.before.remove(function(userId, doc) {
	
});

Campaigns.after.insert(function(userId, doc) {
	
});

Campaigns.after.update(function(userId, doc, fieldNames, modifier, options) {
	
});

Campaigns.after.remove(function(userId, doc) {
	
});
