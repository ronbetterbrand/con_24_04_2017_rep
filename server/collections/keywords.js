Keywords.allow({
    insert: function (userId, doc) {
        return Keywords.userCanInsert(userId, doc);
    },

    update: function (userId, doc, fields, modifier) {
        return Keywords.userCanUpdate(userId, doc);
    },

    remove: function (userId, doc) {
        return Keywords.userCanRemove(userId, doc);
    }
});

Keywords.before.insert(function(userId, doc) {
    doc.createdAt = new Date();
    doc.createdBy = userId;
    doc.modifiedAt = doc.createdAt;
    doc.modifiedBy = doc.createdBy;


    if(!doc.createdBy) doc.createdBy = userId;
});

Keywords.before.update(function(userId, doc, fieldNames, modifier, options) {
    modifier.$set = modifier.$set || {};
    modifier.$set.modifiedAt = new Date();
    modifier.$set.modifiedBy = userId;


});

Keywords.before.remove(function(userId, doc) {

});

Keywords.after.insert(function(userId, doc) {

});

Keywords.after.update(function(userId, doc, fieldNames, modifier, options) {

});

Keywords.after.remove(function(userId, doc) {

});
