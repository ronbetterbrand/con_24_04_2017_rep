this.CmsCustomers = new Mongo.Collection("cms_customers");

this.CmsCustomers.userCanInsert = function(userId, doc) {
	return true;
};

this.CmsCustomers.userCanUpdate = function(userId, doc) {
	return userId && doc.ownerId == userId;
};

this.CmsCustomers.userCanRemove = function(userId, doc) {
	return userId && doc.ownerId == userId;
};
