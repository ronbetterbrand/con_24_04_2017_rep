this.CmsQR = new Mongo.Collection("cms_qr");

this.CmsQR.userCanInsert = function(userId, doc) {
    return true;
};

this.CmsQR.userCanUpdate = function(userId, doc) {
    return userId && doc.ownerId == userId;
};

this.CmsQR.userCanRemove = function(userId, doc) {
    return userId && doc.ownerId == userId;
};
