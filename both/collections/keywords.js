this.Keywords = new Mongo.Collection("temp_keywords");

this.Keywords.userCanInsert = function(userId, doc) {
    return true;
}

this.Keywords.userCanUpdate = function(userId, doc) {
    return true;
}

this.Keywords.userCanRemove = function(userId, doc) {
    return true;
}