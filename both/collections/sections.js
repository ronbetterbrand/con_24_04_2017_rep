this.CmsSections = new Mongo.Collection("cms_sections");

this.CmsSections.userCanInsert = function(userId, doc) {
    return true;
};

this.CmsSections.userCanUpdate = function(userId, doc) {
    return userId && doc.ownerId == userId;
};

this.CmsSections.userCanRemove = function(userId, doc) {
    return userId && doc.ownerId == userId;
};
