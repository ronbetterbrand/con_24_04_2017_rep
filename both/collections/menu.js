this.CmsMenu = new Mongo.Collection("cms_menu");

this.CmsMenu.userCanInsert = function(userId, doc) {
    return true;
};

this.CmsMenu.userCanUpdate = function(userId, doc) {
    return userId && doc.ownerId == userId;
};

this.CmsMenu.userCanRemove = function(userId, doc) {
    return userId && doc.ownerId == userId;
};
