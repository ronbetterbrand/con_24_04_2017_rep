this.Campaigns = new Mongo.Collection("campaigns");

this.Campaigns.userCanInsert = function(userId, doc) {
	return true;
}

this.Campaigns.userCanUpdate = function(userId, doc) {
	return true;
}

this.Campaigns.userCanRemove = function(userId, doc) {
	return true;
}
