this.Analytics = new Mongo.Collection("analytics");

this.Analytics.userCanInsert = function(userId, doc) {
    return true;
}

this.Analytics.userCanUpdate = function(userId, doc) {
    return true;
}

this.Analytics.userCanRemove = function(userId, doc) {
    return true;
}
