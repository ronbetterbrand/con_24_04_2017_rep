this.Imuser = new Mongo.Collection("imuser");

this.Imuser.userCanInsert = function(userId, doc) {
    return true;
}

this.Imuser.userCanUpdate = function(userId, doc) {
    return true;
}

this.Imuser.userCanRemove = function(userId, doc) {
    return true;
}
