this.CmsCampaigns = new Mongo.Collection("cms_campaigns");

this.CmsCampaigns.userCanInsert = function(userId, doc) {
	return true;
};

this.CmsCampaigns.userCanUpdate = function(userId, doc) {
	return userId && doc.ownerId == userId;
};

this.CmsCampaigns.userCanRemove = function(userId, doc) {
	return userId && doc.ownerId == userId;
};
