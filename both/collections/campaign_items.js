this.CmsCampaignItems = new Mongo.Collection("cms_campaign_items");

this.CmsCampaignItems.userCanInsert = function(userId, doc) {
	return true;
};

this.CmsCampaignItems.userCanUpdate = function(userId, doc) {
	return userId && doc.ownerId == userId;
};

this.CmsCampaignItems.userCanRemove = function(userId, doc) {
	return userId && doc.ownerId == userId;
};
